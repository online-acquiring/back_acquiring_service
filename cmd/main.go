package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
)

// @title Hamkorbank E-Comm
// @contact.name API Support
// @contact.url https://hamkorbank.uz/
// @contact.email MaqsadaliHusanov@gmail.com
func main() {
	quitSignal := make(chan os.Signal, 1)
	signal.Notify(quitSignal, os.Interrupt)

	var cfg config.Config
	if err := envconfig.ProcessWith(context.TODO(), &cfg, envconfig.OsLookuper()); err != nil {
		log.Fatal(err)
	}

	l := logger.New(cfg.LogLevel, "acq_back_service")
	application := bootstrap.New(cfg)

	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		OSCall := <-quitSignal
		l.Info(fmt.Sprintf("System Call: %+v", OSCall))
		cancel()
	}()

	docs.SwaggerInfo.Host = cfg.ServerIP + cfg.HTTPPort
	docs.SwaggerInfo.Schemes = []string{"http", "https"}
	application.Run(ctx)
}
