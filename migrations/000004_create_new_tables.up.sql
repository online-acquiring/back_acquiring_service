CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

---merchant_cards
create table merchant_cards
(
    id          uuid                     default uuid_generate_v4() not null
        constraint merchant_cards_pk
            primary key,
    merchant_id uuid                                                not null
        constraint merchant_cards_merchant_id_fk
            references merchants,
    card_id  varchar                                                not null
        constraint card_id_merchant_cards_fk
            references card,
    created_at  timestamp with time zone default now()              not null,
    updated_at  timestamp with time zone
);