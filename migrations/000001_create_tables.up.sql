CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
---params
create table params
(
    id         uuid                 default uuid_generate_v4()
        constraint params_pk
            primary key,
    name       varchar(100),
    code       varchar(64),
    value      varchar(100),
    created_at timestamptz DEFAULT now() NOT NULL,
    updated_at timestamptz,
    constraint params_code_uk
        unique (code)
);

--- merchants
create table merchants
(
    id             uuid                     default uuid_generate_v4() not null
        constraint merchants_pk
            primary key,
    org_name       varchar(200)                                        not null,
    short_name     varchar(80)                                         not null,
    address        varchar(200),
    email          varchar(100),
    phone          varchar(64),
    tax_code       varchar(64),
    state          integer                  default 0                  not null,
    created_at     timestamp with time zone default now()              not null,
    updated_at     timestamp with time zone,
    contract_num   varchar,
    inn            varchar(64),
    doc_created_at date                                                not null
);

---users
create table users
(
    id         uuid                     default uuid_generate_v4() not null
        constraint users_pk
            primary key,
    name       varchar(100)                                        not null,
    state      int                      default 0                  not null,
    login      varchar(60),
    role_code  int                      default 0                  not null,
    email        varchar,
    merchant_id  uuid
        constraint users_merchant_id_fk
            references merchants,
    phone_number varchar(48),
    created_at timestamptz default now() not null,
    updated_at timestamptz,
    constraint users_login_uk
        unique (login)
);

---credentials
create table credentials
(
    id         uuid                     default uuid_generate_v4()            not null
        constraint credentials_pk
            primary key,
    password   varchar(64),
    user_id    uuid                                                           not null
        constraint credentials_user_id_fk
            references users,
    created_at timestamp with time zone default now()                         not null,
    updated_at timestamp with time zone,
    expired_at timestamp with time zone default (now() + '90 days'::interval) not null,
    added_by   integer                  default 0                             not null
);

---schedulers
create table schedulers
(
    id         uuid                     default uuid_generate_v4() not null
        constraint schedulers_pk
            primary key,
    code       varchar(50)                                         not null
        constraint schedulers_code_uk
            unique,
    name       varchar(200)                                        not null,
    expression varchar(100)                                        not null,
    status     integer                  default 0,
    job_id     integer                  default 0,
    created_at timestamp with time zone default now()              not null,
    updated_at timestamp with time zone
);

---accounts
create table accounts
(
    id         uuid                     default uuid_generate_v4() not null
        constraint accounts_pk
            primary key,
    mfo        varchar(5)                                          not null,
    name       varchar(200)                                        not null,
    tax        varchar(30)                                         not null,
    code       varchar(64)                                         not null,
    created_at timestamp with time zone default now()              not null,
    updated_at timestamp with time zone,
    acc_type   integer                  default 0                  not null,
    inn        varchar(64)
);

---terminals
create table terminals
(
    id           uuid    default uuid_generate_v4() not null
        constraint terminals_pk
            primary key,
    name         varchar(50),
    ps_code      varchar(20)                        not null,
    terminal_num varchar(30),
    merchant_num varchar(30),
    port         integer,
    terminal_acc varchar default 0                  not null,
    com          integer default 0                  not null
);

---terminal_accounts
create table terminal_accounts
(
    id          uuid                     default uuid_generate_v4() not null
        constraint terminal_accounts_pk
            primary key,
    terminal_id uuid                                                not null
        constraint terminal_accounts_terminal_id_fk
            references terminals,
    account_id  uuid                                                not null
        constraint terminal_accounts_account_id_fk
            references accounts,
    type        integer                  default 0                  not null,
    state       integer                  default 0                  not null,
    created_at  timestamp with time zone default now()              not null,
    updated_at  timestamp with time zone
);

---merchant_accounts
create table merchant_accounts
(
    id          uuid                     default uuid_generate_v4() not null
        constraint merchant_accounts_pk
            primary key,
    merchant_id uuid                                                not null
        constraint merchant_accounts_merchant_id_fk
            references merchants,
    account_id  uuid                                                not null
        constraint terminal_accounts_account_id_fk
            references accounts,
    type        integer                  default 0                  not null,
    state       integer                  default 0                  not null,
    created_at  timestamp with time zone default now()              not null,
    updated_at  timestamp with time zone
);

---merchant_terminals
create table merchant_terminals
(
    id          uuid                     default uuid_generate_v4() not null
        constraint merchant_terminals_pk
            primary key,
    merchant_id uuid                                                not null
        constraint merchant_terminals_merchant_id_fk
            references merchants,
    terminal_id uuid                                                not null
        constraint merchant_terminals_terminal_id_fk
            references terminals,
    state       integer                  default 0                  not null,
    created_at  timestamp with time zone default now()              not null,
    updated_at  timestamp with time zone
);

---merchant_settings
create table merchant_settings
(
    id           uuid                     default uuid_generate_v4() not null
        constraint merchant_settings_pk
            primary key,
    merchant_id  uuid                                                not null
        constraint merchant_settings_merchant_id_fk
            references merchants,
    merchant_key varchar                                             not null,
    created_at   timestamp with time zone default now()              not null,
    updated_at   timestamp with time zone
);

---card_bins
create table card_bins
(
    bin        varchar(64)
        constraint card_bins_bin_uk
            unique,
    ps_code    varchar(64),
    created_at timestamp with time zone default now() not null,
    updated_at timestamp with time zone
);

---commissions
create table commissions
(
    id          uuid                     default uuid_generate_v4() not null
        constraint commissions_pk
            primary key,
    merchant_id uuid                                                not null
        constraint commissions_merchant_id_fk
            references merchants,
    state       integer                  default 0                  not null,
    fee_type    integer                  default 0                  not null,
    value       numeric                  default 0                  not null,
    created_at  timestamp with time zone default now()              not null,
    updated_at  timestamp with time zone,
    comm_type   integer                  default 0                  not null
);

---payments
create table payments
(
    id              uuid                     default uuid_generate_v4()       not null
        constraint payments_pk
            primary key,
    merchant_id     uuid                                                      not null
        constraint payments_merchant_id_fk
            references merchants,
    card_id         varchar(100)                                              not null,
    amount          numeric                  default 0                        not null,
    currency_code   varchar(3)               default '860'::character varying not null,
    fee_amount      numeric                  default 0,
    processing_id   varchar(100),
    ref_num         varchar(100),
    document_id     varchar(100),
    external_id     varchar(100),
    error_code      varchar(100),
    error_message   varchar(999),
    state           integer                                                   not null,
    creation_date   bigint                   default date_part('epoch'::text, now()),
    created_at      timestamp with time zone default now()                    not null,
    updated_at      timestamp with time zone,
    parent_id       uuid
        constraint payments_payment_id_fk
            references payments,
    reversal_amount numeric                  default 0,
    payment_type    integer                  default 0                        not null
);

create index payments_creation_time_index
    on payments (creation_date desc);

create index payments_payment_type_index
    on payments (payment_type desc);

---objects
create table objects
(
    code integer      not null
        constraint objects_pk
            primary key,
    name varchar(100) not null
);

---object_states
create table object_states
(
    code        integer      not null,
    object_code integer      not null
        constraint object_states_obj_code_fk
            references objects,
    name        varchar(100) not null,
    constraint obj_state_obj_code_uk
        unique (code, object_code)
);

---object_actions
create table object_actions
(
    code        integer      not null,
    object_code integer      not null
        constraint object_actions_obj_code_fk
            references objects,
    name        varchar(100) not null,
    constraint obj_action_obj_code_uk
        unique (code, object_code)
);

---object_transaction_state
create table object_transaction_states
(
    code           integer not null
        constraint object_transaction_states_pk
            primary key,
    object_code    integer not null
        constraint obj_tran_states_obj_code_fk
            references objects,
    state_code     integer not null,
    action_code    integer not null,
    new_state_code integer not null,
    constraint obj_transaction_state_uk
        unique (object_code,state_code,action_code)
);

---objects_action_protocol
create table objects_action_protocol
(
    object_code    integer                                not null
        constraint obj_act_prot_obj_code_fk
            references objects,
    object_id      uuid                                   not null,
    state_code     integer                                not null,
    action_code    integer                                not null,
    new_state_code integer                                not null,
    user_id        uuid                                   not null
        constraint obj_act_prot_user_id_fk
            references users,
    message        varchar(500),
    created_at     timestamp with time zone default now() not null
);

---card
create table card
(
    id         varchar(50)                            not null
        primary key,
    masked_pan varchar(50),
    bin        varchar(6)                             not null,
    expiry     varchar(4),
    created_at timestamp with time zone default now() not null,
    updated_at timestamp with time zone
);

---details
create table details
(
    pay_id     uuid                                   not null
        primary key
        constraint details_payment_id_pk_and_fk
            references payments,
    value      jsonb,
    created_at timestamp with time zone default now() not null,
    updated_at timestamp with time zone,
    payer_data jsonb
);

--add card bins
insert into card_bins(bin,ps_code) values ('8600','UZCARD');
insert into card_bins(bin,ps_code) values ('6262','UZCARD');
insert into card_bins(bin,ps_code) values ('9860','HUMO');
insert into card_bins(bin,ps_code) values ('5614','UZCARD');
insert into card_bins(bin,ps_code) values ('4073','HUMO');
insert into card_bins(bin,ps_code) values ('5440','UZCARD');

--add object
insert into objects (code, name)
values (1, 'payment');

--add object states
insert into object_states (code, object_code, name)
values (1, 1, 'created');
insert into object_states (code, object_code, name)
values (2, 1, 'holded');
insert into object_states (code, object_code, name)
values (3, 1, 'confirmed');
insert into object_states (code, object_code, name)
values (4, 1, 'canceled');
insert into object_states (code, object_code, name)
values (5, 1, 'returned');
insert into object_states (code, object_code, name)
values (6, 1, 'rejected');
insert into object_states (code, object_code, name)
values (7, 1, 'in_manual');
insert into object_states (code, object_code, name)
values (8, 1, 'in_editing');
insert into object_states (code, object_code, name)
values (9, 1, 'in_balance');

--add object actions
insert into object_actions (code, object_code, name)
values (1, 1, 'create');
insert into object_actions (code, object_code, name)
values (2, 1, 'hold');
insert into object_actions (code, object_code, name)
values (3, 1, 'confirm');
insert into object_actions (code, object_code, name)
values (4, 1, 'cancel');
insert into object_actions (code, object_code, name)
values (5, 1, 'return');
insert into object_actions (code, object_code, name)
values (6, 1, 'error');
insert into object_actions (code, object_code, name)
values (7, 1, 'edit');
insert into object_actions (code, object_code, name)
values (8, 1, 'manual');
insert into object_actions (code, object_code, name)
values (9, 1, 'balance');


--add object transaction states
insert into object_transaction_states (code, object_code, state_code, action_code, new_state_code)
values (1, 1, 1, 1, 1); -- created, create, created
insert into object_transaction_states (code, object_code, state_code, action_code, new_state_code)
values (2, 1, 1, 2, 2); -- created, hold, holded
insert into object_transaction_states (code, object_code, state_code, action_code, new_state_code)
values (3, 1, 1, 3, 3); -- created, confirm, confirmed
insert into object_transaction_states (code, object_code, state_code, action_code, new_state_code)
values (4, 1, 1, 6, 6); -- created, error, rejected
insert into object_transaction_states (code, object_code, state_code, action_code, new_state_code)
values (5, 1, 2, 3, 3); -- holded, confirm, confirmed
insert into object_transaction_states (code, object_code, state_code, action_code, new_state_code)
values (6, 1, 2, 4, 4); -- holded, cancel, canceled
insert into object_transaction_states (code, object_code, state_code, action_code, new_state_code)
values (7, 1, 2, 6, 2); -- holded, error, holded
insert into object_transaction_states (code, object_code, state_code, action_code, new_state_code)
values (8, 1, 3, 4, 4); -- confirmed, cancel, canceled
insert into object_transaction_states (code, object_code, state_code, action_code, new_state_code)
values (9, 1, 3, 6, 3); -- confirmed, error, confirmed
insert into object_transaction_states (code, object_code, state_code, action_code, new_state_code)
values (10, 1, 3, 5, 5); -- confirmed, return, returned
insert into object_transaction_states (code, object_code, state_code, action_code, new_state_code)
values (11, 1, 5, 5, 5); -- returned, return, returned
insert into object_transaction_states (code, object_code, state_code, action_code, new_state_code)
values (12, 1, 7, 5, 5); -- in_manual, return, returned
insert into object_transaction_states (code, object_code, state_code, action_code, new_state_code)
values (13, 1, 8, 7, 7); -- in_editing, edit, in_manual


--for user object
insert into objects (code, name)
values (2, 'user');

-- add object states
insert into object_states (code, object_code, name)
values (1, 2, 'created');
insert into object_states (code, object_code, name)
values (2, 2, 'active');
insert into object_states (code, object_code, name)
values (3, 2, 'passive');

-- add object actions
insert into object_actions (code, object_code, name)
values (1, 2, 'create');
insert into object_actions (code, object_code, name)
values (2, 2, 'activate');
insert into object_actions (code, object_code, name)
values (3, 2, 'deactivate');
insert into object_actions (code, object_code, name)
values (4, 2, 'edit');

--add object transaction states
insert into object_transaction_states (code, object_code, state_code, action_code, new_state_code)
values (14, 2, 1, 1, 1); -- created, create, created
insert into object_transaction_states (code, object_code, state_code, action_code, new_state_code)
values (15, 2, 1, 2, 2); -- created, activate, active
insert into object_transaction_states (code, object_code, state_code, action_code, new_state_code)
values (16, 2, 2, 3, 3); -- active, deactivate, passive
insert into object_transaction_states (code, object_code, state_code, action_code, new_state_code)
values (17, 2, 3, 2, 2); -- passive, activate, active
insert into object_transaction_states (code, object_code, state_code, action_code, new_state_code)
values (18, 2, 1, 4, 1); -- created, edit, created
insert into object_transaction_states (code, object_code, state_code, action_code, new_state_code)
values (19, 2, 2, 4, 2); -- active, edit, active

--for merchant object
insert into objects (code, name)
values (3, 'merchant');

-- add object states
insert into object_states (code, object_code, name)
values (1, 3, 'created');
insert into object_states (code, object_code, name)
values (2, 3, 'active');
insert into object_states (code, object_code, name)
values (3, 3, 'passive');

-- add object actions
insert into object_actions (code, object_code, name)
values (1, 3, 'create');
insert into object_actions (code, object_code, name)
values (2, 3, 'activate');
insert into object_actions (code, object_code, name)
values (3, 3, 'deactivate');
insert into object_actions (code, object_code, name)
values (4, 3, 'edit');

--add object transaction states
insert into object_transaction_states (code, object_code, state_code, action_code, new_state_code)
values (20, 3, 1, 1, 1); -- created, create, created
insert into object_transaction_states (code, object_code, state_code, action_code, new_state_code)
values (21, 3, 1, 4, 1); -- created, edit, created
insert into object_transaction_states (code, object_code, state_code, action_code, new_state_code)
values (22, 3, 2, 4, 2); -- active, edit, active