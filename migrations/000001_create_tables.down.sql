drop table if exists object_transaction_states;
drop table if exists objects_action_protocol;
drop table if exists object_actions;
drop table if exists object_states;
drop table if exists objects;

drop table if exists merchant_accounts;
drop table if exists merchant_terminals;
drop table if exists merchant_settings;

drop table if exists terminal_accounts;
drop table if exists terminals;

drop table if exists credentials;
drop table if exists users;

drop table if exists details;
drop table if exists schedulers;
drop table if exists params;
drop table if exists commissions;
drop table if exists accounts;
drop table if exists payments;

drop table if exists card_bins;
drop table if exists card;
drop table if exists merchants;