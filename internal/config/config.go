package config

import (
	"fmt"
)

const ServiceLabel = "back_acquiring_service"

type Config struct {
	Environment string `env:"ENVIRONMENT"`
	ServiceName string `env:"SERVICE_NAME"`
	ServerIP    string `env:"SERVER_IP, default=localhost"`
	LogLevel    string `env:"LOG_LEVEL, default=debug"`
	HTTPPort    string `env:"HTTP_PORT"`
	JSONRPCPort string `env:"JSON_RPC_PORT"`

	Postgres *DB `env:",prefix=POSTGRES_"`

	RMQHost string `env:"AMQP_URL"`

	Redis *Redis `env:",prefix=REDIS_"`
}

type DB struct {
	Host     string `env:"HOST"`
	Port     int    `env:"PORT"`
	User     string `env:"USER"`
	Password string `env:"PASSWORD"`
	Database string `env:"DATABASE"`
	SSLMode  string `env:"SSLMODE"`
	TimeZone string `env:"TIME_ZONE, default=Asia/Tashkent"`
}

type Redis struct {
	URL       string `env:"URL"`
	IsCluster bool   `env:"IS_CLUSTER,default=false"`
}

func (c *DB) PostgresURL() string {
	if c.User == "" {
		return fmt.Sprintf(
			"host=%s port=%d  dbname=%s sslmode=disable",
			c.Host,
			c.Port,
			c.Database,
		)
	}

	if c.Password == "" {
		return fmt.Sprintf(
			"host=%s port=%d user=%s  dbname=%s sslmode=disable",
			c.Host,
			c.Port,
			c.User,
			c.Database,
		)
	}

	return fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		c.Host,
		c.Port,
		c.User,
		c.Password,
		c.Database,
	)
}
