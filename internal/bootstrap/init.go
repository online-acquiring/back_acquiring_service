package bootstrap

import (
	"context"
	"job/back_acquiring_service/internal/config"
	"job/back_acquiring_service/internal/drivers/cache/inmemory"
	"time"

	"github.com/allegro/bigcache"
	"go.uber.org/zap"
)

func initRedis(cfg *config.Config) (redis.UniversalClient, error) {
	l := logger.FromCtx(context.Background(), "initRedis")

	if cfg == nil || cfg.Redis.URL == "" {
		l.Error("no redis_cache url provided")

		return nil, errs.Errf(errs.ErrSourceInternal, "no redis_cache url provided")
	}

	if cfg.Redis.URL == "" {
		l.Error("config.Redis.URL is empty")
		return nil, errs.Errf(errs.ErrSourceInternal, "No redis_cache host provided")
	}

	ctx := context.TODO()

	if !cfg.Redis.IsCluster {
		opt, err := redis.ParseURL(cfg.Redis.URL)
		if err != nil {
			l.Error("redis_cache.ParseURL failed", zap.Error(err))

			return nil, errs.Errf(errs.ErrSourceInternal, err.Error())
		}

		rdb := redis.NewClient(opt)
		l.Info("Standalone Redis client initiated")

		if err := rdb.Ping(ctx).Err(); err != nil {
			l.Error("rbd.Ping failed", zap.Error(err))
			return nil, errs.Errf(errs.ErrSourceInternal, err.Error())
		}

		l.Info("Standalone Redis client pinged successfully")

		return rdb, nil
	}

	opt, err := redis.ParseClusterURL(cfg.Redis.URL)
	if err != nil {
		l.Error("redis_cache.ParseClusterURL failed", zap.Error(err))

		return nil, errs.Errf(errs.ErrSourceInternal, err.Error())
	}

	rdb := redis.NewClusterClient(opt)
	l.Info("Cluster client initiated")

	err = rdb.ForEachShard(ctx, func(ctx context.Context, c *redis.Client) error {
		if err := c.Ping(ctx).Err(); err != nil {
			l.Error("ping on shard failed", zap.Error(err))
		}

		return nil
	})

	if err != nil {
		l.Error("clusterClient.Ping error", zap.Error(err))

		return rdb, errs.Errf(errs.ErrSourceInternal, err.Error())
	}

	l.Info("Redis Cluster client shards pinged successfully")

	return rdb, nil
}

const cacheTTL = 5 // minutes
//nolint:gocritic
func initMemCache(_ config.Config, l logger.Logger) (*inmemory.Cache, func()) {
	cacheConfig := bigcache.DefaultConfig(cacheTTL * time.Minute)
	cacheConfig.CleanWindow = cacheConfig.LifeWindow
	// Number of items on ref is only 4 - we allocate for future
	cacheConfig.MaxEntriesInWindow = 200
	inMemCache := inmemory.New(cacheConfig, l)

	return inMemCache, func() { _ = inMemCache.Cleanup() }
}
