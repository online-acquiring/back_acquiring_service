package bootstrap

import (
	"job/back_acquiring_service/internal/drivers/cache/redis_cache"
	"job/back_acquiring_service/internal/usecases/accounts_action"
	"job/back_acquiring_service/internal/usecases/auth_actions"
	"job/back_acquiring_service/internal/usecases/card_renew"
	"job/back_acquiring_service/internal/usecases/card_source"
	"job/back_acquiring_service/internal/usecases/commission_action"
	"job/back_acquiring_service/internal/usecases/jobs_action"
	"job/back_acquiring_service/internal/usecases/merchant_action"
	"job/back_acquiring_service/internal/usecases/objects"
	"job/back_acquiring_service/internal/usecases/param_action"
	"job/back_acquiring_service/internal/usecases/scheduler_action"
	"job/back_acquiring_service/internal/usecases/service_sms"
	"job/back_acquiring_service/internal/usecases/terminal_action"
	"job/back_acquiring_service/internal/usecases/user_action"
)

type usecases struct {
	createPay        *pay_create.UseCase
	confirmPay       *pay_confirm.UseCase
	cards            *card_source.UseCase
	userAction       *user_action.UseCase
	merchAction      *merchant_action.UseCase
	terminalAction   *terminal_action.UseCase
	accountAction    *accounts_action.UseCase
	commissionAction *commission_action.UseCase
	paymentAction    *pay_source.UseCase
	humoAction       *service_humo.UseCase
	uzcardAction     *service_uzcard.UseCase
	refundAction     *pay_refund.UseCase
	renewCard        *card_renew.UseCase
	param            *param_action.UseCase
	scheduler        *scheduler_action.UseCase
	schJobs          *jobs_action.UseCase
	authActions      *auth_actions.UseCase
	cancelPay        *pay_cancel.UseCase
	actObject        *objects.UseCase
}

func buildUsecases(cr *cron.Cron,
	integrate integrations,
	rd *redis_cache.RedisServ,
	s dbstore.DBStore) usecases {
	humoUC := service_humo.New(integrate.humo)
	uzcardUC := service_uzcard.New(integrate.uzcard)
	otpUC := service_sms.New(integrate.iabs)
	payUC := pay_source.New(s.PaymentRepo(), s.CardRepo(), integrate.esb, s.MerchantRepo())
	objectsUC := objects.New(s.ObjectRepo())
	pRefundUC := pay_refund.New(humoUC, uzcardUC, payUC, s.ParamRepo(),
		s.BINRepo(), integrate.iabsEsb, merchant_action.New(s.MerchantRepo(), objectsUC), objectsUC)
	userUC := user_action.New(s.UserRepo(), objectsUC)
	return usecases{
		createPay: pay_create.New(humoUC, uzcardUC, otpUC, payUC, rd,
			s.BINRepo(), s.ComRepo(), objectsUC),
		confirmPay: pay_confirm.New(humoUC, uzcardUC, otpUC, payUC, rd,
			s.BINRepo(), s.MerchantTerRepo(), s.TerminalRepo(), objectsUC),
		refundAction: pRefundUC,
		cards:        card_source.New(integrate.humo, integrate.uzcard, s.BINRepo(), s.CardRepo(), otpUC, rd, s.MerchantRepo()),
		userAction:   userUC,
		cancelPay: pay_cancel.New(uzcardUC, humoUC, payUC, objectsUC,
			s.MerchantRepo(), s.TerminalRepo(), s.BINRepo(), pRefundUC, userUC),
		merchAction:      merchant_action.New(s.MerchantRepo(), objectsUC),
		terminalAction:   terminal_action.New(s.TerminalRepo()),
		accountAction:    accounts_action.New(s.AccountRepo()),
		commissionAction: commission_action.New(s.ComRepo()),
		paymentAction:    payUC,
		humoAction:       humoUC,
		uzcardAction:     uzcardUC,
		renewCard: card_renew.New(humoUC, uzcardUC, payUC, s.ParamRepo(),
			s.BINRepo(), objectsUC),
		authActions: auth_actions.New(s.MerchantSettingsRepo()),
		param:       param_action.New(s.ParamRepo()),
		scheduler:   scheduler_action.New(s.SchedulerRepo()),
		schJobs:     jobs_action.New(cr, s.TerminalRepo(), humoUC, scheduler_action.New(s.SchedulerRepo())),
		actObject:   objectsUC,
	}
}
