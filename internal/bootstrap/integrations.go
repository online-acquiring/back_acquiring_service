package bootstrap

import (
	"job/back_acquiring_service/internal/config"
	"job/back_acquiring_service/internal/drivers/services/synch/esb"
	"job/back_acquiring_service/internal/drivers/services/synch/humo"
	"job/back_acquiring_service/internal/drivers/services/synch/iabs"
	"job/back_acquiring_service/internal/drivers/services/synch/uzcard"
)

type integrations struct {
	humo    *humo.Client
	uzcard  *uzcard.Client
	iabs    *iabs.Client
	iabsEsb *iabs.ClientIabs
	esb     *esb.Client
}

func buildIntegrations(cfg config.Config) integrations {
	return integrations{
		humo:    humo.New(cfg),
		uzcard:  uzcard.New(cfg),
		iabs:    iabs.New(cfg),
		iabsEsb: iabs.NewIabsEsb(cfg),
		esb:     esb.New(cfg),
	}
}
