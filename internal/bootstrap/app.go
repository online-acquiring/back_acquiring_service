package bootstrap

import (
	"context"
	"fmt"
	"job/back_acquiring_service/internal/drivers/cache/inmemory"
	"job/back_acquiring_service/internal/drivers/cache/redis_cache"
	"job/back_acquiring_service/internal/drivers/dbstore"
	"job/back_acquiring_service/internal/gateways/jsonrpc"
	"job/back_acquiring_service/internal/gateways/rest"
	"job/back_acquiring_service/internal/usecases/jobs_action"
	"net/http"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq" // postgresql lib
)

const gracefulDeadline = 5 * time.Second

type App struct {
	db       *sqlx.DB
	store    dbstore.DBStore
	teardown []func()
	rest     *http.Server
	rd       *redis_cache.RedisServ
	inMemory *inmemory.Cache
	jsonRPC  *http.Server
	jobs     jobs_action.UseCase
}

func New(cfg config.Config) *App {
	teardown := make([]func(), 0)
	// Setting UP logger
	globalLogger := logger.New(cfg.LogLevel, "acq-back-service")
	teardown = append(teardown, func() { _ = logger.Cleanup() })
	globalLogger.Info("Logger initialized")

	log := logger.FromCtx(context.Background(), "bootstrap.New")
	// Подключение к Cache
	rc, err := initRedis(&cfg)
	if err != nil {
		panic(err)
	}

	rd := redis_cache.New(log, rc)

	log.Info("Redis connection established.")

	// Подключение к БД
	db, err := sqlx.Connect("postgres", cfg.Postgres.PostgresURL())
	if err != nil {
		panic(err)
	}

	log.Info("Database connection established")
	teardown = append(teardown, func() {
		log.Info("Database connection closing...")
		if err := db.Close(); err != nil {
			log.Error(err.Error())
		}

		log.Info("Database connection closed")
	})

	storage := dbstore.New(log, db)

	memCache, t := initMemCache(cfg, log)
	teardown = append(teardown, t)

	integrate := buildIntegrations(cfg)
	cr := cron.New()

	useCases := buildUsecases(cr, integrate, rd, *storage)

	httpSrv := rest.New(cfg.HTTPPort, log, &cfg, useCases.userAction,
		useCases.merchAction,
		useCases.terminalAction,
		useCases.accountAction,
		useCases.commissionAction,
		useCases.paymentAction,
		useCases.humoAction,
		useCases.uzcardAction,
		useCases.refundAction,
		useCases.renewCard,
		useCases.param,
		useCases.scheduler,
		useCases.schJobs,
		useCases.actObject,
	)
	teardown = append(teardown, func() {
		log.Info("HTTP is shutting down")
		ctxShutDown, cancel := context.WithTimeout(context.Background(), gracefulDeadline)
		defer cancel()
		if err = httpSrv.Shutdown(ctxShutDown); err != nil {
			log.Error(fmt.Sprintf("server Shutdown Failed:%s", err))
			if err == http.ErrServerClosed {
				err = nil
			}
			return
		}

		log.Info("HTTP is shut down")
	})

	jsonRPC := jsonrpc.New(cfg.JSONRPCPort, log, cfg, useCases.createPay,
		useCases.confirmPay, useCases.cancelPay, useCases.cards, useCases.authActions,
		useCases.paymentAction)
	teardown = append(teardown, func() {
		log.Info("Json RPC is shutting down")
		ctxShutDown, cancel := context.WithTimeout(context.Background(), gracefulDeadline)
		defer cancel()
		if err = jsonRPC.Shutdown(ctxShutDown); err != nil {
			log.Error(fmt.Sprintf("server Shutdown Failed:%s", err))
			if err == http.ErrServerClosed {
				err = nil
			}

			return
		}

		log.Info("HTTP is shut down")
	})

	app := App{
		teardown: teardown,
		rest:     httpSrv,
		db:       db,
		store:    *storage,
		rd:       rd,
		inMemory: memCache,
		jsonRPC:  jsonRPC,
	}
	return &app
}

func (app *App) Run(ctx context.Context) {
	go func() {
		if err := app.rest.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			panic(err)
		}
	}()

	go func() {
		if err := app.jsonRPC.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			panic(err)
		}
	}()
	<-ctx.Done()
	go func(c context.Context) {
		app.jobs.RestartJobs(c)
	}(ctx)
	<-ctx.Done()
	for i := range app.teardown {
		app.teardown[i]()
	}
}
