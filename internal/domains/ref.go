package domains

const (
	CurrencyCodeUZS = "860"
	CurrencyCodeUSD = "840"
)

const (
	PSCodeHUMO   PSCode = "HUMO"
	PSCodeUZCARD PSCode = "UZCARD"
)

func (c PSCode) ToString() string {
	return string(c)
}

const (
	ObjectCodePayment ObjectCode = iota + 1
	ObjectCodeUser
	ObjectCodeMerchant
)

// payment actions
const (
	PaymentActionCreate Action = iota + 1
	PaymentActionHold
	PaymentActionConfirm
	PaymentActionCancel
	PaymentActionReturn
	PaymentActionError
	PaymentActionToEdit
	PaymentActionToManual
	PaymentActionToBalance
)

// payment states
const (
	PaymentStateCreated State = iota + 1
	PaymentStateHolded
	PaymentStateConfirmed
	PaymentStateCanceled
	PaymentStateReturned
	PaymentStateRejected
	PaymentStateInManual
	PaymentStateInEditing
	PaymentStateInBalance
)

// user actions
const (
	UserActionCreate Action = iota + 1
	UserActionActivate
	UserActionDeactivate
	UserActionEdit
)

// user states
const (
	UserStateCreated State = iota + 1
	UserStateActive
	UserStatePassive
)

// merchant states
const (
	MerchantStateCreated State = iota + 1
	MerchantStateActive
	MerchantStatePassive
)

// merchant actions
const (
	MerchantActionCreate Action = iota + 1
	MerchantActionActivate
	MerchantActionDeactivate
	MerchantActionEdit
)

const (
	SuperAdminRole Role = iota
	AdminRole
	MerchantRole
	MerchantUserRole
	SystemUserRole
)

const (
	FeeFromClient Fee = iota
	FeeFromMerchant
	FeeFromClientAndMerchant
)

const (
	PaymentTypeDebit PaymentType = iota
	PaymentTypeReversal
)

const (
	CardStatusActive   = "0"
	CardStatusNoActive = "-1"
)

const (
	TypePercent = iota
	TypeMoney
)

const (
	Allowed    = "ALLOWED"
	NotAllowed = "NOT_ALLOWED"
	ParamY     = "Y"
	ParamN     = "N"
)
const (
	IsAllowedRefundHumo   = "IS_ALLOWED_REFUND_HUMO"
	IsAllowedRefundUzcard = "IS_ALLOWED_REFUND_UZCARD"
	IsAllowedAddHumo      = "IS_ALLOWED_ADD_HUMO"
	IsAllowedAddUzcard    = "IS_ALLOWED_ADD_UZCARD"
)

const (
	JobStatusRunning = 1
	JobStatusStopped = 0
	JobStatusFailed  = -1
)

const (
	ObjectCodeMerchantText = "Merchant"
	ObjectCodePaymentText  = "Payment"
	ObjectCodeUserText     = "User"
)

const (
	PaymentStateCreatedText   = "Создан"
	PaymentStateHoldedText    = "Холдирован"
	PaymentStateConfirmedText = "Потверждён"
	PaymentStateCanceledText  = "Отменен"
	PaymentStateReturnedText  = "Возвращён"
	PaymentStateRejectedText  = "Откланён"
	PaymentStateInManualText  = "Вручную"
	PaymentStateInEditingText = "Возврать невозможно (карта блок)"
	PaymentStateInBalanceText = "В балансе"
)

const (
	PaymentActionCreateText    = "Создать"
	PaymentActionHoldText      = "Холдировать"
	PaymentActionConfirmText   = "Потвер"
	PaymentActionCancelText    = "Отменить"
	PaymentActionReturnText    = "Возвращать"
	PaymentActionErrorText     = "Ошибка"
	PaymentActionToEditText    = "Починить карта блок"
	PaymentActionToManualText  = "Ручной возврат"
	PaymentActionToBalanceText = "Отправить в баланс"
)
