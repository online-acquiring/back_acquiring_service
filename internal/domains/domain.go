package domains

type Param struct {
	ID    ID
	Code  string
	Name  string
	Value string
	CreateUpdateTime
}

type Scheduler struct {
	ID         ID
	Code       string
	Name       string
	Expression string
	Status     int
	JobID      int
	CreateUpdateTime
}

type SchedulerParam struct {
	TaskName     string
	Interval     int8
	IntervalForm int8
	Action       int8
	RunTime      string
}
