package domains

import "time"

type ID string

type ObjectCode int8

type PSCode string

type State int8

type Action int8

type DateTime time.Time

type Role int8

type Fee int8

type PaymentType int8

type Money int64

type AccType int8

type ComType int8

type ComValue float64

type CreateUpdateTime struct {
	CreatedAt time.Time
	UpdatedAt time.Time
}
