package domains

import "time"

type Credential struct {
	ID        ID
	Password  string
	ExpiredAt time.Time
	AddedBy   int8
	CreateUpdateTime
}

type User struct {
	ID       ID
	Merchant Merchant
	Name     string
	State    int8
	Login    string
	Credential
	Role        Role
	Email       string
	PhoneNumber string
	CreateUpdateTime
}

type SMS struct {
	Phone   string
	Message string
	SmsCode string
}
