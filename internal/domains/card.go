package domains

type Terminal struct {
	ID          ID
	MerchantID  string
	Name        string
	TerminalNum string
	MerchantNum string
	Port        int
	PSCode      PSCode
	TerminalAcc []string
	Com         float64
}

type TerminalAccount struct {
	ID          ID
	AccountType AccType
	Terminal    Terminal
	Account     Account
	State       State
	Dates       CreateUpdateTime
}

type Bin struct {
	Code   string
	PSCode PSCode
}

type Card struct {
	ID       string
	Status   string
	Phone    string
	Balance  float64
	Number   string
	Expiry   string
	CardType string
	Owner    string
}

type PSTransaction struct {
	PayCard      Card
	PayTerminal  Terminal
	PayDetail    PayerData
	Amount       int64
	ExternalID   string
	ProcessingID string
	RefNumber    string
	Status       string
	LoanID       string
}

type Reconciliation struct {
	PaymentID string
	Action    string
}

type PayCard struct {
	ID        string
	BIN       string
	MaskedPAN string
	Expiry    string
}
