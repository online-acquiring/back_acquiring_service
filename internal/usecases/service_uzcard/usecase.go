package service_uzcard //nolint

import (
	"context"

	"go.uber.org/zap"
)

type DriverUzcard interface {
	GetCardByPan(ctx context.Context, number, expire string) (*domains.Card, error)
	CardNewOTP(ctx context.Context, pan, exDate, hash, phone string, tempID int) (string, error)
	CardNewVerify(ctx context.Context, pSignID, cCode string) (*domains.Card, error)
	GetCardByPhone(ctx context.Context, phone string) ([]domains.Card, error)
	GetCardID(ctx context.Context, number string) (*domains.Card, error)
	GetCardBalance(ctx context.Context, id string) (*domains.Card, error)
	GetTransactionByExt(ctx context.Context, transaction domains.PSTransaction) (domains.PSTransaction, error)
	CancelTransaction(ctx context.Context, transaction domains.PSTransaction) (domains.PSTransaction, error)
	ReversalTransactionV1(ctx context.Context, transaction domains.PSTransaction) (domains.PSTransaction, error)
	ReversalPartial(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error)
	ReversalToCard(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error)
	AuthTransaction(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error)
	ConfirmTransaction(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error)
}

type UseCase struct {
	uzcard DriverUzcard
}

func New(uzcard DriverUzcard) *UseCase {
	return &UseCase{
		uzcard: uzcard,
	}
}

func (uc *UseCase) CardNewOTP(ctx context.Context, pan, exDate, hash, phone string, tempID int) (string, error) {
	var l = logger.FromCtx(ctx, "service_uzcard.CardNewOTP")
	pSignID, err := uc.uzcard.CardNewOTP(ctx, pan, exDate, hash, phone, tempID)
	if err != nil {
		l.Error("uc.uzcard:CardNewOTP failed", zap.Error(err))

		return "", err
	}

	return pSignID, nil
}

func (uc *UseCase) CardNewVerify(ctx context.Context, pSignID, cCode string) (*domains.Card, error) {
	var l = logger.FromCtx(ctx, "service_uzcard.CardNewVerify")
	cardOtpVer, err := uc.uzcard.CardNewVerify(ctx, pSignID, cCode)
	if err != nil {
		l.Error("uc.uzcard:CardNewVerify failed", zap.Error(err))

		return nil, err
	}

	return cardOtpVer, nil
}

func (uc *UseCase) GetCardByPhone(ctx context.Context, phone string) ([]domains.Card, error) {
	var l = logger.FromCtx(ctx, "service_uzcard.GetCardByPhone")
	cards, err := uc.uzcard.GetCardByPhone(ctx, phone)
	if err != nil {
		l.Error("uc.uzcard.GetCardByPhone failed", zap.Error(err))

		return nil, err
	}

	return cards, nil
}

func (uc *UseCase) GetCardID(ctx context.Context, number string) (*domains.Card, error) {
	var l = logger.FromCtx(ctx, "service_uzcard.GetCardID")
	card, err := uc.uzcard.GetCardID(ctx, number)
	if err != nil {
		l.Error("uc.uzcard.GetCardID failed", zap.Error(err))

		return nil, err
	}

	return card, nil
}

func (uc *UseCase) GetCardBalance(ctx context.Context, id string) (*domains.Card, error) {
	var l = logger.FromCtx(ctx, "service_uzcard.GetCardBalance")
	card, err := uc.uzcard.GetCardBalance(ctx, id)
	if err != nil {
		l.Error("uc.uzcard.GetCardBalance failed", zap.Error(err))

		return nil, err
	}

	return card, nil
}

func (uc *UseCase) GetTransactionByExt(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error) {
	var l = logger.FromCtx(ctx, "service_uzcard.GetTransactionByExt")
	tran, err := uc.uzcard.GetTransactionByExt(ctx, tran)
	if err != nil {
		l.Error("uc.uzcard.GetTransactionByExt failed", zap.Error(err))

		return domains.PSTransaction{}, err
	}

	return tran, nil
}

func (uc *UseCase) CancelTransaction(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error) {
	var l = logger.FromCtx(ctx, "service_uzcard.CancelTransaction")
	tran, err := uc.uzcard.CancelTransaction(ctx, tran)
	if err != nil {
		l.Error("uc.uzcard.CancelTransaction failed", zap.Error(err))

		return domains.PSTransaction{}, err
	}

	return tran, nil
}

// Variant 1
func (uc *UseCase) ReversalTransactionV1(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error) {
	var l = logger.FromCtx(ctx, "service_uzcard.ReversalTransactionV1")
	tran, err := uc.uzcard.ReversalTransactionV1(ctx, tran)
	if err != nil {
		l.Error("uc.uzcard:ReversalTransactionV1 failed", zap.Error(err))

		return domains.PSTransaction{}, err
	}

	return tran, nil
}

// Variant 2
func (uc *UseCase) ReversalPartial(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error) {
	var l = logger.FromCtx(ctx, "service_uzcard.ReversalPartial")
	tran, err := uc.uzcard.ReversalPartial(ctx, tran)
	if err != nil {
		l.Error("uc.uzcard:ReversalPartial failed", zap.Error(err))

		return domains.PSTransaction{}, err
	}

	return tran, nil
}
func (uc *UseCase) ReversalToCard(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error) {
	var l = logger.FromCtx(ctx, "service_uzcard.ReversalToCard")
	tran, err := uc.uzcard.ReversalToCard(ctx, tran)
	if err != nil {
		l.Error("uc.uzcard:ReversalToCard failed", zap.Error(err))

		return domains.PSTransaction{}, err
	}

	return tran, nil
}

func (uc *UseCase) AuthTransaction(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error) {
	var l = logger.FromCtx(ctx, "service_uzcard.AuthTransaction")
	tran, err := uc.uzcard.AuthTransaction(ctx, tran)
	if err != nil {
		l.Error("uc.uzcard:AuthTransaction failed", zap.Error(err))

		return domains.PSTransaction{}, err
	}

	return tran, nil
}

func (uc *UseCase) ConfirmTransaction(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error) {
	var l = logger.FromCtx(ctx, "service_uzcard.ConfirmTransaction")
	tran, err := uc.uzcard.ConfirmTransaction(ctx, tran)
	if err != nil {
		l.Error("uc.uzcard:ConfirmTransaction failed", zap.Error(err))

		return domains.PSTransaction{}, err
	}

	return tran, nil
}

func (uc *UseCase) GetCardByPan(ctx context.Context, number, expire string) (*domains.Card, error) {
	var l = logger.FromCtx(ctx, "service_uzcard.GetCardByPan")
	card, err := uc.uzcard.GetCardByPan(ctx, number, expire)
	if err != nil {
		l.Error("uc.uzcard:GetCardByPan failed", zap.Error(err))

		return nil, err
	}

	return card, nil
}
