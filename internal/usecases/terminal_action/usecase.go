package terminal_action //nolint

import (
	"context"

	"go.uber.org/zap"
)

//nolint:dupl
type DriverTerminal interface {
	CreateTerminal(ctx context.Context, ter domains.Terminal) (domains.Terminal, error)
	GetAllTerminals(ctx context.Context) ([]domains.Terminal, error)
	GetTerminal(ctx context.Context, ter domains.Terminal) (domains.Terminal, error)
	UpdateTerminal(ctx context.Context, ter domains.Terminal) (domains.Terminal, error)

	GetTerminalByType(ctx context.Context, termType string) (domains.Terminal, error)
	CreateTerAcc(ctx context.Context, ter domains.TerminalAccount) (domains.TerminalAccount, error)
}

//nolint:dupl
type terminalSource interface {
	Create(ctx context.Context, ter domains.Terminal) (domains.Terminal, error)
	GetAll(ctx context.Context) ([]domains.Terminal, error)
	Get(ctx context.Context, ter domains.Terminal) (domains.Terminal, error)
	UpdateTerminal(ctx context.Context, ter domains.Terminal) (domains.Terminal, error)

	GetTerminalByType(ctx context.Context, termType string) (domains.Terminal, error)
	CreateTerminalAccount(ctx context.Context, ter domains.TerminalAccount) (domains.TerminalAccount, error)
}

type UseCase struct {
	aSource terminalSource
}

func New(aSource terminalSource) *UseCase {
	return &UseCase{
		aSource: aSource,
	}
}

func (uc *UseCase) CreateTerminal(ctx context.Context, acc domains.Terminal) (domains.Terminal, error) {
	var l = logger.FromCtx(ctx, "terminal_action.CreateTerminal")
	ter, err := uc.aSource.Create(ctx, acc)
	if err != nil {
		l.Error("uc.aSource.Create failed", zap.Error(err))

		return domains.Terminal{}, err
	}

	return ter, nil
}

func (uc *UseCase) GetAllTerminals(ctx context.Context) ([]domains.Terminal, error) {
	var l = logger.FromCtx(ctx, "terminal_action.GetAllTerminals")
	ters, err := uc.aSource.GetAll(ctx)
	if err != nil {
		l.Error("uc.aSource.GetAll failed", zap.Error(err))

		return nil, err
	}

	return ters, nil
}

func (uc *UseCase) GetTerminal(ctx context.Context, acc domains.Terminal) (domains.Terminal, error) {
	var l = logger.FromCtx(ctx, "terminal_action.GetTerminal")
	ter, err := uc.aSource.Get(ctx, acc)
	if err != nil {
		l.Error("uc.aSource.Get failed", zap.Error(err))

		return domains.Terminal{}, err
	}

	return ter, nil
}

func (uc *UseCase) UpdateTerminal(ctx context.Context, ter domains.Terminal) (domains.Terminal, error) {
	var l = logger.FromCtx(ctx, "terminal_action.UpdateTerminal")
	ter, err := uc.aSource.UpdateTerminal(ctx, ter)
	if err != nil {
		l.Error("uc.aSource.UpdateTerminal failed", zap.Error(err))

		return domains.Terminal{}, err
	}

	return ter, nil
}

func (uc *UseCase) CreateTerAcc(ctx context.Context, ter domains.TerminalAccount) (domains.TerminalAccount, error) {
	var l = logger.FromCtx(ctx, "terminal_action.CreateTerAcc")
	t, err := uc.aSource.CreateTerminalAccount(ctx, ter)
	if err != nil {
		l.Error("uc.aSource.CreateTerminalAccount failed", zap.Error(err))

		return domains.TerminalAccount{}, err
	}

	return t, nil
}

func (uc *UseCase) GetTerminalByType(ctx context.Context, termType string) (domains.Terminal, error) {
	var l = logger.FromCtx(ctx, "terminal_action.GetTerminalByType")
	t, err := uc.aSource.GetTerminalByType(ctx, termType)
	if err != nil {
		l.Error("uc.aSource.GetTerminalByType failed", zap.Error(err))

		return domains.Terminal{}, err
	}

	return t, nil
}
