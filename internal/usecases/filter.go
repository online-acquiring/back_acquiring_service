package usecases

import (
	"strings"
)

func GetBIN(number string, bins []domains.Bin) (bin domains.Bin, exists bool) {
	for i := range bins {
		if !strings.HasPrefix(number, bins[i].Code) {
			continue
		}
		bin = bins[i]
		exists = true
		break
	}
	return
}

//nolint:exhaustive
func CheckBalance(clientB, cardB domains.Money, com domains.Commission) bool {
	switch com.FeeType {
	case 0:
		return domains.ComValue(clientB)*com.Value+domains.ComValue(clientB) >= domains.ComValue(cardB)
	case 1:
	default:
	}

	return false
}
