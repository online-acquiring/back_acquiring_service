package jobs_action //nolint

import (
	"context"

	"go.uber.org/zap"
)

// neede method to
func (uc *UseCase) Reconcile() {
	var l = logger.FromCtx(context.Background(), "jobs_action.Reconcile")
	l.Info("scheduler:reco:started")
	err := uc.RecoTerminals(context.TODO())
	if err != nil {
		l.Error("uc.RecoTerminals scheduler reco failed", zap.Error(err))
	} else {
		l.Info("scheduler:reco:ended")
	}
}
