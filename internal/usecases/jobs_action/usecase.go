package jobs_action //nolint

import (
	"context"
	"errors"

	"go.uber.org/zap"
)

const (
	JobCodeReco = "RECONSILATION"
)

// jobs to implement
type DriverJobs interface {
	CreateRecoJob(exp string) (entryID int, err error)
}

type sourceTerminal interface {
	GetTerminalByType(ctx context.Context, termType string) (domains.Terminal, error)
}

type sourceScheduler interface {
	CreateScheduler(ctx context.Context, sch domains.Scheduler) (domains.Scheduler, error)
	GetScheduler(ctx context.Context, id string) (domains.Scheduler, error)
	GetAllSchedulers(ctx context.Context) ([]domains.Scheduler, error)
	UpdateScheduler(ctx context.Context, sch domains.Scheduler) error
	UpdateSchedulerState(ctx context.Context, scheduler domains.Scheduler) error
}

type sourceHumo interface {
	Reconciliation(ctx context.Context, terminal domains.Terminal) (domains.Reconciliation, error)
	ConfirmTransaction(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error)
}
type UseCase struct {
	crn       *cron.Cron
	sourceTer sourceTerminal
	humo      sourceHumo
	sourceSch sourceScheduler
}

func New(crn *cron.Cron, t sourceTerminal, h sourceHumo, s sourceScheduler) *UseCase {
	return &UseCase{
		crn:       crn,
		sourceTer: t,
		humo:      h,
		sourceSch: s,
	}
}

// create any job method to implement and call the needed method
func (uc *UseCase) CreateRecoJob(exp string) (entryID int, err error) {
	var l = logger.FromCtx(context.Background(), "jobs_action.CreateRecoJob")
	id, err := uc.crn.AddFunc(exp, uc.Reconcile)
	if err != nil {
		l.Error("uc.crn.AddFunc failed")
	}

	entryID = int(id)
	uc.crn.Start()

	return
}

func (uc *UseCase) CreateJob(code, exp string) (entryID int, err error) {
	switch code {
	case JobCodeReco:
		return uc.CreateRecoJob(exp)
	default:
		return 0, errors.New("Нереализованный код планировщика - " + code)
	}
}

func (uc *UseCase) RemoveJob(entryID int) {
	uc.crn.Remove(cron.EntryID(entryID))
}

// configuration for re-running
func (uc *UseCase) RestartJobs(ctx context.Context) {
	var l = logger.FromCtx(ctx, "jobs_action.RestartJobs")
	jobs, err := uc.sourceSch.GetAllSchedulers(ctx)
	if err != nil {
		l.Warn("uc.sourceSch.GetAllSchedulers Failed restart jobs,reason:", zap.Error(err))
	}

	for i := range jobs {
		if jobs[i].Status != domains.JobStatusRunning {
			continue
		}
		uc.RemoveJob(jobs[i].JobID)
		jobID, err := uc.CreateJob(jobs[i].Code, jobs[i].Expression)
		if err != nil {
			l.Warn("uc.CreateJob: Failed create job,reason:", zap.Error(err))
		}

		err = uc.sourceSch.UpdateSchedulerState(ctx, domains.Scheduler{ID: jobs[i].ID, Status: domains.JobStatusRunning, JobID: jobID})
		if err != nil {
			l.Warn("uc.sourceSch.UpdateSchedulerState Failed update job,reason:", zap.Error(err))
		}
	}
}
