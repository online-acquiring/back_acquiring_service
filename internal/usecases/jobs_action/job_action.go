package jobs_action //nolint

import (
	"context"

	"go.uber.org/zap"
)

func (uc *UseCase) RecoTerminals(ctx context.Context) error {
	// terminallarni reco qilish kerak
	go func(ctx context.Context) {
		var l = logger.FromCtx(ctx, "jobs_action.RecoTerminals")
		terminal, err := uc.sourceTer.GetTerminalByType(ctx, domains.PSCodeHUMO.ToString())
		if err != nil {
			l.Error("uc.sourceTer.GetTerminalByType failed", zap.Error(err))

			return
		}

		reco, err := uc.humo.Reconciliation(ctx, terminal)
		if err != nil {
			l.Error("uc.humo.Reconciliation failed", zap.Error(err))

			return
		}

		// balki tekshirish kerak
		tran, err := uc.humo.ConfirmTransaction(ctx, domains.PSTransaction{ProcessingID: reco.PaymentID})
		if err != nil {
			l.Error("uc.humo.ConfirmTransaction failed", zap.Error(err),
				zap.String("ref-number", tran.RefNumber))
		}
		// xatolik bo'sa keyinchalik qarb ketish kerak
		l.Info("controller:reco:end:RRN-" + tran.RefNumber)
	}(ctx)

	return nil
}

func (uc *UseCase) ImplementNewMethodHere(ctx context.Context) error {
	return nil
}
