package card_renew //nolint

import (
	"context"
	"errors"

	"go.uber.org/zap"
)

type DriverRenewCard interface {
	RenewCard(ctx context.Context, paymentID string, cardNumber, cardExpire string, card *domains.Card) (customErr errs.Error)
}
type sourceCardBin interface {
	GetCardTypeByNumber(ctx context.Context, number string) (domains.Bin, error)
}
type sourceParam interface {
	GetParamByCode(ctx context.Context, code string) (domains.Param, error)
}
type humoSource interface {
	GetCardMaskedPAN(ctx context.Context, cardNumber string) (*domains.Card, error)
}
type uzcardSource interface {
	GetCardByPan(ctx context.Context, number, expire string) (*domains.Card, error)
	GetCardBalance(ctx context.Context, id string) (*domains.Card, error)
}

type paymentSource interface {
	UpdateCardByID(ctx context.Context, pay domains.Payment) error
	Get(ctx context.Context, pay domains.Payment) (domains.Payment, error)
	GetPaymentByID(ctx context.Context, payment domains.Payment) (domains.Payment, error)
}

type objectSource interface {
	CheckState(ctx context.Context, obj domains.ObjectTranState) (domains.State, error)
	AddActionProtocol(ctx context.Context, p domains.ObjectActionProtocol) (domains.ObjectActionProtocol, error)
}

func New(humo humoSource, uzcard uzcardSource,
	pay paymentSource, param sourceParam, bin sourceCardBin, obj objectSource) *UseCase {
	return &UseCase{
		humoSource:    humo,
		uzcardSource:  uzcard,
		paySource:     pay,
		paramSource:   param,
		cardBinSource: bin,
		objSource:     obj,
	}
}

type UseCase struct {
	cardBinSource sourceCardBin
	paramSource   sourceParam
	humoSource    humoSource
	uzcardSource  uzcardSource
	objSource     objectSource
	paySource     paymentSource
}

func (uc *UseCase) RenewCard(ctx context.Context, paymentID, cardNumber, cardExpire string, card *domains.Card) (customError errs.Error) {
	var l = logger.FromCtx(ctx, "card_renew.RenewCard")
	pay, reqObject, customError := renewCard(ctx, uc, l, paymentID, cardNumber, cardExpire, card)
	var newState domains.State

	if reqObject == nil {
		newState = domains.PaymentStateInManual
	} else {
		newState = reqObject.NewState
	}

	objAction := &domains.ObjectActionProtocol{
		ObjectID: domains.ID(paymentID),
		UserID:   domains.ID(ctx.Value("user_id").(string)),
		Message:  customError.Message,
		ObjectTranState: domains.ObjectTranState{
			Object: domains.Object{
				Code: domains.ObjectCodePayment,
			},
			State:    pay.State,
			Action:   domains.PaymentActionToEdit,
			NewState: newState,
		},
	}

	_, err := uc.objSource.AddActionProtocol(ctx, *objAction)
	if err != nil {
		customError.Err = err
		customError.Code = errs.ErrorCodeInternalServer
		customError.Message = errs.InternalError.Message

		return //nolint
	}

	return customError
}

func renewCard(ctx context.Context, uc *UseCase, l *zap.Logger, paymentID string, cardNumber string, //nolint
	cardExpire string, card *domains.Card) (pay domains.Payment, reqObj *domains.ObjectTranState, customError errs.Error) {
	pay, err := uc.paySource.GetPaymentByID(ctx, domains.Payment{ID: domains.ID(paymentID)})
	if err != nil {
		l.Error("uc.pay.Get failed", zap.Error(err))

		customError.Err = err
		customError.Code = errs.ErrorCodeInternalServer
		customError.Message = errs.InternalError.Message

		return //nolint
	}

	reqObject := &domains.ObjectTranState{
		Object: domains.Object{
			Code: domains.ObjectCodePayment,
		},
		State:  pay.State,
		Action: domains.PaymentActionToEdit,
	}

	if reqObject.NewState, err = uc.objSource.CheckState(ctx, *reqObject); err != nil {
		l.Error("uc.objSource.CheckState failed", zap.Any("new_state", reqObject.NewState))

		customError.Err = errors.New("this action is not available")
		customError.Code = errs.ErrorCodeWrongAction
		customError.Message = errs.WrongAction.Message

		return //nolint
	}

	cardType, errG := uc.cardBinSource.GetCardTypeByNumber(ctx, cardNumber)
	if errG != nil {
		l.Error("uc.cardBinSource.GetCardTypeByNumber failed", zap.Error(errG))

		customError.Err = errG
		customError.Code = errs.ErrorCodeParamsNotFound
		customError.Message = errs.ParamsNotFound.Message

		return //nolint
	}

	switch cardType.PSCode {
	case domains.PSCodeHUMO:
		param, err := uc.paramSource.GetParamByCode(ctx, domains.IsAllowedAddHumo)
		if err != nil {
			l.Error("uc.paramSource.GetParamByCode failed", zap.Error(err))

			customError.Err = err
			customError.Code = errs.ErrorCodeParamsNotFound
			customError.Message = errs.ParamsNotFound.Message

			return //nolint
		}

		if param.Value != domains.Allowed {
			l.Error("param.Value != domains.Allowed failed", zap.String("param.Value", param.Value))

			customError.Err = err
			customError.Code = errs.ErrorCodeActionClosed
			customError.Message = errs.ActionClosed.Message

			return //nolint
		}

		card, err = uc.humoSource.GetCardMaskedPAN(ctx, cardNumber)
		if err != nil {
			l.Error("uc.humoSource.GetCardMaskedPAN failed", zap.Error(err))

			customError.Err = err
			customError.Code = errs.ErrorCodeRemoteFail
			customError.Message = errs.RemoteServiceFail.Message

			return //nolint
		}

	case domains.PSCodeUZCARD:
		param, err := uc.paramSource.GetParamByCode(ctx, domains.IsAllowedAddUzcard)
		if err != nil {
			l.Error("uc.paramSource.GetParamByCode failed", zap.Error(err))
			customError.Err = err
			customError.Code = errs.ErrorCodeParamsNotFound
			customError.Message = errs.ParamsNotFound.Message

			return //nolint
		}

		if param.Value != domains.Allowed {
			l.Error("param.Value != domains.Allowed failed", zap.String("param.Value", param.Value))

			customError.Err = err
			customError.Code = errs.ErrorCodeActionClosed
			customError.Message = errs.ActionClosed.Message

			return //nolint
		}

		card, err = uc.uzcardSource.GetCardByPan(ctx, cardNumber, cardExpire)

		if err != nil {
			l.Error("uc.uzcardSource.GetCardByPan failed", zap.Error(err))
			customError.Err = err
			customError.Code = errs.ErrorCodeRemoteFail
			customError.Message = errs.RemoteServiceFail.Message

			return //nolint
		}

		if card.Status != domains.CardStatusActive {
			l.Error("card.Status != domains.CardStatusActive failed", zap.String("card.Status", card.Status))
			customError.Err = err
			customError.Code = errs.ErrorCodeRemoteFail
			customError.Message = errs.RemoteServiceFail.Message

			return //nolint
		}
	}

	// TODO karta almashgach to'lovdagi status
	// qaysi holatga o'zgaradi shunda, tekshib qo'yishim kerak state.ni
	// va stateni ham o'zgartirishim kerak
	err = uc.paySource.UpdateCardByID(ctx, domains.Payment{
		ID: domains.ID(paymentID),
		Card: domains.PayCard{
			ID:        card.ID,
			MaskedPAN: helpers.ConvertMask(cardNumber),
			Expiry:    card.Expiry,
			BIN:       cardNumber[:4],
		},
		State: reqObject.NewState,
	})
	if err != nil {
		l.Error("uc.paySource.UpdateCardByID failed", zap.Error(err))

		customError.Err = err
		customError.Code = errs.ErrorCodeInternalServer
		customError.Message = errs.InternalError.Message

		return //nolint
	}

	return pay, reqObject, errs.Error{
		Err:  nil,
		Code: 0,
	}
}
