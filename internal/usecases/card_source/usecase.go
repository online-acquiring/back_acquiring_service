package card_source //nolint

import (
	"context"
	"crypto/sha256"
	"strings"
	"sync"
	"time"

	"go.uber.org/zap"
)

//go:generate mockgen -destination=mock.go -source=usecase.go -package=card_source

const (
	two  = 2
	five = 5
)

type binSource interface {
	GetCardTypeByNumber(ctx context.Context, number string) (domains.Bin, error)
}

type mrSource interface {
	CreateMerchantCard(ctx context.Context, t domains.MerchantCard) (domains.MerchantCard, error)
	GetMerCard(ctx context.Context, cardID, mrID string) bool
}

type dbSource interface {
	Create(ctx context.Context, card domains.PayCard) (domains.PayCard, error)
	GetCardPS(ctx context.Context, id string) (domains.PSCode, string, string, error)
}

type cardSource interface {
	GetCardByPhone(ctx context.Context, phone string) ([]domains.Card, error)
	GetCardMaskedPAN(ctx context.Context, cardNumber string) (*domains.Card, error)
	GetCardNumber(ctx context.Context, cardID string) (*domains.Card, error)
	GetCardBalance(ctx context.Context, number string) (*domains.Card, error)
	GetCardID(ctx context.Context, number string) (*domains.Card, error)
	GetCardByPan(ctx context.Context, number, expire string) (*domains.Card, error)
}

type otpSource interface {
	SendSMS(ctx context.Context, phone string) (string, error)
}

type cacheSource interface {
	SetData(ctx context.Context, key string, data interface{}) error
	SetWithDuration(ctx context.Context, key string, data interface{}, duration time.Duration) error
	GetData(ctx context.Context, key string, resp interface{}) (err error)
}

type UseCase struct {
	humo, uzcard cardSource
	bin          binSource
	dbCard       dbSource
	otp          otpSource
	cache        cacheSource
	mr           mrSource
}

func New(humo, uzcard cardSource, bin binSource, dbCard dbSource, otp otpSource, cache cacheSource, mr mrSource) *UseCase {
	return &UseCase{
		humo:   humo,
		uzcard: uzcard,
		bin:    bin,
		dbCard: dbCard,
		otp:    otp,
		cache:  cache,
		mr:     mr,
	}
}

func (uc *UseCase) GetAllCardsByPhone(ctx context.Context, phone string) (bothCards []*domains.Card, errCustom *errs.Error) {
	var (
		l     = logger.FromCtx(ctx, "card_source.GetAllCardsByPhone")
		mutex sync.RWMutex
		wg    sync.WaitGroup
	)

	wg.Add(two)
	go func() {
		defer wg.Done()
		humoCards, err := uc.humo.GetCardByPhone(ctx, phone)
		if err != nil {
			l.Error("uc.humo.GetCardByPhone failed", zap.Error(err))

			errCustom = errs.NewError(errs.ErrorCodeInternalServer, err.Error())

			return
		}

		if len(humoCards) != 0 {
			for _, c := range humoCards {
				h, err := uc.humo.GetCardMaskedPAN(ctx, c.Number)
				if err != nil {
					l.Error("uc.humo.GetCardMaskedPAN failed", zap.Error(err))

					continue
				}

				h.Number = helpers.ConvertMask(c.Number)
				h.Expiry = c.Expiry

				mutex.Lock()
				bothCards = append(bothCards, h)
				mutex.Unlock()
			}
		}
	}()

	go func() {
		defer wg.Done()
		uzCards, err := uc.uzcard.GetCardByPhone(ctx, phone)
		if err != nil {
			l.Error("uc.uzcard.GetCardByPhone failed", zap.Error(err))

			errCustom = errs.NewError(errs.ErrorCodeInternalServer, err.Error())

			return
		}

		if len(uzCards) != 0 {
			for _, v := range uzCards {
				u, err := uc.uzcard.GetCardBalance(ctx, v.ID)
				if err != nil {
					l.Error("uc.uzcard.GetCardBalance failed" + err.Error())

					continue
				}

				if u.Status == domains.CardStatusNoActive {
					l.Error("u.Status == domains.CardStatusNoActive failed", zap.String("u.State", u.Status))

					continue
				}

				mutex.Lock()
				bothCards = append(bothCards, u)
				mutex.Unlock()
			}
		}
	}()

	wg.Wait()

	if len(bothCards) == 0 && errCustom != nil {
		l.Error("len(bothCards) == 0 && errCustom failed")
		errCustom = errs.CardNotFound

		return //nolint
	}

	return bothCards, nil
}

type cData struct {
	CardID    string
	BIN       string
	MaskedPAN string
	SMSCode   string
	Expiry    string
}

func (uc *UseCase) CreateCard(ctx context.Context, card domains.PayCard) (string, *errs.Error) {
	var (
		l  = logger.FromCtx(ctx, "card_source.CreateCard")
		c  *domains.Card
		cr = sha256.New()
	)

	bin, err := uc.bin.GetCardTypeByNumber(ctx, card.BIN)
	if err != nil {
		l.Error("uc.bin.GetCardTypeByNumber failed", zap.Error(err))

		return "", errs.CardBinNotExist
	}

	switch bin.PSCode {
	case domains.PSCodeUZCARD:
		c, err = uc.uzcard.GetCardID(ctx, card.BIN)
		if err != nil {
			l.Error("uc.uzcard.GetCardID failed", zap.Error(err))

			return "", errs.CardNotFound
		}

		c, err = uc.uzcard.GetCardBalance(ctx, c.ID)
		if err != nil {
			l.Error("uc.uzcard.GetCardBalance failed", zap.Error(err))

			return "", errs.CardNotFound
		}

	case domains.PSCodeHUMO:
		c, err = uc.humo.GetCardByPan(ctx, card.BIN, card.Expiry)
		if err != nil {
			l.Error("uc.humo.GetCardByPan failed", zap.Error(err))

			return "", errs.CardNotFound
		}

		cardP, err := uc.humo.GetCardMaskedPAN(ctx, card.BIN)
		if err != nil {
			l.Error("uc.humo.GetCardMaskedPAN failed", zap.Error(err))

			return "", errs.CardNotFound
		}

		c.ID = cardP.ID
	default:
		l.Error("Unknown ps code")

		return "", errs.InternalError
	}

	smsCode, err := uc.otp.SendSMS(ctx, c.Phone) //nolint
	if err != nil {
		l.Error("uc.otp.SendSMS failed", zap.Error(err))

		return "", errs.SendOTP
	}

	smsCode = "123321"
	cr.Write([]byte(smsCode))
	hSmsCode := cr.Sum(nil)
	cData := cData{
		CardID:    c.ID,
		SMSCode:   string(hSmsCode),
		Expiry:    card.Expiry,
		MaskedPAN: helpers.ConvertMask(card.BIN),
		BIN:       bin.Code,
	}
	keyUUID := helpers.GenerateUUIDWithPrefix("")
	err = uc.cache.SetWithDuration(ctx, keyUUID, &cData, five*time.Minute)
	if err != nil {
		l.Error("uc.cache.SetWithDuration failed", zap.Error(err), zap.Any("cacheReq", cData))

		return "", errs.InternalError
	}

	return keyUUID, nil
}

func (uc *UseCase) VerifyCard(ctx context.Context, key, smsCode, mrID string) (domains.PayCard, *errs.Error) {
	var (
		l     = logger.FromCtx(ctx, "card_source.VerifyCard")
		respC cData
		cr    = sha256.New()
	)

	err := uc.cache.GetData(ctx, key, &respC)
	if err != nil {
		l.Error("uc.cache.GetData failed", zap.Error(err), zap.String("key", key))

		return domains.PayCard{}, errs.InternalError
	}

	cr.Write([]byte(smsCode))
	hSmsCode := string(cr.Sum(nil))
	if !strings.EqualFold(hSmsCode, respC.SMSCode) {
		l.Error("sms code not found")

		return domains.PayCard{}, errs.WrongOTP
	}

	pCard := domains.PayCard{
		ID:        respC.CardID,
		BIN:       respC.BIN,
		MaskedPAN: respC.MaskedPAN,
		Expiry:    respC.Expiry,
	}

	resp, err := uc.dbCard.Create(ctx, pCard)
	if err != nil {
		l.Error("uc.dbCard.Create failed", zap.Error(err))

		return resp, errs.NewError(errs.ErrorCodeInternalServer, err.Error())
	}

	mCard := domains.MerchantCard{
		Merchant: domains.Merchant{ID: domains.ID(mrID)},
		Card:     domains.Card{ID: resp.ID},
	}
	_, err = uc.mr.CreateMerchantCard(ctx, mCard)
	if err != nil {
		return domains.PayCard{}, errs.NewError(errs.ErrorCodeInternalServer, err.Error())
	}

	return resp, nil
}

func (uc *UseCase) InfoCard(ctx context.Context, id, mrID string) (*domains.Card, *errs.Error) {
	var (
		l        = logger.FromCtx(ctx, "card_source.VerifyCard")
		respCard *domains.Card
	)

	if !uc.mr.GetMerCard(ctx, id, mrID) {
		l.Error("uc.mr.GetMerCard failed", zap.String("card_id", id), zap.String("mer_id", mrID))

		return nil, errs.ActionClosed
	}

	ps, ms, exp, err := uc.dbCard.GetCardPS(ctx, id)
	if err != nil {
		l.Error("uc.dbCard.GetCardPS failed", zap.Error(err))

		return respCard, errs.CardNotFound
	}

	switch ps {
	case domains.PSCodeUZCARD:
		respCard, err = uc.uzcard.GetCardBalance(ctx, id)
		if err != nil {
			l.Error("uc.uzcard.GetCardBalance failed", zap.Error(err))

			return respCard, errs.InternalError
		}
	case domains.PSCodeHUMO:
		respCard, err = uc.humo.GetCardNumber(ctx, id)
		if err != nil {
			l.Error("uc.humo.GetCardNumber failed", zap.Error(err))

			return respCard, errs.InternalError
		}

		respCard, err = uc.humo.GetCardBalance(ctx, respCard.Number)
		if err != nil {
			l.Error("uc.humo.GetCardBalance failed", zap.Error(err))

			return respCard, errs.InternalError
		}
	default:
		l.Error("there is not this ps")

		return nil, errs.InternalError
	}

	respCard.Number = ms
	respCard.Expiry = exp

	return respCard, nil
}
