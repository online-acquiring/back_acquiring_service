package commission_action //nolint

//nolint:goimports
import (
	"context"
	"go.uber.org/zap"
)

type DriverCommission interface {
	CreateCommission(ctx context.Context, com domains.Commission) (domains.Commission, error)
	GetAllCommissions(ctx context.Context) ([]domains.Commission, error)
	GetCommission(ctx context.Context, com domains.Commission) (domains.Commission, error)
	UpdateCommission(ctx context.Context, com domains.Commission) (domains.Commission, error)
}

type comRepoSource interface {
	Create(ctx context.Context, com domains.Commission) (domains.Commission, error)
	GetAll(ctx context.Context) ([]domains.Commission, error)
	Get(ctx context.Context, com domains.Commission) (domains.Commission, error)
	Update(ctx context.Context, com domains.Commission) (domains.Commission, error)
}

type UseCase struct {
	comSource comRepoSource
}

func New(c comRepoSource) *UseCase {
	return &UseCase{
		comSource: c,
	}
}

func (u *UseCase) CreateCommission(ctx context.Context, com domains.Commission) (domains.Commission, error) {
	var l = logger.FromCtx(ctx, "commission_action.CreateCommission")
	com, err := u.comSource.Create(ctx, com)
	if err != nil {
		l.Error("u.comSource.Create failed", zap.Error(err))

		return domains.Commission{}, err
	}

	return com, nil
}

func (u *UseCase) GetAllCommissions(ctx context.Context) ([]domains.Commission, error) {
	var l = logger.FromCtx(ctx, "commission_action.GetAllCommissions")
	coms, err := u.comSource.GetAll(ctx)
	if err != nil {
		l.Error("u.comSource.GetAll failed", zap.Error(err))
		return nil, err
	}

	return coms, nil
}

func (u *UseCase) GetCommission(ctx context.Context, com domains.Commission) (domains.Commission, error) {
	var l = logger.FromCtx(ctx, "commission_action.GetCommission")
	com, err := u.comSource.Get(ctx, com)
	if err != nil {
		l.Error("u.comSource.Get failed", zap.Error(err))

		return domains.Commission{}, err
	}

	return com, nil
}

func (u *UseCase) UpdateCommission(ctx context.Context, com domains.Commission) (domains.Commission, error) {
	var l = logger.FromCtx(ctx, "commission_action.UpdateCommission")
	com, err := u.comSource.Update(ctx, com)
	if err != nil {
		l.Error("u.comSource.Update failed", zap.Error(err))

		return domains.Commission{}, nil
	}

	return com, nil
}
