package user_action //nolint

import (
	"context"
	"errors"

	"github.com/google/uuid"
	"go.uber.org/zap"
)

const LoginMinSize = 5

type DriverUser interface {
	CreateUser(ctx context.Context, user domains.User) (domains.User, error)
	GetUserByLoginAndPassword(ctx context.Context, user domains.User) (domains.User, error)
	UpdateUserByUserID(ctx context.Context, user domains.User) error
	UpdateUserPasswordByLogin(ctx context.Context, user domains.User) error
	UpdateUserStates(ctx context.Context, user domains.User) error
	UpdateUserStatesByLogin(ctx context.Context, login string, state int) error
	UpdateUserPassByExistedPass(ctx context.Context, login, old, new string) error
	GetAllUsers(ctx context.Context) ([]domains.User, error)
}

type userSource interface {
	AddUser(ctx context.Context, user domains.User) (domains.User, error)
	GetUserByLoginAndPass(ctx context.Context, user domains.User) (domains.User, error)
	GetUserByLogin(ctx context.Context, user domains.User) (domains.User, error)
	UpdateUserByUserID(ctx context.Context, user domains.User) error
	UpdateUserPassByLogin(ctx context.Context, user domains.User) error
	UpdateUserState(ctx context.Context, user domains.User) error
	UpdateUserStateByLogin(ctx context.Context, login string, state int) error
	UpdateUserPassByExistedPassword(ctx context.Context, userID, newPass string) error
	GetUsers(ctx context.Context) ([]domains.User, error)
	GetUserByID(ctx context.Context, userID domains.ID) (domains.User, error)
}

type objectSource interface {
	AddActionProtocol(ctx context.Context, p domains.ObjectActionProtocol) (domains.ObjectActionProtocol, error)
	CheckState(ctx context.Context, obj domains.ObjectTranState) (domains.State, error)
}

type UseCase struct {
	user   userSource
	object objectSource
}

func New(u userSource, object objectSource) *UseCase {
	return &UseCase{
		user:   u,
		object: object,
	}
}

func (uc *UseCase) CreateUser(ctx context.Context, user domains.User) (domains.User, error) {
	var (
		l   = logger.FromCtx(ctx, "user_action.CreateUser")
		err error
	)

	var objTr = domains.ObjectTranState{
		Object: domains.Object{
			Code: domains.ObjectCodeUser,
		},
		State:  domains.UserStateCreated,
		Action: domains.UserActionCreate,
	}
	if objTr.NewState, err = uc.object.CheckState(ctx, objTr); err != nil {
		l.Error("uc.object.CheckState failed", zap.Any("new_state", objTr.NewState))

		return domains.User{}, errs.WrongAction
	}

	if user.Role == domains.SuperAdminRole {
		l.Error("check user.Role failed")

		return domains.User{}, errors.New("нельзя создать ползователь с ролем superAdmin")
	}

	if !userPasswordCheck(user.Password) {
		l.Error("userPasswordCheck failed")

		return domains.User{}, errors.New("парол должен быть не менее 6 символов и не должен быть легкие")
	}

	user.State = int8(objTr.NewState)
	user.Password, _ = tools.HashSecret(user.Password)
	addUser, err := uc.user.AddUser(ctx, user)
	if err != nil {
		l.Error("uc.user.AddUser failed", zap.Error(err))

		return domains.User{}, err
	}

	objActionID := uuid.New().String()
	var objAction = domains.ObjectActionProtocol{
		ObjectID: domains.ID(objActionID),
		UserID:   addUser.ID,
		Message:  "",
		ObjectTranState: domains.ObjectTranState{
			Object: domains.Object{
				Code: domains.ObjectCodeUser,
			},
			State:    domains.State(addUser.State),
			Action:   domains.UserActionCreate,
			NewState: objTr.NewState,
		},
	}
	_, err = uc.object.AddActionProtocol(ctx, objAction)
	if err != nil {
		l.Error("uc.object.AddActionProtocol failed", zap.Error(err))

		return domains.User{}, err
	}

	return addUser, nil
}

func (uc *UseCase) GetUserByLoginAndPassword(ctx context.Context, user domains.User) (domains.User, error) {
	var l = logger.FromCtx(ctx, "user_action.GetUserByLoginAndPassword")
	user1, err := uc.user.GetUserByLogin(ctx, user)
	if err != nil {
		l.Error("uc.user.GetUserByLogin failed")

		return domains.User{}, err
	}

	if tools.CheckSecretHash(user.Password, user1.Password) {
		l.Error("tools.CheckSecretHash success")

		return user1, err
	}

	l.Error("tools.CheckSecretHash wrong password")
	return domains.User{}, errors.New("парол неверный")
}

// func (u *UseCase) GetUserByLogin(ctx context.Context, user domains.User) (domains.User, error) {
//	user1, err := u.user.GetUserByLogin(ctx, user)
//	if err != nil {
//		return domains.User{}, err
//	}
//	if tools.CheckSecretHash(user.Password, user1.Password) {
//		return user1, err
//	} else {
//		return domains.User{}, errors.New("парол неверный")
//	}
// }

func (uc *UseCase) UpdateUserByUserID(ctx context.Context, user domains.User) error {
	var l = logger.FromCtx(ctx, "user_action.UpdateUserByUserID")
	usr, err := uc.user.GetUserByID(ctx, user.ID)
	if err != nil {
		return err
	}

	curAct := domains.UserActionActivate
	if domains.State(user.State) == domains.UserStatePassive {
		curAct = domains.UserActionDeactivate
	}

	var objTr = domains.ObjectTranState{
		Object: domains.Object{
			Code: domains.ObjectCodeUser,
		},
		State:  domains.State(usr.State),
		Action: curAct,
	}
	if objTr.NewState, err = uc.object.CheckState(ctx, objTr); err != nil {
		l.Error("uc.object.CheckState failed", zap.Any("new_state", objTr.NewState))

		return errs.WrongAction
	}

	if !userPasswordCheck(user.Password) {
		l.Error("userPasswordCheck failed")

		return errors.New("парол должен быть не менее 6 символов и не должен быть легкие")
	}

	user.Password, _ = tools.HashSecret(user.Password)
	if err := uc.user.UpdateUserByUserID(ctx, user); err != nil {
		l.Error("uc.object.UpdateUserByUserID failed", zap.Error(err))

		return err
	}

	objActionID := uuid.New().String()
	var objAction = domains.ObjectActionProtocol{
		ObjectID: domains.ID(objActionID),
		UserID:   user.ID,
		Message:  "",
		ObjectTranState: domains.ObjectTranState{
			Object: domains.Object{
				Code: domains.ObjectCodeUser,
			},
			State:    domains.State(usr.State),
			Action:   curAct,
			NewState: objTr.NewState,
		},
	}
	_, err = uc.object.AddActionProtocol(ctx, objAction)
	if err != nil {
		l.Error("uc.object.AddActionProtocol failed", zap.Error(err))

		return err
	}

	return err
}

func (uc *UseCase) UpdateUserPasswordByLogin(ctx context.Context, user domains.User) error {
	var (
		l   = logger.FromCtx(ctx, "user_action.UpdateUserPasswordByLogin")
		err error
	)
	userReq := domains.User{
		Login: user.Login,
	}
	usr, err := uc.user.GetUserByLogin(ctx, userReq)
	if err != nil {
		l.Error("uc.user.GetUserByLogin failed", zap.Error(err))

		return err
	}

	var objTr = domains.ObjectTranState{
		Object: domains.Object{
			Code: domains.ObjectCodeUser,
		},
		State:  domains.State(usr.State),
		Action: domains.UserActionEdit,
	}
	if objTr.NewState, err = uc.object.CheckState(ctx, objTr); err != nil {
		l.Error("uc.object.CheckState failed", zap.Any("new_state", objTr.NewState))

		return errs.WrongAction
	}

	if !userPasswordCheck(user.Credential.Password) {
		l.Error("userPasswordCheck failed")

		return errors.New("парол должен быть не менее 6 символов и не должен быть легкие")
	}

	user.Password, _ = tools.HashSecret(user.Password)
	if err = uc.user.UpdateUserPassByLogin(ctx, user); err != nil {
		l.Error("uc.user.UpdateUserPassByLogin failed", zap.Error(err))

		return err
	}

	objActionID := uuid.New().String()
	var objAction = domains.ObjectActionProtocol{
		ObjectID: domains.ID(objActionID),
		UserID:   usr.ID,
		Message:  "",
		ObjectTranState: domains.ObjectTranState{
			Object: domains.Object{
				Code: domains.ObjectCodeUser,
			},
			State:    domains.State(usr.State),
			Action:   domains.UserActionEdit,
			NewState: objTr.NewState,
		},
	}
	_, err = uc.object.AddActionProtocol(ctx, objAction)
	if err != nil {
		l.Error("uc.object.AddActionProtocol failed", zap.Error(err))

		return err
	}

	return err
}

func (uc *UseCase) UpdateUserStates(ctx context.Context, user domains.User) error {
	var l = logger.FromCtx(ctx, "user_action.UpdateUserStates")
	usr, err := uc.user.GetUserByID(ctx, user.ID)
	if err != nil {
		return err
	}

	curAct := domains.UserActionActivate
	if domains.State(user.State) == domains.UserStatePassive {
		curAct = domains.UserActionDeactivate
	}

	var objTr = domains.ObjectTranState{
		Object: domains.Object{
			Code: domains.ObjectCodeUser,
		},
		State:  domains.State(usr.State),
		Action: curAct,
	}
	if objTr.NewState, err = uc.object.CheckState(ctx, objTr); err != nil {
		l.Error("uc.object.CheckState failed", zap.Any("new_state", objTr.NewState))

		return errs.WrongAction
	}

	if err := uc.user.UpdateUserState(ctx, user); err != nil {
		return err
	}

	objActionID := uuid.New().String()
	var objAction = domains.ObjectActionProtocol{
		ObjectID: domains.ID(objActionID),
		UserID:   user.ID,
		Message:  "",
		ObjectTranState: domains.ObjectTranState{
			Object: domains.Object{
				Code: domains.ObjectCodeUser,
			},
			State:    domains.State(usr.State),
			Action:   curAct,
			NewState: objTr.NewState,
		},
	}
	_, err = uc.object.AddActionProtocol(ctx, objAction)
	if err != nil {
		l.Error("uc.object.AddActionProtocol failed", zap.Error(err))

		return err
	}

	return err
}

func (uc *UseCase) UpdateUserStatesByLogin(ctx context.Context, login string, state int) error {
	return uc.user.UpdateUserStateByLogin(ctx, login, state)
}

func (uc *UseCase) GetAllUsers(ctx context.Context) ([]domains.User, error) {
	return uc.user.GetUsers(ctx)
}

func (uc *UseCase) UpdateUserPassByExistedPass(ctx context.Context, login, oldPass, newPass string) error {
	var (
		l   = logger.FromCtx(ctx, "user_action.UpdateUserPassByExistedPass")
		err error
	)

	userReq := domains.User{
		Login: login,
	}
	usr, err := uc.user.GetUserByLogin(ctx, userReq)
	if err != nil {
		l.Error("uc.user.GetUserByLogin failed", zap.Error(err))

		return err
	}

	var objTr = domains.ObjectTranState{
		Object: domains.Object{
			Code: domains.ObjectCodeUser,
		},
		State:  domains.State(usr.State),
		Action: domains.UserActionEdit,
	}
	if objTr.NewState, err = uc.object.CheckState(ctx, objTr); err != nil {
		l.Error("uc.object.CheckState failed", zap.Any("new_state", objTr.NewState))

		return errs.WrongAction
	}

	if !userPasswordCheck(newPass) {
		l.Error("userPasswordCheck failed")

		return errors.New("парол должен быть не менее 6 символов и не должен быть легкие")
	}

	userDb, err := uc.GetUserByLoginAndPassword(ctx, domains.User{Login: login, Credential: domains.Credential{Password: oldPass}})
	if err != nil {
		l.Error("uc.GetUserByLoginAndPassword failed", zap.Error(err))

		return err
	}

	newPass, _ = tools.HashSecret(newPass)
	if err = uc.user.UpdateUserPassByExistedPassword(ctx, string(userDb.ID), newPass); err != nil {
		l.Error("uc.user.UpdateUserPassByExistedPassword failed", zap.Error(err))

		return err
	}

	objActionID := uuid.New().String()
	var objAction = domains.ObjectActionProtocol{
		ObjectID: domains.ID(objActionID),
		UserID:   usr.ID,
		Message:  "",
		ObjectTranState: domains.ObjectTranState{
			Object: domains.Object{
				Code: domains.ObjectCodeUser,
			},
			State:    domains.State(usr.State),
			Action:   domains.UserActionEdit,
			NewState: objTr.NewState,
		},
	}
	_, err = uc.object.AddActionProtocol(ctx, objAction)
	if err != nil {
		l.Error("uc.object.AddActionProtocol failed", zap.Error(err))

		return err
	}

	return err
}

func userPasswordCheck(pass string) bool {
	// keyinchalik qo'shiladi s := []string{"123456","qaz123"}
	// keyinchalik qo'shiladi fmt.Println(sort.SearchStrings(s,pass))
	return len(pass) > LoginMinSize
}
