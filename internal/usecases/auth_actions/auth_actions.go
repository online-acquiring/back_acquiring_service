package auth_actions //nolint

import (
	"context"

	"go.uber.org/zap"
)

type merchSettingsSource interface {
	Get(ctx context.Context, mrKey string) (domains.Merchant, error)
}

type UseCase struct {
	mrSet merchSettingsSource
}

func New(mrSet merchSettingsSource) *UseCase {
	return &UseCase{
		mrSet: mrSet,
	}
}

func (uc *UseCase) GetMerchant(ctx context.Context, key string) (domains.Merchant, error) {
	var l = logger.FromCtx(ctx, "auth_actions.GetMerchant")
	mer, err := uc.mrSet.Get(ctx, key)
	if err != nil {
		l.Error("uc.mrSet.Get failed", zap.Error(err))

		return domains.Merchant{}, err
	}

	return mer, nil
}
