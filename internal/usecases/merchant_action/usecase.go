package merchant_action //nolint

import (
	"context"

	"github.com/google/uuid"
	"go.uber.org/zap"
)

//nolint:dupl
type DriverMerchant interface {
	CreateMerchant(ctx context.Context, mr domains.Merchant) (domains.Merchant, error)
	GetAllMerchants(ctx context.Context) ([]domains.Merchant, error)
	GetMerchant(ctx context.Context, mr domains.Merchant) ([]domains.Merchant, error)
	UpdateMerchant(ctx context.Context, mr domains.Merchant) (domains.Merchant, error)

	CreateMerchTer(ctx context.Context, mr domains.MerchantTerminal) (domains.MerchantTerminal, error)
	GetMerchTer(ctx context.Context, mt domains.MerchantTerminal) (domains.MerchantTerminal, error)
	UpdateMerchTer(ctx context.Context, t domains.MerchantTerminal) (domains.MerchantTerminal, error)

	CreateMerchAcc(ctx context.Context, acc domains.MerchantAccount) (domains.MerchantAccount, error)
	GetMerchAcc(ctx context.Context, acc domains.MerchantAccount) (domains.MerchantAccount, error)
	UpdateMerchAcc(ctx context.Context, acc domains.MerchantAccount) (domains.MerchantAccount, error)
}

//nolint:dupl
type merchantSource interface {
	Create(ctx context.Context, mr domains.Merchant) (domains.Merchant, error)
	GetAll(ctx context.Context) ([]domains.Merchant, error)
	Get(ctx context.Context, mr domains.Merchant) ([]domains.Merchant, error)
	UpdateMerchant(ctx context.Context, mr domains.Merchant) (domains.Merchant, error)

	CreateMerchantTerminal(ctx context.Context, t domains.MerchantTerminal) (domains.MerchantTerminal, error)
	GetMerchantTerminal(ctx context.Context, t domains.MerchantTerminal) (domains.MerchantTerminal, error)
	UpdateMerchantTerminal(ctx context.Context, t domains.MerchantTerminal) (domains.MerchantTerminal, error)

	CreateMerchantAccount(ctx context.Context, acc domains.MerchantAccount) (domains.MerchantAccount, error)
	GetMerchantAccount(ctx context.Context, mr domains.MerchantAccount) (domains.MerchantAccount, error)
	UpdateMerchantAccount(ctx context.Context, acc domains.MerchantAccount) (domains.MerchantAccount, error)
}

type objectSource interface {
	AddActionProtocol(ctx context.Context, p domains.ObjectActionProtocol) (domains.ObjectActionProtocol, error)
	CheckState(ctx context.Context, obj domains.ObjectTranState) (domains.State, error)
}

type UseCase struct {
	merchantSource merchantSource
	object         objectSource
}

func New(m merchantSource, object objectSource) *UseCase {
	return &UseCase{
		merchantSource: m,
		object:         object,
	}
}

func (uc *UseCase) CreateMerchant(ctx context.Context, mr domains.Merchant) (domains.Merchant, error) {
	var (
		l   = logger.FromCtx(ctx, "merchant_action.CreateMerchant")
		err error
	)
	var objTr = domains.ObjectTranState{
		Object: domains.Object{
			Code: domains.ObjectCodeMerchant,
		},
		State:  domains.MerchantStateCreated,
		Action: domains.MerchantActionCreate,
	}
	if objTr.NewState, err = uc.object.CheckState(ctx, objTr); err != nil {
		l.Error("uc.object.CheckState failed", zap.Any("new_state", objTr.NewState))

		return domains.Merchant{}, errs.WrongAction
	}

	mer, err := uc.merchantSource.Create(ctx, mr)
	if err != nil {
		l.Error("uc.merchantSource.Create failed", zap.Error(err))

		return domains.Merchant{}, err
	}

	objActionID := uuid.New().String()
	var objAction = domains.ObjectActionProtocol{
		ObjectID: domains.ID(objActionID),
		UserID:   domains.ID(ctx.Value("user_id").(string)),
		Message:  "",
		ObjectTranState: domains.ObjectTranState{
			Object: domains.Object{
				Code: domains.ObjectCodeMerchant,
			},
			State:    mer.State,
			Action:   domains.MerchantActionCreate,
			NewState: objTr.NewState,
		},
	}
	_, err = uc.object.AddActionProtocol(ctx, objAction)
	if err != nil {
		l.Error("uc.object.AddActionProtocol failed", zap.Error(err))

		return domains.Merchant{}, err
	}

	return mer, nil
}

func (uc *UseCase) GetAllMerchants(ctx context.Context) ([]domains.Merchant, error) {
	var l = logger.FromCtx(ctx, "merchant_action.GetAllMerchants")
	mers, err := uc.merchantSource.GetAll(ctx)
	if err != nil {
		l.Error("u.merchantSource.GetAll failed", zap.Error(err))

		return nil, err
	}

	return mers, nil
}

func (uc *UseCase) GetMerchant(ctx context.Context, mr domains.Merchant) ([]domains.Merchant, error) {
	var l = logger.FromCtx(ctx, "merchant_action.GetMerchant")
	mer, err := uc.merchantSource.Get(ctx, mr)
	if err != nil {
		l.Error("u.merchantSource.Get failed", zap.Error(err))

		return nil, err
	}

	return mer, nil
}

func (uc *UseCase) CreateMerchTer(ctx context.Context, mr domains.MerchantTerminal) (domains.MerchantTerminal, error) {
	var l = logger.FromCtx(ctx, "merchant_action.CreateMerchTer")
	mer, err := uc.merchantSource.CreateMerchantTerminal(ctx, mr)
	if err != nil {
		l.Error("u.merchantSource.CreateMerchantTerminal failed", zap.Error(err))

		return domains.MerchantTerminal{}, err
	}

	return mer, nil
}

func (uc *UseCase) GetMerchTer(ctx context.Context, mt domains.MerchantTerminal) (domains.MerchantTerminal, error) {
	var l = logger.FromCtx(ctx, "merchant_action.GetMerchTer")
	mt, err := uc.merchantSource.GetMerchantTerminal(ctx, mt)
	if err != nil {
		l.Error("u.merchantSource.GetMerchantTerminal failed", zap.Error(err))

		return domains.MerchantTerminal{}, err
	}

	return mt, nil
}

func (uc *UseCase) UpdateMerchTer(ctx context.Context, t domains.MerchantTerminal) (domains.MerchantTerminal, error) {
	var l = logger.FromCtx(ctx, "merchant_action.UpdateMerchTer")
	mt, err := uc.merchantSource.UpdateMerchantTerminal(ctx, t)
	if err != nil {
		l.Error("u.merchantSource.UpdateMerchantTerminal failed", zap.Error(err))

		return domains.MerchantTerminal{}, err
	}

	return mt, nil
}

func (uc *UseCase) CreateMerchAcc(ctx context.Context, mr domains.MerchantAccount) (domains.MerchantAccount, error) {
	var l = logger.FromCtx(ctx, "merchant_action.CreateMerchAcc")
	mer, err := uc.merchantSource.CreateMerchantAccount(ctx, mr)
	if err != nil {
		l.Error("u.merchantSource.CreateMerchantAccount failed", zap.Error(err))

		return domains.MerchantAccount{}, err
	}

	return mer, nil
}

func (uc *UseCase) GetMerchAcc(ctx context.Context, acc domains.MerchantAccount) (domains.MerchantAccount, error) {
	var l = logger.FromCtx(ctx, "merchant_action.GetMerchAcc")
	mer, err := uc.merchantSource.GetMerchantAccount(ctx, acc)
	if err != nil {
		l.Error("u.merchantSource.GetMerchantAccount failed", zap.Error(err))

		return domains.MerchantAccount{}, err
	}
	return mer, nil
}

func (uc *UseCase) UpdateMerchant(ctx context.Context, mr domains.Merchant) (domains.Merchant, error) {
	var l = logger.FromCtx(ctx, "merchant_action.UpdateMerchant")

	mCur, err := uc.merchantSource.Get(ctx, mr)
	if err != nil {
		return domains.Merchant{}, err
	}

	var objTr = domains.ObjectTranState{
		Object: domains.Object{
			Code: domains.ObjectCodeMerchant,
		},
		State:  mCur[0].State,
		Action: domains.MerchantActionEdit,
	}
	if objTr.NewState, err = uc.object.CheckState(ctx, objTr); err != nil {
		l.Error("uc.object.CheckState failed", zap.Any("new_state", objTr.NewState))

		return domains.Merchant{}, errs.WrongAction
	}

	mCur[0].ID = mr.ID
	mCur[0].ContractNumber = mr.ContractNumber
	mCur[0].Inn = mr.Inn
	mCur[0].OrgName = mr.OrgName
	mCur[0].ShortName = mr.ShortName
	mCur[0].DocCreatedAt = mr.DocCreatedAt
	mer, err := uc.merchantSource.UpdateMerchant(ctx, mCur[0])
	if err != nil {
		l.Error("u.merchantSource.UpdateMerchant failed", zap.Error(err))

		return domains.Merchant{}, err
	}

	objActionID := uuid.New().String()
	var objAction = domains.ObjectActionProtocol{
		ObjectID: domains.ID(objActionID),
		UserID:   domains.ID(ctx.Value("user_id").(string)),
		Message:  "",
		ObjectTranState: domains.ObjectTranState{
			Object: domains.Object{
				Code: domains.ObjectCodeUser,
			},
			State:    mCur[0].State,
			Action:   domains.MerchantActionEdit,
			NewState: objTr.NewState,
		},
	}
	_, err = uc.object.AddActionProtocol(ctx, objAction)
	if err != nil {
		l.Error("uc.object.AddActionProtocol failed", zap.Error(err))

		return domains.Merchant{}, err
	}

	return mer, nil
}

func (uc *UseCase) UpdateMerchAcc(ctx context.Context, acc domains.MerchantAccount) (domains.MerchantAccount, error) {
	var l = logger.FromCtx(ctx, "merchant_action.UpdateMerchAcc")
	mer, err := uc.merchantSource.UpdateMerchantAccount(ctx, acc)
	if err != nil {
		l.Error("u.merchantSource.UpdateMerchantAccount failed", zap.Error(err))

		return domains.MerchantAccount{}, err
	}

	return mer, nil
}
