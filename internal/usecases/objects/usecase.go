package objects

//nolint:goimports
import (
	"context"
	"go.uber.org/zap"
)

type DriverObject interface {
	CheckState(ctx context.Context, ob domains.ObjectTranState) (domains.State, error)
	AddActionProtocol(ctx context.Context, p domains.ObjectActionProtocol) (domains.ObjectActionProtocol, error)
	GetActionProtocols(ctx context.Context) ([]domains.ObjectActionProtocol, error)
	GetActionProtocolByPayID(ctx context.Context, payment domains.Payment) ([]domains.ObjectActionProtocol, error)
}
type objectSource interface {
	Get(ctx context.Context, ob domains.ObjectTranState) (domains.State, error)
	AddActionProtocol(ctx context.Context, p domains.ObjectActionProtocol) (domains.ObjectActionProtocol, error)
	GetActionProtocols(ctx context.Context) ([]domains.ObjectActionProtocol, error)
	GetActionProtocolByPayID(ctx context.Context, payment domains.Payment) ([]domains.ObjectActionProtocol, error)
}

type UseCase struct {
	obRepo objectSource
}

func New(objSrc objectSource) *UseCase {
	return &UseCase{
		obRepo: objSrc,
	}
}

func (uc *UseCase) CheckState(ctx context.Context, obj domains.ObjectTranState) (domains.State, error) {
	var l = logger.FromCtx(ctx, "objects.CheckState")
	newState, err := uc.obRepo.Get(ctx, obj)
	if err != nil {
		l.Error("uc.obRepo.Get failed", zap.Error(err))

		return newState, err
	}

	return newState, nil
}

func (uc *UseCase) AddActionProtocol(ctx context.Context, pObj domains.ObjectActionProtocol) (domains.ObjectActionProtocol, error) {
	var l = logger.FromCtx(ctx, "objects.AddActionProtocol")
	objAct, err := uc.obRepo.AddActionProtocol(ctx, pObj)
	if err != nil {
		l.Error("uc.obRepo.AddActionProtocol failed", zap.Error(err))

		return domains.ObjectActionProtocol{}, err
	}

	return objAct, nil
}

func (uc *UseCase) GetActionProtocols(ctx context.Context) ([]domains.ObjectActionProtocol, error) {
	var l = logger.FromCtx(ctx, "objects.GetActionProtocols")
	objAct, err := uc.obRepo.GetActionProtocols(ctx)
	if err != nil {
		l.Error("uc.obRepo.GetActionProtocols failed", zap.Error(err))

		return objAct, err
	}

	return objAct, nil
}

func (uc *UseCase) GetActionProtocolByPayID(ctx context.Context, payment domains.Payment) ([]domains.ObjectActionProtocol, error) {
	var l = logger.FromCtx(ctx, "objects.GetActionProtocols")
	objAct, err := uc.obRepo.GetActionProtocolByPayID(ctx, payment)
	if err != nil {
		l.Error("uc.obRepo.GetActionProtocols failed", zap.Error(err))

		return objAct, err
	}

	return objAct, nil
}
