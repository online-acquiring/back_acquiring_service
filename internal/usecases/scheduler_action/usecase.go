package scheduler_action //nolint

import (
	"context"

	"go.uber.org/zap"
)

type DriverScheduler interface {
	CreateScheduler(ctx context.Context, sch domains.Scheduler) (domains.Scheduler, error)
	GetScheduler(ctx context.Context, id string) (domains.Scheduler, error)
	GetAllSchedulers(ctx context.Context) ([]domains.Scheduler, error)
	UpdateScheduler(ctx context.Context, sch domains.Scheduler) error
	UpdateSchedulerState(ctx context.Context, scheduler domains.Scheduler) error
}

type sourceScheduler interface {
	Create(ctx context.Context, sch domains.Scheduler) (domains.Scheduler, error)
	Get(ctx context.Context, ID string) (domains.Scheduler, error)
	GetAll(ctx context.Context) ([]domains.Scheduler, error)
	UpdateScheduler(ctx context.Context, id, expression string) error
	UpdateSchedulerState(ctx context.Context, scheduler domains.Scheduler) error
}

type UseCase struct {
	schSource sourceScheduler
}

func New(sch sourceScheduler) *UseCase {
	return &UseCase{
		schSource: sch,
	}
}

func (uc *UseCase) CreateScheduler(ctx context.Context, sch domains.Scheduler) (domains.Scheduler, error) {
	var l = logger.FromCtx(ctx, "scheduler_action.CreateScheduler")
	sch, err := uc.schSource.Create(ctx, sch)
	if err != nil {
		l.Error("uc.schSource.Create failed", zap.Error(err))

		return domains.Scheduler{}, err
	}

	return sch, nil
}

func (uc *UseCase) GetScheduler(ctx context.Context, id string) (domains.Scheduler, error) {
	var l = logger.FromCtx(ctx, "scheduler_action.CreateScheduler")
	sch, err := uc.schSource.Get(ctx, id)
	if err != nil {
		l.Error("uc.schSource.Get failed", zap.Error(err))

		return domains.Scheduler{}, err
	}

	return sch, nil
}

func (uc *UseCase) GetAllSchedulers(ctx context.Context) ([]domains.Scheduler, error) {
	var l = logger.FromCtx(ctx, "scheduler_action.GetAllSchedulers")
	sch, err := uc.schSource.GetAll(ctx)
	if err != nil {
		l.Error("uc.schSource.GetAll failed", zap.Error(err))

		return nil, err
	}

	return sch, nil
}

func (uc *UseCase) UpdateScheduler(ctx context.Context, sch domains.Scheduler) error {
	var l = logger.FromCtx(ctx, "scheduler_action.UpdateScheduler")
	if err := uc.schSource.UpdateScheduler(ctx, string(sch.ID), sch.Expression); err != nil {
		l.Error("uc.schSource.UpdateScheduler failed", zap.Error(err))

		return err
	}

	return nil
}

func (uc *UseCase) UpdateSchedulerState(ctx context.Context, scheduler domains.Scheduler) error {
	var l = logger.FromCtx(ctx, "scheduler_action.UpdateSchedulerState")
	if err := uc.schSource.UpdateSchedulerState(ctx, scheduler); err != nil {
		l.Error("uc.schSource.UpdateSchedulerState failed", zap.Error(err))

		return err
	}

	return nil
}
