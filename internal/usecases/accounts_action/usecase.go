package accounts_action //nolint

import (
	"context"

	"go.uber.org/zap"
)

type DriverAccount interface {
	CreateAccount(ctx context.Context, acc domains.Account) (domains.Account, error)
	GetAccounts(ctx context.Context) ([]domains.Account, error)
	GetAccount(ctx context.Context, acc domains.Account) (domains.Account, error)
	UpdateAccount(ctx context.Context, acc domains.Account) (domains.Account, error)
}

type accountSource interface {
	Create(ctx context.Context, acc domains.Account) (domains.Account, error)
	GetAll(ctx context.Context) ([]domains.Account, error)
	Get(ctx context.Context, acc domains.Account) (domains.Account, error)
	UpdateAccount(ctx context.Context, acc domains.Account) (domains.Account, error)
}

type UseCase struct {
	aSource accountSource
}

func New(aSource accountSource) *UseCase {
	return &UseCase{
		aSource: aSource,
	}
}

func (uc *UseCase) CreateAccount(ctx context.Context, acc domains.Account) (domains.Account, error) {
	var l = logger.FromCtx(ctx, "accounts_action.CreateAccount")
	acc, err := uc.aSource.Create(ctx, acc)
	if err != nil {
		l.Error("u.aSource.Create failed", zap.Error(err))

		return domains.Account{}, err
	}

	return acc, nil
}

func (uc *UseCase) GetAccounts(ctx context.Context) ([]domains.Account, error) {
	var l = logger.FromCtx(ctx, "accounts_action.GetAccounts")
	accounts, err := uc.aSource.GetAll(ctx)
	if err != nil {
		l.Error("u.aSource.GetAll failed", zap.Error(err))

		return nil, nil
	}
	return accounts, nil
}

func (uc *UseCase) GetAccount(ctx context.Context, acc domains.Account) (domains.Account, error) {
	var l = logger.FromCtx(ctx, "accounts_action.GetAccount")
	acc, err := uc.aSource.Get(ctx, acc)
	if err != nil {
		l.Error("u.aSource.Get failed", zap.Error(err))

		return domains.Account{}, err
	}

	return acc, nil
}

func (uc *UseCase) UpdateAccount(ctx context.Context, acc domains.Account) (domains.Account, error) {
	var l = logger.FromCtx(ctx, "accounts_action.UpdateAccount")
	acc, err := uc.aSource.UpdateAccount(ctx, acc)
	if err != nil {
		l.Error("u.aSource.UpdateAccount failed", zap.Error(err))

		return domains.Account{}, err
	}

	return acc, nil
}
