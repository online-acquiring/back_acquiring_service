package service_sms //nolint

import (
	"context"
	"errors"
	"fmt"

	"go.uber.org/zap"
)

const (
	phoneSize    = 12
	oTPSize      = 5
	docTypeOther = 1
	docType      = 1
)

type driverSms interface {
	SendSms(ctx context.Context, smsData domains.SMS) (string, error)
}

type UseCase struct {
	sms driverSms
}

func New(driver driverSms) *UseCase {
	return &UseCase{
		sms: driver,
	}
}

func (uc *UseCase) SendSMS(ctx context.Context, phone string) (string, error) {
	var l = logger.FromCtx(ctx, "service_sms.SendSMS")
	genData, err := uc.generateSmsData(phone, docType)
	if err != nil {
		l.Error("uc.generateSmsData failed", zap.Error(err), zap.String("phone", phone))

		return "", err
	}

	smsCode, err := uc.sms.SendSms(ctx, genData)
	if err != nil {
		l.Error("uc.sms.SendSMS failed", zap.Error(err), zap.String("phone", phone))

		return "", err
	}

	return smsCode, nil
}

func (uc *UseCase) generateSmsData(phone string, docType int) (domains.SMS, error) {
	var l = logger.FromCtx(context.Background(), "service_sms.generateSmsData")
	phoneNumber := validation.OnlyDigits(phone)
	if len(phoneNumber) != phoneSize {
		l.Error("len(phoneNumber) != phoneSize failed")

		return domains.SMS{}, errors.New("phone format is wrong")
	}

	sms := domains.SMS{
		Phone: phoneNumber,
	}

	switch docType {
	case docTypeOther:
		code := tools.RandomDigitsString(oTPSize)
		message := combineTextWithCode(code)
		sms.Message = message
		sms.SmsCode = code
	default:
		fmt.Println("It is not still finished")
	}

	return sms, nil
}

func combineTextWithCode(code string) string {
	return "Acquiring tizim uchun kod: " + code // vaqtinchalik
}
