package param_action //nolint

import (
	"context"

	"go.uber.org/zap"
)

type DriverParam interface {
	CreateParam(ctx context.Context, param domains.Param) (domains.Param, error)
	GetAllParams(ctx context.Context) ([]domains.Param, error)
	GetParamByCode(ctx context.Context, code string) (domains.Param, error)
	GetSystemParamByID(ctx context.Context, id string) (domains.Param, error)
	UpdateParamByID(ctx context.Context, param domains.Param) error
	UpdateSystemParamByCode(ctx context.Context, param domains.Param) error
}

type sourceParam interface {
	Create(ctx context.Context, param domains.Param) (domains.Param, error)
	GetAll(ctx context.Context) ([]domains.Param, error)
	GetParamByCode(ctx context.Context, code string) (domains.Param, error)
	GetSystemParamByID(ctx context.Context, id string) (domains.Param, error)
	UpdateParamByID(ctx context.Context, param domains.Param) error
	UpdateSystemParamByCode(ctx context.Context, code, value string) error
}

type UseCase struct {
	param sourceParam
}

func New(param sourceParam) *UseCase {
	return &UseCase{
		param: param,
	}
}
func (uc *UseCase) CreateParam(ctx context.Context, param domains.Param) (domains.Param, error) {
	var l = logger.FromCtx(ctx, "param_action.CreateParam")
	p, err := uc.param.Create(ctx, param)
	if err != nil {
		l.Error("uc.param.Create failed", zap.Error(err))

		return domains.Param{}, err
	}

	return p, nil
}

func (uc *UseCase) GetAllParams(ctx context.Context) ([]domains.Param, error) {
	var l = logger.FromCtx(ctx, "param_action.GetAllParams")
	params, err := uc.param.GetAll(ctx)
	if err != nil {
		l.Error("uc.param.GetAll failed", zap.Error(err))

		return nil, err
	}

	return params, nil
}

func (uc *UseCase) GetParamByCode(ctx context.Context, code string) (domains.Param, error) {
	var l = logger.FromCtx(ctx, "param_action.GetParamByCode")
	p, err := uc.param.GetParamByCode(ctx, code)
	if err != nil {
		l.Error("uc.param.GetParamByCode failed", zap.Error(err))

		return domains.Param{}, err
	}

	return p, nil
}

func (uc *UseCase) GetSystemParamByID(ctx context.Context, id string) (domains.Param, error) {
	var l = logger.FromCtx(ctx, "param_action.GetSystemParamByID")
	p, err := uc.param.GetSystemParamByID(ctx, id)
	if err != nil {
		l.Error("uc.param.GetSystemParamByID failed", zap.Error(err))

		return domains.Param{}, err
	}

	return p, nil
}

func (uc *UseCase) UpdateParamByID(ctx context.Context, param domains.Param) error {
	var l = logger.FromCtx(ctx, "param_action.UpdateParamByID")
	if err := uc.param.UpdateParamByID(ctx, param); err != nil {
		l.Error("uc.param.UpdateParamByID failed", zap.Error(err))

		return err
	}

	return nil
}

func (uc *UseCase) UpdateSystemParamByCode(ctx context.Context, param domains.Param) error {
	var l = logger.FromCtx(ctx, "param_action.UpdateSystemParamByCode")
	if err := uc.param.UpdateSystemParamByCode(ctx, param.Code, param.Value); err != nil {
		l.Error("uc.param.UpdateSystemParamByCode failed", zap.Error(err))

		return err
	}

	return nil
}
