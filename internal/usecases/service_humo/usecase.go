package service_humo //nolint

import (
	"context"

	"go.uber.org/zap"
)

type DriverHumo interface {
	GetCardNumber(ctx context.Context, cardID string) (*domains.Card, error)
	GetCardMaskedPAN(ctx context.Context, cardNumber string) (*domains.Card, error)
	GetCardByPhone(ctx context.Context, phone string) ([]domains.Card, error)
	GetCardByPan(ctx context.Context, number, expire string) (*domains.Card, error)
	GetCardBalance(ctx context.Context, number string) (*domains.Card, error)
	AuthTransaction(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error)
	ConfirmTransaction(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error)
	GetTransactionByExt(ctx context.Context, transaction domains.PSTransaction) (domains.PSTransaction, error)
	CancelTransaction(ctx context.Context, transaction domains.PSTransaction) (domains.PSTransaction, error)
	ReversalTransactionV1(ctx context.Context, transaction domains.PSTransaction) (domains.PSTransaction, error)
	ToCard(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error)
	Reconciliation(ctx context.Context, terminal domains.Terminal) (domains.Reconciliation, error)
}

type UseCase struct {
	humo DriverHumo
}

func New(humo DriverHumo) *UseCase {
	return &UseCase{
		humo: humo,
	}
}

func (uc *UseCase) GetCardByPhone(ctx context.Context, phone string) ([]domains.Card, error) {
	var l = logger.FromCtx(ctx, "service_humo.GetCardByPhone")
	cards, err := uc.humo.GetCardByPhone(ctx, phone)
	if err != nil {
		l.Error("uc.humo.GetCardByPhone failed", zap.Error(err))

		return nil, err
	}

	return cards, nil
}

func (uc *UseCase) GetCardByPan(ctx context.Context, number, expiry string) (*domains.Card, error) {
	var l = logger.FromCtx(ctx, "service_humo.GetCardByPhone")
	card, err := uc.humo.GetCardByPan(ctx, number, expiry)
	if err != nil {
		l.Error("uc.humo:GetCardByPan failed", zap.Error(err))

		return nil, err
	}

	return card, nil
}

func (uc *UseCase) GetCardBalance(ctx context.Context, number string) (*domains.Card, error) {
	var l = logger.FromCtx(ctx, "service_humo.GetCardBalance")
	card, err := uc.humo.GetCardBalance(ctx, number)
	if err != nil {
		l.Error("uc.humo:GetCardBalance failed", zap.Error(err))

		return nil, err
	}

	return card, nil
}

func (uc *UseCase) GetCardMaskedPAN(ctx context.Context, cardNumber string) (*domains.Card, error) {
	var l = logger.FromCtx(ctx, "service_humo.GetCardMaskedPAN")
	card, err := uc.humo.GetCardMaskedPAN(ctx, cardNumber)
	if err != nil {
		l.Error("uc.humo:GetCardMaskedPAN failed", zap.Error(err))

		return nil, err
	}

	return card, nil
}

func (uc *UseCase) GetCardNumber(ctx context.Context, cardID string) (*domains.Card, error) {
	var l = logger.FromCtx(ctx, "service_humo.GetCardNumber")
	card, err := uc.humo.GetCardNumber(ctx, cardID)
	if err != nil {
		l.Error("uc.humo:GetCardNumber", zap.Error(err))

		return nil, err
	}

	return card, nil
}

func (uc *UseCase) AuthTransaction(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error) {
	var l = logger.FromCtx(ctx, "service_humo.AuthTransaction")

	auth, err := uc.humo.AuthTransaction(ctx, tran)
	if err != nil {
		l.Error("uc.humo.AuthTransaction failed", zap.Error(err))

		return domains.PSTransaction{}, err
	}

	return auth, nil
}

func (uc *UseCase) ConfirmTransaction(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error) {
	var l = logger.FromCtx(ctx, "service_humo.ConfirmTransaction")
	auth, err := uc.humo.ConfirmTransaction(ctx, tran)
	if err != nil {
		l.Error("uc.humo.ConfirmTransaction failed", zap.Error(err))

		return domains.PSTransaction{}, err
	}

	return auth, nil
}

func (uc *UseCase) GetTransactionByExt(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error) {
	var l = logger.FromCtx(ctx, "service_humo.GetTransactionByExt")
	tran, err := uc.humo.GetTransactionByExt(ctx, tran)
	if err != nil {
		l.Error("uc.humo:GetTransactionByExt failed", zap.Error(err))

		return domains.PSTransaction{}, err
	}

	return tran, nil
}

func (uc *UseCase) CancelTransaction(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error) {
	var l = logger.FromCtx(ctx, "service_humo.CancelTransaction")
	tran, err := uc.humo.CancelTransaction(ctx, tran)
	if err != nil {
		l.Error("uc.humo:CancelTransaction failed", zap.Error(err))

		return domains.PSTransaction{}, err
	}

	return tran, nil
}

// Variant 1
func (uc *UseCase) ReversalTransactionV1(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error) {
	var l = logger.FromCtx(ctx, "service_humo.ReversalTransactionV1")
	tran, err := uc.humo.ReversalTransactionV1(ctx, tran)
	if err != nil {
		l.Error("uc.humo.ReversalTransactionV1 failed", zap.Error(err))

		return domains.PSTransaction{}, err
	}

	return tran, nil
}

// Variant 2
func (uc *UseCase) ToCard(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error) {
	var l = logger.FromCtx(ctx, "service_humo.ToCard")
	tran, err := uc.humo.ToCard(ctx, tran)
	if err != nil {
		l.Error("uc.humo.ToCard failed", zap.Error(err))

		return domains.PSTransaction{}, err
	}

	return tran, nil
}

func (uc *UseCase) Reconciliation(ctx context.Context, terminal domains.Terminal) (domains.Reconciliation, error) {
	var l = logger.FromCtx(ctx, "service_humo.Reconciliation")
	tran, err := uc.humo.Reconciliation(ctx, terminal)
	if err != nil {
		l.Error("uc.humo:Reconciliation failed", zap.Error(err))

		return domains.Reconciliation{}, err
	}

	return tran, nil
}
