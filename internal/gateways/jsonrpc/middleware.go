package jsonrpc

import (
	"context"
	"fmt"
	"net/http"

	"go.uber.org/zap"
)

type merSource interface {
	GetMerchant(ctx context.Context, key string) (domains.Merchant, error)
}

// Define our struct
type authenticationMiddleware struct {
	merSource merSource
	merchant  domains.Merchant
}

// Middleware function, which will be called for each request
func (amw *authenticationMiddleware) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var (
			l   = logger.FromCtx(context.Background(), "middleware.Middleware")
			key = r.Header.Get("merchant-key")
			err error
		)
		amw.merchant, err = amw.merSource.GetMerchant(context.Background(), key)
		if err != nil || amw.merchant.ID == "" || amw.merchant.State == domains.MerchantStatePassive {
			l.Error("amw.merSource.GetMerchant failed", zap.Error(err))

			w.WriteHeader(http.StatusForbidden)
			fmt.Fprintf(w, `{"jsonrpc": "2.0", "error": 
				{"code": %v, "message": "%v"}, "id": null}`, errs.MerchantNotFound.Code, errs.MerchantNotFound.Message)
		} else {
			values := r.URL.Query()
			values.Add("merchant-id", string(amw.merchant.ID))
			r.URL.RawQuery = values.Encode()
			// Pass down the request to the next middleware (or final handler)
			next.ServeHTTP(w, r)
		}
	})
}
