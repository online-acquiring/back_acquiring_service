package jsonrpc

import (
	"context"
	"encoding/json"

	"go.uber.org/zap"
)

const env = "preprod"

type cardSource interface {
	GetAllCardsByPhone(ctx context.Context, phone string) ([]*domains.Card, *errs.Error)
	CreateCard(ctx context.Context, card domains.PayCard) (string, *errs.Error)
	VerifyCard(ctx context.Context, key, smsCode, mrID string) (domains.PayCard, *errs.Error)
	InfoCard(ctx context.Context, id, mrID string) (*domains.Card, *errs.Error)
}

type Cards struct {
	card cardSource
	cfg  config.Config
}

type reqCardsParam struct {
	Phone string `json:"phone"`
}

type reqCard struct {
	CardID    string `json:"card_id"`
	MaskedPAN string `json:"masked_pan"`
	Expiry    string `json:"expiry"`
}

type respCardsParam struct {
	Cards []reqCard `json:"cards"`
}

func (c *Cards) List(params interface{}) (interface{}, *errs.Error) {
	var (
		ctx = context.Background()
		l   = logger.FromCtx(ctx, "cards.List")
	)

	if c.cfg.Environment == env {
		return []reqCard{
			{CardID: "4E6E2A4BCAF24955BBA4A05D79A5034C", MaskedPAN: "8600 **** **** 0318"},
		}, nil
	}

	mParams, err := json.Marshal(params)
	if err != nil {
		l.Error("json.Marshal failed", zap.Error(err))

		return nil, errs.NewError(errs.ErrorCodeInternalServer, err.Error())
	}

	var (
		args []reqCardsParam
		resp respCardsParam
	)
	err = json.Unmarshal(mParams, &args)
	if err != nil {
		l.Error("json.Unmarshal failed", zap.Error(err))

		return nil, errs.NewError(errs.ErrorCodeInternalServer, err.Error())
	}

	cards, errC := c.card.GetAllCardsByPhone(ctx, args[0].Phone)
	if errC != nil {
		l.Error("c.cards.GetAllCardsByPhone failed", zap.Error(errC))

		return nil, errC
	}

	for i := range cards {
		card := reqCard{
			CardID:    cards[i].ID,
			MaskedPAN: cards[i].Number,
			Expiry:    cards[i].Expiry,
		}

		resp.Cards = append(resp.Cards, card)
	}

	return resp, nil
}

type reqCreateParams struct {
	Number string `json:"number"`
	Expiry string `json:"expiry"`
}

type respCreateParams struct {
	Number string `json:"number"`
	Expiry string `json:"expiry"`
	Key    string `json:"key"`
}

func (c *Cards) Create(params interface{}) (interface{}, *errs.Error) {
	var (
		ctx = context.Background()
		l   = logger.FromCtx(ctx, "cards.Create")
	)

	if c.cfg.Environment == env {
		return respCreateParams{
			Number: "8600 **** **** 0318",
			Expiry: "1225",
			Key:    "2f36d5e8-1735-47da-9e28-492aae9dd327",
		}, nil
	}

	mParams, err := json.Marshal(params)
	if err != nil {
		l.Error("json.Marshal failed", zap.Error(err))

		return nil, errs.NewError(errs.ErrorCodeInternalServer, err.Error())
	}

	var args []reqCreateParams
	err = json.Unmarshal(mParams, &args)
	if err != nil {
		l.Error("json.Unmarshal failed", zap.Error(err))

		return nil, errs.NewError(errs.ErrorCodeInternalServer, err.Error())
	}

	pCard := domains.PayCard{
		BIN:    args[0].Number,
		Expiry: args[0].Expiry,
	}
	key, errC := c.card.CreateCard(ctx, pCard)
	if errC != nil {
		return nil, errC
	}

	resp := respCreateParams{
		Number: helpers.ConvertMask(args[0].Number),
		Expiry: args[0].Expiry,
		Key:    key,
	}

	return resp, nil
}

type reqVerifyParams struct {
	Key         string `json:"key"`
	ConfirmCode string `json:"confirm_code"`
}

type respVerifyParams struct {
	ID     string `json:"id"`
	Number string `json:"number"`
	Expiry string `json:"expiry"`
}

func (c *Cards) Verify(params interface{}, mrID string) (interface{}, *errs.Error) {
	var (
		ctx = context.Background()
		l   = logger.FromCtx(ctx, "cards.Create")
	)

	if c.cfg.Environment == env {
		return respVerifyParams{
			ID:     "2f36d5e8-1735-47da-9e28-492aae9dd327",
			Number: "8600 **** **** 0318",
			Expiry: "1225",
		}, nil
	}

	mParams, err := json.Marshal(params)
	if err != nil {
		l.Error("json.Marshal failed", zap.Error(err))

		return nil, errs.NewError(errs.ErrorCodeInternalServer, err.Error())
	}

	var args []reqVerifyParams
	err = json.Unmarshal(mParams, &args)
	if err != nil {
		l.Error("json.Unmarshal failed", zap.Error(err))

		return nil, errs.NewError(errs.ErrorCodeInternalServer, err.Error())
	}

	card, errV := c.card.VerifyCard(ctx, args[0].Key, args[0].ConfirmCode, mrID)
	if errV != nil {
		return nil, errV
	}

	resp := respVerifyParams{
		ID:     card.ID,
		Number: card.MaskedPAN,
		Expiry: card.Expiry,
	}

	return resp, nil
}

type reqInfoParams struct {
	CardID string `json:"card_id"`
}

type respInfoParams struct {
	Number  string  `json:"number"`
	Expiry  string  `json:"expiry"`
	Balance float64 `json:"balance"`
}

func (c *Cards) Info(params interface{}, mrID string) (interface{}, *errs.Error) {
	var (
		ctx = context.Background()
		l   = logger.FromCtx(ctx, "cards.Info")
	)

	if c.cfg.Environment == env {
		return respInfoParams{
			Number:  "8600 **** **** 0318",
			Expiry:  "1225",
			Balance: 1000,
		}, nil
	}

	mParams, err := json.Marshal(params)
	if err != nil {
		l.Error("json.Marshal failed", zap.Error(err))

		return nil, errs.NewError(errs.ErrorCodeInternalServer, err.Error())
	}

	var args []reqInfoParams
	err = json.Unmarshal(mParams, &args)
	if err != nil {
		l.Error("json.Unmarshal failed", zap.Error(err))

		return nil, errs.NewError(errs.ErrorCodeInternalServer, err.Error())
	}

	card, errV := c.card.InfoCard(ctx, args[0].CardID, mrID)
	if errV != nil {
		return nil, errV
	}

	resp := respInfoParams{
		Number:  card.Number,
		Expiry:  card.Expiry,
		Balance: card.Balance,
	}

	return resp, nil
}
