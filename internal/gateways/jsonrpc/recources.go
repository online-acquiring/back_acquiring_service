package jsonrpc

type Response struct {
	JSONRPC string      `json:"jsonrpc"`
	Result  interface{} `json:"result,omitempty"`
	ID      string      `json:"id"`
}

func view(data interface{}, id string) Response {
	return Response{
		JSONRPC: "2.0",
		Result:  data,
		ID:      id,
	}
}
