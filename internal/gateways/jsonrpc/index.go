package jsonrpc

import (
	"context"
	json1 "encoding/json"
	"fmt"
	"net/http"
	"time"

	"go.uber.org/zap"

	"github.com/gorilla/mux"
)

type Server struct {
	cfg        config.Config
	router     *mux.Router
	payCreate  createPay
	payConfirm confirmPay
	payCancel  cancelPay
	card       cardSource
	payDtsGet  getDetails
	log        logger.Logger
}

func New(port string,
	log logger.Logger,
	cfg config.Config,
	pay createPay,
	payConfirm confirmPay,
	payCancel cancelPay,
	card cardSource,
	merSource merSource,
	payDtsGet getDetails,
) *http.Server {
	var (
		router = mux.NewRouter()
		amw    authenticationMiddleware
	)

	srv := &Server{
		log:        log,
		cfg:        cfg,
		router:     router,
		payCreate:  pay,
		payConfirm: payConfirm,
		payCancel:  payCancel,
		card:       card,
		payDtsGet:  payDtsGet,
	}

	router.HandleFunc("/api/v1/ecomm", srv.jsonRPCHandler)
	amw.merSource = merSource

	router.Use(amw.Middleware)

	var httpServer = &http.Server{
		Addr:              port,
		ReadHeaderTimeout: 60 * time.Second,
		Handler:           srv,
	}
	srv.log.Info(fmt.Sprintf("Json RPC server is initialized on port: %v", port))

	return httpServer
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	s.router.ServeHTTP(w, r)
}

func (s *Server) jsonRPCHandler(w http.ResponseWriter, r *http.Request) {
	var l = logger.FromCtx(context.Background(), "index.jsonRPCHandler")
	// check method
	if r.Method != http.MethodPost {
		l.Error("http method is wrong", zap.String("http method", r.Method))

		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, `{"jsonrpc": "2.0", "error": {"code": %v, "message": "http method is wrong"}, "id": null}`, errs.ErrorCodeWrongInputParams)

		return
	}

	// Распарсить запрос JSON-RPC из тела запроса
	var req map[string]interface{}
	err := json1.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		l.Error("json1.NewDecoder failed", zap.Error(err))

		// Кастомизированный ответ при ошибке парсинга запроса
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, `{"jsonrpc": "2.0", 
		"error": {"code": %v, "message": "Request parsing error"}, "id": null}`,
			errs.ErrorCodeWrongInputParams)

		return
	}

	// Обработать запрос и получить ответ
	id, ok := req["id"].(string)
	if !ok {
		l.Error("check id failed", zap.Any("raw-param", req))

		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, `{"jsonrpc": "2.0", 
		"error": {"code": %v, "message": "Error parsing request type id wrong"}, "id": null}`,
			errs.ErrorCodeWrongInputParams)

		return
	}

	var mrID = r.URL.Query().Get("merchant-id")
	resp, errC := s.handleJSONRPCRequest(req, mrID)
	if errC != nil {
		l.Error("s.handleJSONRPCRequest failed", zap.Error(errC))

		// Кастомизированный ответ при ошибке обработки запроса
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, `{"jsonrpc": "2.0", "error": {"code": %v, "message": "%s"}, "id": null}`, errC.ErrCode(), errC.Msg())

		return
	}

	// Отправить успешный ответ
	w.Header().Set("Content-Type", "application/json")
	err = json1.NewEncoder(w).Encode(view(resp, id))
	if err != nil {
		fmt.Fprintf(w, `{"jsonrpc": "2.0", "error": {"code": %v, "message": "%s"}, "id": null}`, errs.InternalError, err.Error())

		return
	}
}

func (s *Server) handleJSONRPCRequest(req map[string]interface{}, mrID string) (interface{}, *errs.Error) {
	var l = logger.FromCtx(context.Background(), "index.handleJSONRPCRequest")
	// Проверяем наличие полей "method" и "params" в запросе
	if _, ok := req["method"]; !ok {
		l.Error("check method failed", zap.Any("raw-param", req))

		return nil, errs.MethodNotFound
	}

	if _, ok := req["params"]; !ok {
		l.Error("check method failed", zap.Any("raw-param", req))

		return nil, errs.ParamsNotFound
	}

	// Получаем название метода и параметры из запроса
	method := req["method"].(string)
	params := req["params"]
	// Вызываем соответствующий метод для обработки запроса
	acqPay := new(Payment)
	acqPay.cfg = s.cfg

	acqCards := new(Cards)
	acqCards.card = s.card
	acqCards.cfg = s.cfg

	switch method {
	case "pay.get":
		acqPay.getDtsPay = s.payDtsGet
		return acqPay.Get(params)
	case "pay.create":
		acqPay.createPay = s.payCreate
		acqPay.confirmPay = s.payConfirm
		return acqPay.Create(params, mrID)
	case "pay.confirm":
		acqPay.confirmPay = s.payConfirm
		return acqPay.Confirm(params)
	case "pay.confirmHold":
		acqPay.confirmPay = s.payConfirm
		return acqPay.ConfirmHold(params)
	case "pay.cancel":
		acqPay.cancelPay = s.payCancel
		return acqPay.Cancel(params, mrID)
	case "pay.cancelPartial":
		acqPay.cancelPay = s.payCancel
		return acqPay.CancelPartial(params, mrID)
	case "card.list":
		return acqCards.List(params)
	case "card.create":
		return acqCards.Create(params)
	case "card.verify":
		return acqCards.Verify(params, mrID)
	case "card.info":
		return acqCards.Info(params, mrID)
	default:
		return nil, errs.MethodUnknown
	}
}
