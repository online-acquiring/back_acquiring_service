package jsonrpc

import (
	"context"
	"strconv"
	"time"

	"go.uber.org/zap"
)

const minSumPayment = 0

type createPay interface {
	CreatePay(ctx context.Context, req domains.Payment, card domains.Card) (string, bool, *errs.Error)
}

type confirmPay interface {
	ConfirmPay(ctx context.Context, req domains.Payment, smsCode string, hold bool) (domains.Payment, *errs.Error)
	ConfirmHoldPay(ctx context.Context, req domains.Payment) (domains.Payment, *errs.Error)
}

type cancelPay interface {
	CancelPayment(ctx context.Context, payID domains.ID, mrID string) *errs.Error
	CancelPartialPayment(ctx context.Context, mrID string, payID domains.ID, pAmount domains.Money, pDts []domains.Detail) *errs.Error
}

type getDetails interface {
	Get(ctx context.Context, pay domains.Payment) (domains.Payment, error)
}

type Payment struct {
	createPay  createPay
	confirmPay confirmPay
	cancelPay  cancelPay
	getDtsPay  getDetails
	cfg        config.Config
}

type Card struct {
	ID     string `json:"id,omitempty"`
	Number string `json:"number,omitempty"`
	Expiry string `json:"expiry,omitempty"`
}

type Detail struct {
	Field string `json:"field"`
	Value string `json:"value"`
}

type PayerData struct {
	Nationality     string `json:"nationality,omitempty"`
	DocType         string `json:"doc_type,omitempty"`
	SerialNo        string `json:"serial_no,omitempty"`
	IDCard          string `json:"id_card,omitempty"`
	DocValidThrough string `json:"doc_valid_through,omitempty"`
	PersonCode      string `json:"person_code,omitempty"`
	Surname         string `json:"surname,omitempty"`
	FirstName       string `json:"first_name,omitempty"`
	MiddleName      string `json:"middle_name,omitempty"`
	BirthDate       string `json:"birth_date,omitempty"`
}

type ReqCheck struct {
	MerchantKey  string    `json:"merchant_key" bind:"required"`
	ExternalID   string    `json:"external_id" binding:"required"`
	Amount       int64     `json:"amount" binding:"required"`
	CurrencyCode string    `json:"currency_code" binding:"required" default:"860,840.."`
	Card         Card      `json:"card"`
	Details      []Detail  `json:"details"`
	PayerData    PayerData `json:"payer_data"`
}

type RespCheck struct {
	PayID         string `json:"pay_id"`
	ConfirmMethod string `json:"confirm_method"`
	FeeAmount     int    `json:"fee_amount"`
}

func (p *Payment) Create(params interface{}, mrID string) (interface{}, *errs.Error) {
	var l = logger.FromCtx(context.Background(), "payment.Create")
	if p.cfg.Environment == env {
		return RespCheck{
			PayID:         "79580137-e358-4bb3-b7e0-06e2a3893273",
			ConfirmMethod: "SMS",
			FeeAmount:     0,
		}, nil
	}

	mParams, err := json.Marshal(params)
	if err != nil {
		l.Error("json.Marshal failed", zap.Error(err))

		return nil, errs.NewError(errs.ErrorCodeInternalServer, err.Error())
	}

	var args []ReqCheck
	err = json.Unmarshal(mParams, &args)
	if err != nil {
		l.Error("json.Unmarshal failed", zap.Error(err))

		return nil, errs.NewError(errs.ErrorCodeInternalServer, err.Error())
	}

	var (
		ctx        = context.Background()
		payReq     = domains.Payment{}
		card       = domains.Card{}
		respCreate = RespCheck{}
	)

	if args[0].Amount <= 0 {
		l.Error("total sum has to bigger than 0")

		return nil, errs.NewError(errs.ErrorCodeInternalServer, "total sum has to bigger than 0")
	}

	payReq.Merchant.ID = domains.ID(mrID)
	payReq.Card.ID = args[0].Card.ID
	payReq.Card.Expiry = args[0].Card.Expiry
	payReq.Amount = domains.Money(args[0].Amount)
	payReq.CurrencyCode = args[0].CurrencyCode
	payReq.ExternalID = args[0].ExternalID
	payReq.PayerData = domains.PayerData(args[0].PayerData)

	card.ID = args[0].Card.ID
	card.Number = args[0].Card.Number
	card.Expiry = args[0].Card.Expiry

	dts := make([]domains.Detail, len(args[0].Details))
	for i := range args[0].Details {
		dts[i].Field = args[0].Details[i].Field
		dts[i].Value = args[0].Details[i].Value
	}

	payReq.Details = dts
	payID, confirmCode, errC := p.createPay.CreatePay(ctx, payReq, card)
	if errC != nil {
		l.Error("p.createPay.CreatePay failed", zap.Error(errC))

		return nil, errC
	}

	respCreate.PayID = payID
	respCreate.FeeAmount = 0
	respCreate.ConfirmMethod = "SMS"
	if confirmCode {
		respCreate.ConfirmMethod = "NONE"
	}

	return respCreate, nil
}

type ReqConfirm struct {
	PayID       string `json:"pay_id" bind:"required"`
	ConfirmCode string `json:"confirm_code" bind:"required"`
	Hold        bool   `json:"hold" bind:"required"`
}

type RespConfirm struct {
	Card       Card      `json:"card"`
	ExternalID string    `json:"external_id"`
	State      string    `json:"state"`
	Details    []Detail  `json:"details"`
	PayerData  PayerData `json:"payer_data"`
}

func (p *Payment) Confirm(params interface{}) (interface{}, *errs.Error) {
	var l = logger.FromCtx(context.Background(), "payment.Confirm")
	if p.cfg.Environment == env {
		return RespConfirm{
			Card: Card{
				ID: "4E6E2A4BCAF24955BBA4A05D79A5034C",
			},
			ExternalID: "8c3f2eed-5ffe-4761-bab2-722a1f43bf46",
			State:      "2",
			Details: []Detail{
				{Field: "created_at", Value: "04.08.2023"},
			},
			PayerData: PayerData{
				Nationality:     "UZB",
				DocType:         "001",
				SerialNo:        "AA",
				IDCard:          "3339379",
				DocValidThrough: "2023-12-31",
				PersonCode:      "12345678912345",
				Surname:         "Odilov",
				FirstName:       "Jasur",
				MiddleName:      "Ilxomjon ogli",
				BirthDate:       "09.09.1995",
			},
		}, nil
	}

	mParams, err := json.Marshal(params)
	if err != nil {
		l.Error("json.Marshal failed", zap.Error(err))

		return nil, errs.NewError(errs.ErrorCodeInternalServer, err.Error())
	}

	var args []ReqConfirm

	if err = json.Unmarshal(mParams, &args); err != nil {
		l.Error("json.Unmarshal failed", zap.Error(err))

		return nil, errs.NewError(errs.ErrorCodeInternalServer, err.Error())
	}

	var (
		ctx         = context.Background()
		respConfirm = RespConfirm{}
		reqConfirm  = domains.Payment{}
	)

	reqConfirm.ID = domains.ID(args[0].PayID)

	payment, errC := p.confirmPay.ConfirmPay(ctx, reqConfirm, args[0].ConfirmCode, args[0].Hold)
	if errC != nil {
		l.Error("p.confirmPay.ConfirmPay failed", zap.Error(errC))

		return nil, errC
	}

	respConfirm.PayerData = PayerData(payment.PayerData)
	respConfirm.State = strconv.Itoa(int(payment.State))
	respConfirm.Card = Card{
		ID:     payment.Card.ID,
		Number: payment.Card.MaskedPAN,
		Expiry: payment.Card.Expiry,
	}
	respConfirm.ExternalID = payment.ExternalID

	for i := range payment.Details {
		resD := Detail{
			Field: payment.Details[i].Field,
			Value: payment.Details[i].Value,
		}

		respConfirm.Details = append(respConfirm.Details, resD)
	}

	return respConfirm, nil
}

type ReqConfirmHold struct {
	PayID string `json:"pay_id" bind:"required"`
}

type RespConfirmHold struct {
	Card      Card      `json:"card"`
	State     int       `json:"state"`
	Details   []Detail  `json:"details"`
	PayerData PayerData `json:"payer_data"`
}

func (p *Payment) ConfirmHold(params interface{}) (interface{}, *errs.Error) {
	var l = logger.FromCtx(context.Background(), "payment.ConfirmHold")
	if p.cfg.Environment == env {
		return RespConfirm{
			Card: Card{
				ID: "4E6E2A4BCAF24955BBA4A05D79A5034C",
			},
			ExternalID: "8c3f2eed-5ffe-4761-bab2-722a1f43bf46",
			State:      "3",
			Details: []Detail{
				{Field: "created_at", Value: "04.08.2023"},
			},
			PayerData: PayerData{
				Nationality:     "UZB",
				DocType:         "001",
				SerialNo:        "AA",
				IDCard:          "3339379",
				DocValidThrough: "2023-12-31",
				PersonCode:      "12345678912345",
				Surname:         "Odilov",
				FirstName:       "Jasur",
				MiddleName:      "Ilxomjon ogli",
				BirthDate:       "09.09.1995",
			},
		}, nil
	}

	mParams, err := json.Marshal(params)
	if err != nil {
		l.Error("json.Marshal failed", zap.Error(err))

		return nil, errs.NewError(errs.ErrorCodeInternalServer, err.Error())
	}

	var args []ReqConfirmHold
	if err = json.Unmarshal(mParams, &args); err != nil {
		l.Error("json.Unmarshal failed", zap.Error(err))

		return nil, errs.NewError(errs.ErrorCodeInternalServer, err.Error())
	}

	var (
		ctx          = context.Background()
		respConfHold = RespConfirmHold{}
		reqConfHold  = domains.Payment{}
	)

	reqConfHold.ID = domains.ID(args[0].PayID)

	payment, errC := p.confirmPay.ConfirmHoldPay(ctx, reqConfHold)
	if errC != nil {
		l.Error("p.confirmPay.ConfirmHoldPay failed", zap.Error(errC))

		return nil, errC
	}

	respConfHold.Card = Card{
		ID:     payment.Card.ID,
		Number: payment.Card.MaskedPAN,
		Expiry: payment.Card.Expiry,
	}
	respConfHold.State = int(payment.State)
	for i := range payment.Details {
		resD := Detail{
			Field: payment.Details[i].Field,
			Value: payment.Details[i].Value,
		}

		respConfHold.Details = append(respConfHold.Details, resD)
	}

	respConfHold.PayerData = PayerData(payment.PayerData)

	return respConfHold, nil
}

type ReqCancelPayment struct {
	PayID string `json:"pay_id"`
}

type RespCancelPayment struct {
	PayID string `json:"pay_id"`
}

func (p *Payment) Cancel(params interface{}, mrID string) (interface{}, *errs.Error) {
	var l = logger.FromCtx(context.Background(), "payment.Cancel")
	if p.cfg.Environment == env {
		return RespCancelPayment{
			PayID: "6954b88c-121a-4e48-afa3-b254cc95be9a",
		}, nil
	}

	mParams, err := json.Marshal(params)
	if err != nil {
		l.Error("json.Marshal failed", zap.Error(err))

		return nil, errs.NewError(errs.ErrorCodeInternalServer, err.Error())
	}

	var args []ReqCancelPayment

	if err = json.Unmarshal(mParams, &args); err != nil {
		l.Error("json.Unmarshal failed", zap.Error(err))

		return nil, errs.NewError(errs.ErrorCodeInternalServer, err.Error())
	}

	var resp = RespCancelPayment{}
	errC := p.cancelPay.CancelPayment(context.Background(), domains.ID(args[0].PayID), mrID)
	if errC != nil {
		l.Error("p.cancelPay.CancelPayment failed", zap.Error(errC))

		return nil, errC
	}

	resp.PayID = args[0].PayID

	return resp, nil
}

type ReqGetDetails struct {
	PayID string `json:"pay_id"`
}

type RespGetDetails struct {
	CreateAt  time.Time `json:"create_at,omitempty"`
	Amount    int64     `json:"amount,omitempty"`
	State     int       `json:"state,omitempty"`
	Card      Card      `json:"card"`
	Details   []Detail  `json:"details,omitempty"`
	PayerData PayerData `json:"payer_data"`
}

func (p *Payment) Get(params interface{}) (interface{}, *errs.Error) {
	var l = logger.FromCtx(context.Background(), "payment.Get")
	if p.cfg.Environment == env {
		return RespGetDetails{
			CreateAt: time.Now(),
			Amount:   10400,
			State:    5,
			Card: Card{
				ID:     "4E6E2A4BCAF24955BBA4A05D79A5034C",
				Number: "8600 **** **** 0318",
			},
			Details: []Detail{
				{Field: "ticket_id", Value: "123456"},
			},
			PayerData: PayerData{
				Nationality:     "UZB",
				DocType:         "001",
				SerialNo:        "AA",
				IDCard:          "3339379",
				DocValidThrough: "2023-12-31",
				PersonCode:      "12345678912345",
				Surname:         "Odilov",
				FirstName:       "Jasur",
				MiddleName:      "Ilxomjon ogli",
				BirthDate:       "09.09.1995",
			},
		}, nil
	}

	mParams, err := json.Marshal(params)
	if err != nil {
		l.Error("json.Marshal failed", zap.Error(err))

		return nil, errs.NewError(errs.ErrorCodeInternalServer, err.Error())
	}

	var args []ReqGetDetails
	if err = json.Unmarshal(mParams, &args); err != nil {
		l.Error("json.Unmarshal failed", zap.Error(err))

		return nil, errs.NewError(errs.ErrorCodeInternalServer, err.Error())
	}

	var (
		resp RespGetDetails
		ctx  = context.Background()
	)

	details, errC := p.getDtsPay.Get(ctx, domains.Payment{ID: domains.ID(args[0].PayID)})
	if errC != nil {
		l.Error("p.getDtsPay.Get failed", zap.Error(errC))

		return nil, errs.PaymentNotFound
	}

	for _, detail := range details.Details {
		var dt = Detail{
			Field: detail.Field,
			Value: detail.Value,
		}

		resp.Details = append(resp.Details, dt)
	}

	resp.PayerData = PayerData(details.PayerData)
	resp.CreateAt = details.CreatedAt
	resp.Card = Card{
		ID:     details.Card.ID,
		Number: details.Card.MaskedPAN,
		Expiry: details.Card.Expiry,
	}
	resp.Amount = int64(details.Amount)
	resp.State = int(details.State)

	return resp, nil
}

type ReqCancelPartPayment struct {
	PayID   string   `json:"pay_id"`
	Amount  int64    `json:"amount"`
	Details []Detail `json:"details"`
}

type RespCancelPartPayment struct {
	PayID string `json:"pay_id"`
}

func (p *Payment) CancelPartial(params interface{}, mrID string) (interface{}, *errs.Error) {
	var l = logger.FromCtx(context.Background(), "payment.CancelPartial")
	if p.cfg.Environment == env {
		return RespCancelPayment{
			PayID: "6954b88c-121a-4e48-afa3-b254cc95be9a",
		}, nil
	}

	mParams, err := json.Marshal(params)
	if err != nil {
		l.Error("json.Marshal failed", zap.Error(err))

		return nil, errs.NewError(errs.ErrorCodeInternalServer, err.Error())
	}

	var args []ReqCancelPartPayment

	if err = json.Unmarshal(mParams, &args); err != nil {
		l.Error("json.Unmarshal failed", zap.Error(err))

		return nil, errs.NewError(errs.ErrorCodeInternalServer, err.Error())
	}

	// shu joyda minSumPayment ni 0 ga o'zgartirish kerak
	if args[0].Amount <= minSumPayment {
		l.Error("total sum has to bigger than 0", zap.String("amount", strconv.FormatInt(args[0].Amount, 10)))

		return nil, errs.NewError(errs.ErrorCodeInternalServer, "total sum has to bigger than 500")
	}

	dts := make([]domains.Detail, len(args[0].Details))
	for i := range args[0].Details {
		dts[i].Field = args[0].Details[i].Field
		dts[i].Value = args[0].Details[i].Value
	}

	errC := p.cancelPay.CancelPartialPayment(context.Background(), mrID, domains.ID(args[0].PayID), domains.Money(args[0].Amount), dts)
	if errC != nil {
		l.Error("p.cancelPay.CancelPartialPayment failed", zap.Error(errC))

		return nil, errC
	}

	var resp RespCancelPartPayment
	resp.PayID = args[0].PayID

	return resp, nil
}
