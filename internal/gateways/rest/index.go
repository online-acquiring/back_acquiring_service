package rest

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

type humoSource interface {
	GetCardMaskedPAN(ctx context.Context, cardNumber string) (*domains.Card, error)
	GetCardByPan(ctx context.Context, number, expire string) (*domains.Card, error)
	GetCardBalance(ctx context.Context, number string) (*domains.Card, error)
	ReversalTransactionV1(ctx context.Context, transaction domains.PSTransaction) (domains.PSTransaction, error)
	ToCard(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error)
}

type uzcardSource interface {
	GetCardByPan(ctx context.Context, number, expire string) (*domains.Card, error)
	CardNewOTP(ctx context.Context, pan, exDate, hash, phone string, tempID int) (string, error)
	CardNewVerify(ctx context.Context, pSignID, cCode string) (*domains.Card, error)
	ReversalTransactionV1(ctx context.Context, transaction domains.PSTransaction) (domains.PSTransaction, error)
	ReversalPartial(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error)
	ReversalToCard(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error)
}
type schedulerJob interface {
	CreateJob(code, exp string) (entryID int, err error)
	RemoveJob(entryID int)
}

type Server struct {
	log    logger.Logger
	router *gin.Engine
	cfg    *config.Config
	// cache            *redis_cache.RedisServ
	userAction       user_action.DriverUser
	merchantAction   merchant_action.DriverMerchant
	terminalAction   terminal_action.DriverTerminal
	accountAction    accounts_action.DriverAccount
	commissionAction commission_action.DriverCommission
	paymentAction    pay_source.DriverPayment
	humoAction       humoSource
	uzcardAction     uzcardSource
	refundAction     pay_refund.DriverPayRefund
	renewCard        card_renew.DriverRenewCard
	param            param_action.DriverParam
	scheduler        scheduler_action.DriverScheduler
	schJob           schedulerJob
	actObject        objects.DriverObject
}

func New(port string,
	log logger.Logger,
	cfg *config.Config,
	u user_action.DriverUser,
	m merchant_action.DriverMerchant,
	t terminal_action.DriverTerminal,
	a accounts_action.DriverAccount,
	c commission_action.DriverCommission,
	p pay_source.DriverPayment,
	humo humoSource,
	uzcard uzcardSource,
	refund pay_refund.DriverPayRefund,
	renewCard card_renew.DriverRenewCard,
	param param_action.DriverParam,
	sch scheduler_action.DriverScheduler,
	jobs schedulerJob,
	actObj objects.DriverObject,
) *http.Server {
	r := gin.New()
	r.Use(cors.CORSMiddleware())
	srv := &Server{
		log:    log,
		router: r,
		cfg:    cfg,
		//cache:            nil,
		userAction:       u,
		merchantAction:   m,
		terminalAction:   t,
		accountAction:    a,
		commissionAction: c,
		paymentAction:    p,
		humoAction:       humo,
		uzcardAction:     uzcard,
		refundAction:     refund,
		renewCard:        renewCard,
		param:            param,
		scheduler:        sch,
		schJob:           jobs,
		actObject:        actObj,
		//cardSource:       card,
	}
	srv.endpoints()
	httpServer := &http.Server{
		Addr:              port,
		Handler:           srv,
		ReadHeaderTimeout: 70 * time.Second,
	}
	srv.log.Info(fmt.Sprintf("HTTP server is initialized on port: %v", port))
	return httpServer
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}
