package rest

//nolint:goimports
import (
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type merchant struct {
	ID             string `json:"id"`
	ContractNumber string `json:"contract_number"`
	Inn            string `json:"inn"`
	MerchOrgName   string `json:"merch_name"`
	ShortName      string `json:"short_name"`
	Mail           string `json:"mail"`
	Address        string `json:"address"`
	Mobile         string `json:"mobile"`
	Tax            string `json:"tax"`
	State          *int8  `json:"state,omitempty"`
	DocCreatedAt   string `json:"doc_created_at"`
	FeeType        int8   `json:"fee_type"`
	Value          string `json:"value"`
	ComType        int8   `json:"com_type"`
	TerminalNum    string `json:"terminal_num"`
	MerchantNum    string `json:"merchant_num"`
	AccountNumber  string `json:"account_number"`
	TerminalID     string `json:"terminal_id"`
	TerminalType   int8   `json:"terminal_type"`
	AccountID      string `json:"account_id"`
	CreatedAt      string `json:"created_at"`
	UpdatedAt      string `json:"updated_at"`
}

const hundred = 100

func toMerchantsView(ctx context.Context, m []domains.Merchant) []merchant {
	views := make([]merchant, len(m))
	switch ctx.Value("user_role").(string) {
	case strconv.Itoa(int(domains.SuperAdminRole)):
		for i := range m {
			state := new(int8)
			*state = int8(m[i].State)
			var terminalType int8 = 0
			if m[i].Terminal.PSCode == domains.PSCodeHUMO {
				terminalType = 1
			}
			value := float64(m[i].Commission.Value)
			if m[i].Commission.ComType == 1 {
				value /= hundred
			}
			views[i] = merchant{
				ID:             string(m[i].ID),
				ContractNumber: m[i].ContractNumber,
				Inn:            m[i].Inn,
				MerchOrgName:   m[i].OrgName,
				ShortName:      m[i].ShortName,
				Mail:           m[i].Mail,
				Address:        m[i].Address,
				Mobile:         m[i].Mobile,
				Tax:            m[i].Tax,
				Value:          validation.GetSumView(value),
				State:          state,
				DocCreatedAt:   tools.ParseDateToViewString(m[i].DocCreatedAt),
				FeeType:        int8(m[i].Commission.FeeType),
				ComType:        int8(m[i].Commission.ComType),
				TerminalNum:    m[i].Terminal.TerminalNum,
				MerchantNum:    m[i].Terminal.MerchantNum,
				AccountNumber:  m[i].Account.Code,
				TerminalID:     string(m[i].Terminal.ID),
				TerminalType:   terminalType,
				AccountID:      string(m[i].Account.ID),
				CreatedAt:      tools.ParseTimeToViewString(m[i].Dates.CreatedAt),
				UpdatedAt:      tools.ParseTimeToViewString(m[i].Dates.UpdatedAt),
			}
		}
	default:
		for i := range m {
			var terminalType int8 = 0
			if m[i].Terminal.PSCode == domains.PSCodeHUMO {
				terminalType = 1
			}
			value := float64(m[i].Commission.Value)
			if m[i].Commission.ComType == 1 {
				value /= hundred
			}

			views[i] = merchant{
				ID:             string(m[i].ID),
				ContractNumber: m[i].ContractNumber,
				Inn:            m[i].Inn,
				MerchOrgName:   m[i].OrgName,
				ShortName:      m[i].ShortName,
				Mail:           m[i].Mail,
				Address:        m[i].Address,
				Mobile:         m[i].Mobile,
				Tax:            m[i].Tax,
				DocCreatedAt:   tools.ParseDateToViewString(m[i].DocCreatedAt),
				FeeType:        int8(m[i].Commission.FeeType),
				Value:          validation.GetSumView(value),
				ComType:        int8(m[i].Commission.ComType),
				TerminalNum:    m[i].Terminal.TerminalNum,
				MerchantNum:    m[i].Terminal.MerchantNum,
				AccountNumber:  m[i].Account.Code,
				TerminalID:     string(m[i].Terminal.ID),
				TerminalType:   terminalType,
				AccountID:      string(m[i].Account.ID),
				CreatedAt:      tools.ParseTimeToViewString(m[i].Dates.CreatedAt),
				UpdatedAt:      tools.ParseTimeToViewString(m[i].Dates.UpdatedAt),
			}
		}
	}
	return views
}

// / Get All merchants
// @Summary Get All Merchants
// @Description Get All Merchants
// @Router /api/v1/ecomm/merchants [GET]
// @Tags Merchants
// @Accept json
// @Produce json
// @Param page query string false "query by page"
// @Param limit query string false "query by limit"
// @Success 200 {object} R{data=respMerchants}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) getMerchants() gin.HandlerFunc {
	return func(c *gin.Context) {
		merData, err := s.merchantAction.GetAllMerchants(c)
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeDB, err.Error()))
			return
		}
		c.JSON(http.StatusOK, view(
			respMerchants{Merchants: toMerchantsView(c, merData), Length: len(merData)}))
	}
}

type respMerchants struct {
	Merchants []merchant `json:"merchants"`
	Length    int        `json:"length"`
}

type reqMerchant struct {
	Merchant Merchant `json:"merchant"`
	FeeType  int8     `json:"fee_type"`
	Value    float64  `json:"value"`
	ComType  int8     `json:"com_type"`
}

type Merchant struct {
	ContractNumber string `json:"contract_num"`
	DocCreateDate  string `json:"doc_create_date,omitempty"`
	MerchName      string `json:"merch_name"`
	ShortMerchName string `json:"short_merch_name"`
	Inn            string `json:"inn"`
	Mail           string `json:"mail"`
	Address        string `json:"address"`
	Mobile         string `json:"mobile"`
}

type respMerchant struct {
	MerchantID string `json:"merchant_id"`
}

// / Add new merchant
// @Summary Add New Merchant
// @Description Add New Merchant
// @Router /api/v1/ecomm/merchants/createMerchant [POST]
// @Tags Merchants
// @Accept json
// @Produce json
// @Param reqMerchant body reqMerchant true "ID|Merchant|FeeType|Value|ComType"
// @Success 200 {object} R{data=respMerchant}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) addMerchant() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			req reqMerchant
			m   domains.Merchant
			com domains.Commission
			err error
		)
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})

			return
		}

		// 0-merchant,1-client,2-both
		switch req.FeeType {
		case int8(domains.FeeFromClient):
			com.FeeType = domains.FeeFromClient
		case int8(domains.FeeFromMerchant):
			com.FeeType = domains.FeeFromMerchant
		case int8(domains.FeeFromClientAndMerchant):
			com.FeeType = domains.FeeFromClientAndMerchant
		default:
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})
			return
		}

		// 0-%, 1-fixed
		switch req.ComType {
		case domains.TypePercent:
			com.ComType = domains.TypePercent
		case domains.TypeMoney:
			com.ComType = domains.TypeMoney
		default:
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})
			return
		}

		docCreateDate, err := tools.ParseDate(req.Merchant.DocCreateDate)
		if err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})

			return
		}

		m, err = s.merchantAction.CreateMerchant(c, domains.Merchant{
			ContractNumber: req.Merchant.ContractNumber,
			Inn:            req.Merchant.Inn,
			OrgName:        req.Merchant.MerchName,
			ShortName:      req.Merchant.ShortMerchName,
			DocCreatedAt:   *docCreateDate,
			State:          domains.MerchantStateCreated,
			//Mail:         "otabek94_30@mail.ru",
			//Address:      "minavarhoji",
			//Mobile:       "998999072754",
			//Tax:          "bor",
		})
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeDB, err.Error()))

			return
		}

		if req.Value < 0 {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeDB, err.Error()))

			return
		}

		com.MerchantID = m.ID
		com.Value = domains.ComValue(req.Value)
		_, err = s.commissionAction.CreateCommission(c, com)
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeDB, err.Error()))

			return
		}

		c.JSON(http.StatusOK, view(respMerchant{MerchantID: string(m.ID)}))
	}
}

// getMerchant godoc
// @Summary Get Merchant
// @Description Get Merchant
// @Router /api/v1/ecomm/merchants/by-id/{id} [GET]
// @Tags Merchants
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} R{data=merchant}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) getMerchant() gin.HandlerFunc {
	return func(c *gin.Context) {
		merchantID := c.Param("id")
		if merchantID == "" {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: "Ид не передан",
			})
			return
		}
		mer, err := s.merchantAction.GetMerchant(c, domains.Merchant{ID: domains.ID(merchantID)})
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeDB, err.Error()))
			return
		}
		c.JSON(http.StatusOK, view(
			toMerchantsView(c, mer)))
	}
}

type reqUpdateMerchant struct {
	ID       string   `json:"id" binding:"required"`
	Merchant Merchant `json:"merchant"`
	FeeType  int8     `json:"fee_type"`
	Value    float64  `json:"value"`
	ComType  int8     `json:"com_type"`
}

// / Add new merchant
// @Summary Update Existing Merchant
// @Description Existing Merchant
// @Router /api/v1/ecomm/merchants/merchant [PUT]
// @Tags Merchants
// @Accept json
// @Produce json
// @Param reqUpdateMerchant body reqUpdateMerchant true "ID|Merchant|FeeType|Value|ComType"
// @Success 200 {object} R{data=respMerchant}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) updateMerchant() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			req reqUpdateMerchant
			m   domains.Merchant
			com domains.Commission
			err error
		)
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})

			return
		}

		// 0-merchant,1-client,2-both
		switch req.FeeType {
		case int8(domains.FeeFromClient):
			com.FeeType = domains.FeeFromClient
		case int8(domains.FeeFromMerchant):
			com.FeeType = domains.FeeFromMerchant
		case int8(domains.FeeFromClientAndMerchant):
			com.FeeType = domains.FeeFromClientAndMerchant
		default:
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})

			return
		}

		// 0-%, 1-fixed
		switch req.ComType {
		case domains.TypePercent:
			com.ComType = domains.TypePercent
		case domains.TypeMoney:
			com.ComType = domains.TypeMoney
		default:
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})

			return
		}

		docCreateDate, err := tools.ParseDate(req.Merchant.DocCreateDate)
		if err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})

			return
		}

		m, err = s.merchantAction.UpdateMerchant(c, domains.Merchant{
			ID:             domains.ID(req.ID),
			ContractNumber: req.Merchant.ContractNumber,
			Inn:            req.Merchant.Inn,
			OrgName:        req.Merchant.MerchName,
			ShortName:      req.Merchant.ShortMerchName,
			DocCreatedAt:   *docCreateDate,
			//Mail:         "otabek94_30@mail.ru",
			//Address:      "minavarhoji",
			//Mobile:       "998999072754",
			//Tax:          "bor",
		})
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeDB, err.Error()))

			return
		}

		if req.Value < 0 {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeDB, err.Error()))

			return
		}

		com.MerchantID = m.ID
		com.Value = domains.ComValue(req.Value)
		_, err = s.commissionAction.UpdateCommission(c, com)
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeDB, err.Error()))

			return
		}

		c.JSON(http.StatusOK, view(respMerchant{MerchantID: string(m.ID)}))
	}
}
