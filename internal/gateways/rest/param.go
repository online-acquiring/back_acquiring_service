package rest

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type reqUpdateParam struct {
	ParamCode  string `json:"param_code"`
	ParamValue string `json:"param_value"`
}

// updateParamByCode godoc
// @Summary update param value by code
// @Description Update param value by code
// @Router /api/v1/ecomm/param/code [PUT]
// @Tags Params
// @Accept json
// @Produce json
// @Param reqUpdateParam body reqUpdateParam true "ParamCode|ParamValue"
// @Success 200 {object} R{data=respCommon}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) updateParamByCode() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Input process and Validation
		req := reqUpdateParam{}
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})
			return
		}

		// Handle request
		err := s.param.UpdateSystemParamByCode(c, domains.Param{Code: req.ParamCode, Value: req.ParamValue})
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeRemoteOther, err.Error()))
			return
		}

		c.JSON(http.StatusOK, view(
			respCommon{ResultMsg: "Успешно изменен"}))
	}
}

type reqUpdateParamByID struct {
	ParamID    string `json:"param_id"`
	ParamValue string `json:"param_value"`
}

// updateParamByID godoc
// @Summary update param value by id
// @Description Update param value by id
// @Router /api/v1/ecomm/param/id [PUT]
// @Tags Params
// @Accept json
// @Produce json
// @Param reqUpdateParamByID body reqUpdateParamByID true "ParamID|ParamValue"
// @Success 200 {object} R{data=respCommon}
// @Failure 422 {object} R
// @Failure 500 {object} R
//
//nolint:dupl
func (s *Server) updateParamByID() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Input process and Validation
		req := reqUpdateParamByID{}
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})
			return
		}

		// Handle request
		err := s.param.UpdateParamByID(c, domains.Param{ID: domains.ID(req.ParamID), Value: req.ParamValue})
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeRemoteOther, err.Error()))
			return
		}

		c.JSON(http.StatusOK, view(
			respCommon{ResultMsg: "Успешно изменен"}))
	}
}

// updateParamByID godoc
// @Summary get params of settings
// @Description Get params of settings
// @Router /api/v1/ecomm/params [GET]
// @Tags Params
// @Accept json
// @Produce json
// @Success 200 {object} R{data=[]params}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) getParams() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Handle request
		res, err := s.param.GetAllParams(c)
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeRemoteOther, err.Error()))
			return
		}

		c.JSON(http.StatusOK, view(
			toParamsView(res)))
	}
}

type params struct {
	ID        string `json:"id"`
	Code      string `json:"code"`
	Name      string `json:"name"`
	Value     string `json:"value"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

func toParamsView(s []domains.Param) []params {
	views := make([]params, len(s))
	for i, v := range s {
		views[i] = params{
			ID:        string(v.ID),
			Code:      v.Code,
			Name:      v.Name,
			Value:     v.Value,
			CreatedAt: tools.ParseTimeToViewString(v.CreatedAt),
			UpdatedAt: tools.ParseTimeToViewString(v.UpdatedAt),
		}
	}
	return views
}
