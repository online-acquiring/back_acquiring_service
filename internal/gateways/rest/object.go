package rest

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

type actProtocol struct {
	ObjectCode   string `json:"object_code"`
	ObjectID     string `json:"object_id"`
	StateCode    string `json:"state_code"`
	ActionCode   string `json:"action_code"`
	NewStateCode string `json:"new_state_code"`
	UserID       string `json:"user_id"`
	Message      string `json:"message"`
	CreatedAt    string `json:"created_at"`
}

// getProtocols godoc
// @Summary Get protocols
// @Description Get protocols
// @Router /api/v1/ecomm/objects [GET]
// @Tags Protocols
// @Accept json
// @Produce json
// @Success 200 {object} R{data=[]actProtocol}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) getProtocols() gin.HandlerFunc {
	return func(c *gin.Context) {
		actObjProtocols, err := s.actObject.GetActionProtocols(c)
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeRemoteOther, err.Error()))
			return
		}
		c.JSON(http.StatusOK, view(
			toProtocolView(actObjProtocols)))
	}
}

// getProtocolByID godoc
// @Summary get protocol
// @Description get protocol by payment ID
// @Router /api/v1/ecomm/objects/by-id/{payment_id} [GET]
// @Tags Protocols
// @Accept json
// @Produce json
// @Success 200 {object} R{data=[]actProtocol}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) getProtocolByID() gin.HandlerFunc {
	return func(c *gin.Context) {
		id := c.Param("payment_id")
		if id == "" {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: "Ид не передан",
			})
			return
		}
		actObjProtocols, err := s.actObject.GetActionProtocolByPayID(c, domains.Payment{
			ID: domains.ID(id),
		})
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeRemoteOther, err.Error()))
			return
		}
		c.JSON(http.StatusOK, view(
			toProtocolView(actObjProtocols)))
	}
}
func toProtocolView(obj []domains.ObjectActionProtocol) []actProtocol {
	object := make([]actProtocol, len(obj))
	for i := range obj {
		object[i] = textCases(obj[i])
	}
	return object
}

func textCases(obj domains.ObjectActionProtocol) actProtocol { //nolint
	par := actProtocol{
		ObjectID:  string(obj.ObjectID),
		UserID:    string(obj.UserID),
		Message:   obj.Message,
		CreatedAt: tools.ParseTimeToViewString(time.Time(obj.CreatedAt)),
	}
	switch obj.Object.Code { //nolint
	case domains.ObjectCodeMerchant:
		par.ObjectCode = domains.ObjectCodeMerchantText
	case domains.ObjectCodePayment:
		par.ObjectCode = domains.ObjectCodePaymentText
	default:
		par.ObjectCode = domains.ObjectCodeUserText
	}

	switch obj.State { //nolint
	case domains.PaymentStateCreated:
		par.StateCode = domains.PaymentStateCreatedText
	case domains.PaymentStateHolded:
		par.StateCode = domains.PaymentStateHoldedText
	case domains.PaymentStateConfirmed:
		par.StateCode = domains.PaymentStateConfirmedText
	case domains.PaymentStateCanceled:
		par.StateCode = domains.PaymentStateCanceledText
	case domains.PaymentStateReturned:
		par.StateCode = domains.PaymentStateReturnedText
	case domains.PaymentStateRejected:
		par.StateCode = domains.PaymentStateRejectedText
	case domains.PaymentStateInManual:
		par.StateCode = domains.PaymentStateInManualText
	case domains.PaymentStateInEditing:
		par.StateCode = domains.PaymentStateInEditingText
	case domains.PaymentStateInBalance:
		par.StateCode = domains.PaymentStateInBalanceText
	}

	switch obj.Action { //nolint
	case domains.PaymentActionCreate:
		par.ActionCode = domains.PaymentActionCreateText
	case domains.PaymentActionHold:
		par.ActionCode = domains.PaymentActionHoldText
	case domains.PaymentActionConfirm:
		par.ActionCode = domains.PaymentActionConfirmText
	case domains.PaymentActionCancel:
		par.ActionCode = domains.PaymentActionCancelText
	case domains.PaymentActionReturn:
		par.ActionCode = domains.PaymentActionReturnText
	case domains.PaymentActionError:
		par.ActionCode = domains.PaymentActionErrorText
	case domains.PaymentActionToEdit:
		par.ActionCode = domains.PaymentActionToEditText
	case domains.PaymentActionToManual:
		par.ActionCode = domains.PaymentActionToManualText
	case domains.PaymentActionToBalance:
		par.ActionCode = domains.PaymentActionToBalanceText
	}

	switch obj.NewState { //nolint
	case domains.PaymentStateCreated:
		par.NewStateCode = domains.PaymentStateCreatedText
	case domains.PaymentStateHolded:
		par.NewStateCode = domains.PaymentStateHoldedText
	case domains.PaymentStateConfirmed:
		par.NewStateCode = domains.PaymentStateConfirmedText
	case domains.PaymentStateCanceled:
		par.NewStateCode = domains.PaymentStateCanceledText
	case domains.PaymentStateReturned:
		par.NewStateCode = domains.PaymentStateReturnedText
	case domains.PaymentStateRejected:
		par.NewStateCode = domains.PaymentStateRejectedText
	case domains.PaymentStateInManual:
		par.NewStateCode = domains.PaymentStateInManualText
	case domains.PaymentStateInEditing:
		par.NewStateCode = domains.PaymentStateInEditingText
	case domains.PaymentStateInBalance:
		par.NewStateCode = domains.PaymentStateInBalanceText
	}
	return par
}
