package rest

import (
	"fmt"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/twinj/uuid"
)

type TokenDetails struct {
	AccessToken string
	AccessUUID  string
	AtExpires   int64
	UserID      string
	Role        string
	OrgName     string
}

const defaultLifeCycleMin = 40

func createToken(user domains.User) (*TokenDetails, error) {
	td := &TokenDetails{}
	td.AtExpires = time.Now().Add(time.Minute * defaultLifeCycleMin).Unix()
	td.AccessUUID = uuid.NewV4().String()
	var err error
	atClaims := jwt.MapClaims{}
	atClaims["authorized"] = true
	atClaims["access_uuid"] = td.AccessUUID
	atClaims["user_id"] = string(user.ID)
	atClaims["user_role"] = strconv.Itoa(int(user.Role))
	atClaims["exp"] = td.AtExpires

	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	td.AccessToken, err = at.SignedString([]byte(os.Getenv("ACCESS_SECRET")))
	if err != nil {
		return nil, err
	}
	return td, nil
}

func (s *Server) middleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		tk, err := ExtractTokenMetadata(c.Request)
		if err != nil {
			c.JSON(http.StatusUnauthorized, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: "Not authorized",
			})
			c.Abort()
			return
		}
		c.Set("user_id", tk.UserID)
		c.Set("user_role", tk.Role)
		c.Next()
	}
}

func ExtractTokenMetadata(r *http.Request) (*TokenDetails, error) {
	token, err := VerifyToken(r)
	if err != nil {
		return nil, err
	}
	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {
		accessUUID, ok := claims["access_uuid"].(string)
		if !ok {
			return nil, err
		}
		userID := claims["user_id"].(string)
		role := claims["user_role"].(string)

		return &TokenDetails{
			AccessUUID: accessUUID,
			UserID:     userID,
			Role:       role,
		}, nil
	}
	return nil, nil
}

func VerifyToken(r *http.Request) (*jwt.Token, error) {
	tokenString := ExtractToken(r)
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(os.Getenv("ACCESS_SECRET")), nil
	})
	if err != nil {
		return nil, err
	}
	return token, nil
}

const headerSize = 2

func ExtractToken(r *http.Request) string {
	bearToken := r.Header.Get("Authorization")
	strArr := strings.Split(bearToken, " ")
	if len(strArr) == headerSize {
		return strArr[1]
	}
	return ""
}

// logika qilaman
func (s *Server) checkRole(role domains.Role) gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			logRole interface{}
			ok      bool
		)
		if logRole, ok = c.Get("user_role"); ok {
			loginRole := logRole.(string)
			if (loginRole == strconv.Itoa(int(domains.MerchantRole)) ||
				loginRole == strconv.Itoa(int(domains.AdminRole)) ||
				loginRole == strconv.Itoa(int(domains.MerchantUserRole))) &&
				(role == domains.SuperAdminRole) {
				c.JSON(http.StatusUnauthorized, R{
					Status:    status.Failure,
					ErrorCode: status.ErrorCodeValidation,
					ErrorNote: logRole.(string) + " role is limited",
				})
				c.Abort()
				return
			}
		}
		c.Next()
	}
}
