package rest

import (
	"job/back_acquiring_service/internal/domains"
	"job/back_acquiring_service/internal/helpers/tools"
	"job/back_acquiring_service/internal/helpers/tools/validation"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

type reqLogin struct {
	Login    string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

type respLogin struct {
	Token string       `json:"token" binding:"required"`
	Role  domains.Role `json:"role" binding:"required"`
}

type respAuth struct {
	Login          string `json:"login"`
	ExpirationDate bool   `json:"expiration_date"`
}

const (
	addedBySuperAdmin = iota
)
const (
	DayActive = true
)

// Check user for login to system
// @Summary check user for login to system
// @Description Check user for login to system by username and password
// @Router /api/v1/ecomm/login [POST]
// @Tags Auth
// @Accept json
// @Produce json
// @Param reqLogin body reqLogin true "Login|Password"
// @Success 200 {object} R{data=respLogin}
// @Success 307 {object} R{data=respAuth}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) login() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Input process and Validate User
		req := reqLogin{}
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})

			return
		}

		// Handle login request
		res, err := s.userAction.GetUserByLoginAndPassword(c, domains.User{
			Login: req.Login,
			Credential: domains.Credential{
				Password: req.Password,
			},
		})
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeRemoteOther, err.Error()))

			return
		}

		if time.Now().After(res.Credential.ExpiredAt) {
			c.JSON(http.StatusTemporaryRedirect, view(respAuth{Login: res.Login, ExpirationDate: !DayActive}))

			return
		}

		if res.Credential.AddedBy == addedBySuperAdmin {
			c.JSON(http.StatusTemporaryRedirect, view(respAuth{Login: res.Login, ExpirationDate: DayActive}))

			return
		}

		// create a token
		td, err := createToken(res)
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeRemoteOther, err.Error()))

			return
		}

		c.JSON(http.StatusOK, view(
			respLogin{Token: td.AccessToken, Role: res.Role}))
	}
}

type reqAddUser struct {
	Name        string `json:"name" binding:"required"`
	Login       string `json:"login" binding:"required"`
	Password    string `json:"password" binding:"required"`
	Role        int8   `json:"role" binding:"required"`
	Email       string `json:"email"`
	PhoneNumber string `json:"phone_number"`
	MerchantID  string `json:"merchant_id"`
}

// Add new user
// @Summary add new user
// @Description Add new user
// @Router /api/v1/ecomm/user/add-user [POST]
// @Tags Users
// @Accept json
// @Produce json
// @Param reqAddUser body reqAddUser true "Name|Login|Password|Role|Email|MerchantID|PhoneNumber"
// @Success 200 {object} R{data=respCommon}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) addUser() gin.HandlerFunc {
	return func(c *gin.Context) {
		var req *reqAddUser
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})

			return
		}

		if req.Role > int8(domains.MerchantUserRole) || req.Role < int8(domains.SuperAdminRole) {
			c.JSON(http.StatusUnprocessableEntity, errView(status.ErrorCodeRemoteOther, errs.ErrInValidation.Error()))

			return
		}

		// Handle add request
		req.PhoneNumber = validation.PhoneValidation(req.PhoneNumber)
		var userReq = domains.User{
			Name:  req.Name,
			Login: req.Login,
			Credential: domains.Credential{
				Password: req.Password,
				CreateUpdateTime: domains.CreateUpdateTime{
					CreatedAt: time.Now(),
				},
			},
			Role:  domains.Role(req.Role),
			Email: req.Email,
			Merchant: domains.Merchant{
				ID: domains.ID(req.MerchantID),
			},
			PhoneNumber: req.PhoneNumber,
			CreateUpdateTime: domains.CreateUpdateTime{
				CreatedAt: time.Now(),
			},
		}
		_, err := s.userAction.CreateUser(c, userReq)
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeRemoteOther, err.Error()))

			return
		}

		c.JSON(http.StatusOK, view(
			respCommon{ResultMsg: "Успешно сохранено"}))
	}
}

type reqUpdateUserState struct {
	UserID string `json:"user_id" binding:"required"`
	State  int    `json:"state"`
}

type respCommon struct {
	ResultMsg string `json:"result_msg" binding:"required"`
}

// Update existed user state
// @Summary update existed user state
// @Description Update existed user state
// @Router /api/v1/ecomm/user/state [PUT]
// @Tags Users
// @Accept json
// @Produce json
// @Param reqUpdateUserState body reqUpdateUserState true "UserID|State"
// @Success 200 {object} R{data=respCommon}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) updateUserState() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Input process and Validation
		req := reqUpdateUserState{}
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})

			return
		}

		if req.State > int(domains.UserActionDeactivate) || req.State < int(domains.UserActionCreate) {
			c.JSON(http.StatusUnprocessableEntity,
				errView(status.ErrorCodeRemoteOther, errs.ErrInValidation.Error()))

			return
		}

		// Handle request
		err := s.userAction.UpdateUserStates(c, domains.User{
			ID:    domains.ID(req.UserID),
			State: int8(req.State),
		})
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeRemoteOther, err.Error()))

			return
		}

		c.JSON(http.StatusOK, view(
			respCommon{ResultMsg: "Успешно изменено"}))
	}
}

type reqUpdateUserByPass struct {
	Login       string `json:"username" binding:"required"`
	OldPassword string `json:"old_password" binding:"required"`
	NewPassword string `json:"new_password" binding:"required"`
}

// Update existed user password
// @Summary update existed user password
// @Description Update existed user password
// @Router /api/v1/ecomm/user/by-password [PUT]
// @Tags Users
// @Accept json
// @Produce json
// @Param reqUpdateUserByPass body reqUpdateUserByPass true "Login|OldPassword|NewPassword"
// @Success 200 {object} R{data=respCommon}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) updateUserByPassword() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Input process and Validation
		req := reqUpdateUserByPass{}
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})

			return
		}

		// Handle request
		if req.OldPassword == req.NewPassword {
			c.JSON(http.StatusUnprocessableEntity, errView(status.ErrorCodeValidation, errs.ErrPasswordExpired.Error()))

			return
		}

		err := s.userAction.UpdateUserPassByExistedPass(c, req.Login, req.OldPassword, req.NewPassword)
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeRemoteOther, err.Error()))

			return
		}

		c.JSON(http.StatusOK, view(
			respCommon{ResultMsg: "Успешно изменено"}))
	}
}

// / Get users
// @Summary get users
// @Description Get users
// @Router /api/v1/ecomm/users [GET]
// @Tags Users
// @Accept json
// @Produce json
// @Success 200 {object} R{data=[]user}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) getUsers() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Handle request
		res, err := s.userAction.GetAllUsers(c)
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeRemoteOther, err.Error()))
			return
		}

		c.JSON(http.StatusOK, view(
			toUsersView(res)))
	}
}

type user struct {
	ID          string `json:"id"  binding:"required"`
	Name        string `json:"name" binding:"required"`
	Login       string `json:"login" binding:"required"`
	Role        int8   `json:"role_code" binding:"required"`
	Email       string `json:"email"`
	MerchantID  string `json:"merchant_id"`
	OrgName     string `json:"org_name"`
	PhoneNumber string `json:"phone_number"`
	Status      string `json:"status"`
	CreatedAt   string `json:"created_at"`
}

//nolint:gocritic
func toUsersView(s []domains.User) []user {
	views := make([]user, len(s))
	for i, v := range s {
		m := user{
			ID:          string(v.ID),
			Name:        v.Name,
			Login:       v.Login,
			Role:        int8(v.Role),
			Email:       v.Email,
			MerchantID:  string(v.Merchant.ID),
			OrgName:     v.Merchant.OrgName,
			PhoneNumber: v.PhoneNumber,
			Status:      getUserStateView(domains.Action(v.State)),
			CreatedAt:   tools.ParseDateToViewString(v.CreateUpdateTime.CreatedAt),
		}
		views[i] = m
	}
	return views
}

const (
	Created  = "Создан"
	Active   = "Активный"
	Passive  = "Пассивный" //nolint:gosec
	NoStatus = "Неизвестный статус"
)

//nolint:exhaustive
func getUserStateView(state domains.Action) string {
	var sv string
	switch state {
	case domains.UserActionCreate:
		sv = Created
	case domains.UserActionActivate:
		sv = Active
	case domains.UserActionDeactivate:
		sv = Passive
	default:
		sv = NoStatus
	}
	return sv
}

type reqUpdateUserPass struct {
	UserID   string `json:"user_id" binding:"required"`
	Password string `json:"password" binding:"required"`
}

// / Update user password
// @Summary update user password
// @Description Update user password
// @Router /api/v1/ecomm/user/pass [PUT]
// @Tags Users
// @Accept json
// @Produce json
// @Param reqUpdateUserState body reqUpdateUserState true "UserID|Password"
// @Success 200 {object} R{data=respCommon}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) updateUserPass() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Input process and Validation
		req := reqUpdateUserPass{}
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})

			return
		}

		// Handle request
		err := s.userAction.UpdateUserByUserID(c, domains.User{
			ID: domains.ID(req.UserID),
			Credential: domains.Credential{
				Password: req.Password,
			},
		})
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeRemoteOther, err.Error()))

			return
		}

		c.JSON(http.StatusOK, view(
			respCommon{ResultMsg: "Успешно изменено"}))
	}
}

type reqUpdateUserByLogin struct {
	Login       string `json:"login" binding:"required"`
	NewPassword string `json:"new_password" binding:"required"`
}

// / Update user password by login
// @Summary update user password by login
// @Description Update user password by login
// @Router /api/v1/ecomm/user/by-login [PUT]
// @Tags Users
// @Accept json
// @Produce json
// @Param reqUpdateUserByLogin body reqUpdateUserByLogin true "Login|Password"
// @Success 200 {object} R{data=respCommon}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) updateUserPassByLogin() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Input process and Validation
		req := reqUpdateUserByLogin{}
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})

			return
		}

		// Handle request
		err := s.userAction.UpdateUserPasswordByLogin(c, domains.User{
			Login: req.Login,
			Credential: domains.Credential{
				Password: req.NewPassword,
			},
		})
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeRemoteOther, err.Error()))
			return
		}

		c.JSON(http.StatusOK, view(
			respCommon{ResultMsg: "Успешно изменено"}))
	}
}
