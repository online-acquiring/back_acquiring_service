package rest

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
)

type account struct {
	ID        string `json:"id"`
	Code      string `json:"code"` // account number
	Name      string `json:"name"`
	Tax       string `json:"tax"`
	AccType   int8   `json:"acc_type"`
	MFO       string `json:"mfo"`
	Inn       string `json:"inn"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

func toAccountView(m []domains.Account) []account {
	views := make([]account, len(m))
	for i := range m {
		d := m[i]
		views[i] = account{
			ID:        string(d.ID),
			Code:      d.Code,
			Name:      d.Name,
			Tax:       d.Tax,
			AccType:   int8(d.AccType),
			MFO:       d.MFO,
			Inn:       d.Inn,
			CreatedAt: tools.ParseDateToViewString(m[i].Dates.CreatedAt),
			UpdatedAt: tools.ParseDateToViewString(m[i].Dates.UpdatedAt),
		}
	}
	return views
}

// / Get All accounts
// @Summary Get All accounts
// @Description Get All accounts
// @Router /api/v1/ecomm/accounts [GET]
// @Tags Accounts
// @Accept json
// @Produce json
// @Success 200 {object} R{data=[]account}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) getAccounts() gin.HandlerFunc {
	return func(c *gin.Context) {
		acc, err := s.accountAction.GetAccounts(c)
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeDB, err.Error()))
			return
		}
		c.JSON(http.StatusOK, view(
			toAccountView(acc)))
	}
}

// getAccount godoc
// @Summary Get account
// @Description Get account
// @Router /api/v1/ecomm/accounts/by-id/{id} [GET]
// @Tags Accounts
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} R{data=account}
// @Failure 422 {object} R
// @Failure 500 {object} R
//
//nolint:dupl
func (s *Server) getAccount() gin.HandlerFunc {
	return func(c *gin.Context) {
		accountID := c.Param("id")
		if accountID == "" {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: "Ид не передан",
			})
			return
		}
		acc, err := s.accountAction.GetAccount(c, domains.Account{ID: domains.ID(accountID)})
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeDB, err.Error()))
			return
		}
		c.JSON(http.StatusOK, view(
			toAccountView([]domains.Account{acc})))
	}
}

type reqAccount struct {
	MerchantID  string   `json:"merchant_id"`
	TerminalIDs []string `json:"terminal_ids"`
	Acc         Account  `json:"account"`
}
type Account struct {
	AccType int8   `json:"acc_type"`
	MFO     string `json:"mfo"`
	Name    string `json:"name"`
	AccNum  string `json:"acc_num"`
	Inn     string `json:"inn"`
	Tax     string `json:"tax"`
}

const (
	Main = iota
	Other
)

// / Add new account
// @Summary Add New Account
// @Description Add New Account
// @Router /api/v1/ecomm/accounts/createAccount [POST]
// @Tags Accounts
// @Accept json
// @Produce json
// @Param reqAccount body reqAccount true "MerchantID|TerminalID|Acc"
// @Success 200 {object} R{data=respCommon}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) addAccount() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			req     reqAccount
			accType domains.AccType
		)
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})
			return
		}
		switch req.Acc.AccType {
		case int8(Main):
			accType = Main
		case int8(Other):
			accType = Other
		default:
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: errors.New("invalid type account").Error(),
			})
			return
		}
		acc, err := s.accountAction.CreateAccount(c, domains.Account{
			Code:    req.Acc.AccNum,
			Name:    req.Acc.Name,
			AccType: accType, // aniqlik kiritaman
			MFO:     req.Acc.MFO,
			Inn:     req.Acc.Inn,
			Tax:     req.Acc.Tax,
		})

		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeDB, err.Error()))
			return
		}
		//nolint:gocritic
		//for _, ter := range req.TerminalIDs {
		//	_, err = s.terminalAction.CreateTerAcc(c, domains.TerminalAccount{
		//		AccountType: acc.AccType,
		//		Terminal: domains.Terminal{
		//			ID: domains.ID(ter),
		//		},
		//		Account: domains.Account{
		//			ID: acc.ID,
		//		},
		//	})
		//	if err != nil {
		//		c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeDB, err.Error()))
		//		return
		//	}
		//}

		_, err = s.merchantAction.CreateMerchAcc(c, domains.MerchantAccount{
			Merchant: domains.Merchant{
				ID: domains.ID(req.MerchantID),
			},
			Account: domains.Account{
				ID: acc.ID,
			},
		})
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeDB, err.Error()))
			return
		}
		c.JSON(http.StatusOK, view(respCommon{ResultMsg: "Успешно создан"}))
	}
}

type reqUpdateAccount struct {
	ID         string  `json:"id" binding:"required"`
	MerchantID string  `json:"merchant_id"`
	TerminalID string  `json:"terminal_id"`
	Acc        Account `json:"account"`
}

// / Add Update account
// @Summary Add Update Account
// @Description Add Update Account
// @Router /api/v1/ecomm/accounts/account [PUT]
// @Tags Accounts
// @Accept json
// @Produce json
// @Param reqUpdateAccount body reqUpdateAccount true "ID|MerchantID|TerminalID|Acc"
// @Success 200 {object} R{data=respCommon}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) updateAccount() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			req     reqUpdateAccount
			accType domains.AccType
		)
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})
			return
		}
		switch req.Acc.AccType {
		case int8(Main):
			accType = Main
		case int8(Other):
			accType = Other
		default:
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: errors.New("invalid type account").Error(),
			})
			return
		}
		_, err := s.accountAction.UpdateAccount(c, domains.Account{
			ID:      domains.ID(req.ID),
			Code:    req.Acc.AccNum,
			Name:    req.Acc.Name,
			AccType: accType, // aniqlik kiritaman
			MFO:     req.Acc.MFO,
			Inn:     req.Acc.Inn,
			Tax:     req.Acc.Tax,
		})

		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeDB, err.Error()))

			return
		}

		c.JSON(http.StatusOK, view(respCommon{ResultMsg: "Успешно изменён"}))
	}
}
