package rest

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

type reqGetPayments struct {
	BeginTime string `json:"begin_time"`
	EndTime   string `json:"end_time"`
}

const (
	defaultHour = 24
	defaultSum  = 100
)

// getPayment godoc
// @Summary Get Payment
// @Description Get Payment
// @Router /api/v1/ecomm/payments/by-id/{id} [GET]
// @Tags Payments
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} R{data=payment}
// @Failure 422 {object} R
// @Failure 500 {object} R
//
//nolint:dupl
func (s *Server) getPayment() gin.HandlerFunc {
	return func(c *gin.Context) {
		paymentID := c.Param("id")
		if paymentID == "" {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: "Ид не передан",
			})
			return
		}
		pay, err := s.paymentAction.GetPaymentByID(c, domains.Payment{
			ID: domains.ID(paymentID),
		})
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeDB, err.Error()))
			return
		}
		c.JSON(http.StatusOK, view(
			toPaymentView([]domains.Payment{pay})))
	}
}

// getPayments godoc
// @Summary Get All Payments
// @Description Get All Payments
// @Router /api/v1/ecomm/payments [POST]
// @Tags Payments
// @Accept json
// @Produce json
// @Param reqGetPayments body reqGetPayments true "BeginTime|EndTime"
// @Success 200 {object} R{data=[]payment}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) getPayments() gin.HandlerFunc {
	return func(c *gin.Context) {
		req := reqGetPayments{}
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})
			return
		}
		var endTime, beginTime *time.Time

		if req.BeginTime == "" {
			t := time.Now()
			beginTime = &t
		} else {
			t, err := tools.ParseDate(req.BeginTime)
			if err != nil {
				c.JSON(http.StatusUnprocessableEntity, R{
					Status:    status.Failure,
					ErrorCode: status.ErrorCodeValidation,
					ErrorNote: err.Error(),
				})
				return
			}
			beginTime = t
		}
		if req.EndTime == "" {
			t := time.Now()
			endTime = &t
		} else {
			t, err := tools.ParseDate(req.EndTime)
			if err != nil {
				c.JSON(http.StatusUnprocessableEntity, R{
					Status:    status.Failure,
					ErrorCode: status.ErrorCodeValidation,
					ErrorNote: err.Error(),
				})
				return
			}
			endTime = t
		}
		if beginTime.Equal(*endTime) {
			et := endTime.Add(time.Hour * defaultHour)
			endTime = &et
		}
		// Handle request
		res, err := s.paymentAction.GetAllByPeriod(c, *beginTime, *endTime)
		s.log.Info(fmt.Sprintf("date:%v-%v-RES-%#v", beginTime, endTime, res))
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeRemoteOther, err.Error()))
			return
		}
		c.JSON(http.StatusOK, view(
			toPaymentView(res)))
	}
}

type payment struct {
	ID             string   `json:"id"`
	ParentID       string   `json:"parent_id"`
	MerchantID     string   `json:"merchant_id"`
	PaymentType    int8     `json:"payment_type"`
	CardID         string   `json:"card_id"`
	CardNumber     string   `json:"card_number"`
	CardExpire     string   `json:"card_expire"`
	PSCode         string   `json:"ps_code"`
	CurrencyCode   string   `json:"currency_code"`
	Amount         string   `json:"amount"`
	FeeAmount      int64    `json:"fee_amount"`
	ExternalID     string   `json:"external_id"`
	RRN            string   `json:"rrn"`
	ProcessingID   string   `json:"processing_id"`
	DocumentID     string   `json:"document_id"`
	State          int8     `json:"state"`
	CreatedAt      string   `json:"created_at"`
	UpdatedAt      string   `json:"updated_at"`
	ErrorCode      string   `json:"error_code"`
	ErrorMessage   string   `json:"error_message"`
	RefNum         string   `json:"ref_num"`
	ReversalAmount string   `json:"reversal_amount"`
	Merchant       merchant `json:"merchant"`
}

func toPaymentView(p []domains.Payment) []payment {
	views := make([]payment, len(p))
	for i := range p {
		sum := validation.GetSumView(float64(p[i].Amount / defaultSum))
		switch p[i].Amount % defaultSum {
		case 0:
			sum += ".00"
		default:
			sum += "." + strconv.Itoa(int(p[i].Amount%defaultSum))
		}
		views[i] = payment{
			ID:           string(p[i].ID),
			ParentID:     string(p[i].ParentID),
			PaymentType:  int8(p[i].PaymentType),
			CardID:       p[i].Card.ID,
			CardNumber:   p[i].Card.MaskedPAN,
			CardExpire:   p[i].Card.Expiry,
			PSCode:       p[i].PsCode,
			CurrencyCode: p[i].CurrencyCode,
			Amount:       sum,
			FeeAmount:    int64(p[i].FeeAmount),
			ExternalID:   p[i].ExternalID,
			RRN:          p[i].RRN,
			ProcessingID: p[i].ProcessingID,
			DocumentID:   p[i].DocumentID,
			State:        int8(p[i].State),
			CreatedAt:    tools.ParseDateToViewString(p[i].CreatedAt),
			UpdatedAt:    tools.ParseDateToViewString(p[i].UpdatedAt),
			ErrorCode:    p[i].ErrorCode,
			ErrorMessage: p[i].ErrorMessage,
			RefNum:       p[i].RefNum,
			Merchant: merchant{
				ID:           string(p[i].Merchant.ID),
				MerchOrgName: p[i].Merchant.OrgName,
			},
			ReversalAmount: validation.GetSumView(float64(p[i].ReversalAmount)),
		}
	}
	return views
}

//nolint:deadcode,unused
type reqPaymentChangeState struct {
	ID    string `json:"id"`
	State int8   `json:"state"`
}

// changePaymentState godoc
// @Summary Change Payment State
// @Description Change Payment State
// @Router /api/v1/ecomm/payments/state [PUT]
// @Tags Payments
// @Accept json
// @Produce json
// @Param reqPaymentChangeState body reqPaymentChangeState true "ID|State"
// @Success 200 {object} R{data=respCommon}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) changePaymentState() gin.HandlerFunc {
	return func(c *gin.Context) {

	}
}

type reqAddCard struct {
	PaymentID  string `json:"payment_id" binding:"required"`
	CardNumber string `json:"card_number" binding:"required"`
	CardExpire string `json:"card_expire"  binding:"required"`
}

// TODO smsli deb qolsa degan maqsadda bu qolib turibdi
//
//nolint:deadcode,unused
type respAddCard struct {
	OperationID string `json:"operation_id" binding:"required"`
}

//nolint:deadcode,unused
type respAddCardConfirm struct {
	OperationID string `json:"operation_id" binding:"required"`
	ConfirmCode string `json:"confirm_code" binding:"required"`
}

// addCard godoc
// @Summary Add New Card
// @Description Add New Card
// @Router /api/v1/ecomm/payments/addCard [POST]
// @Tags Payments
// @Accept json
// @Produce json
// @Param reqAddCard body reqAddCard true "PaymentID|CardNumber|CardExpire"
// @Success 200 {object} R{data=respCommon}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) addCard() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			req  reqAddCard
			card *domains.Card
		)
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: errs.ErrInValidation.Error(),
			})
			return
		}
		// TODO karta qachon tugashini aniqlik kiritib, mantiqni moslash kerak
		req.CardExpire = helpers.FixExpiry(req.CardExpire)
		if !validation.ValidateDate(req.CardExpire) {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: errs.ErrWithCardExpireDate.Error(),
			})
			return
		}
		req.CardNumber = strings.ReplaceAll(req.CardNumber, " ", "")
		customErr := s.renewCard.RenewCard(c, req.PaymentID, req.CardNumber, req.CardExpire, card)
		if customErr.Err != nil {
			c.JSON(http.StatusInternalServerError, errView(customErr.Code, customErr.Err.Error()))
			return
		}
		c.JSON(http.StatusOK, view(
			respCommon{ResultMsg: "Успешно изменен"}))
	}
}

func (s *Server) confirmCard() gin.HandlerFunc {
	return func(c *gin.Context) {

	}
}

type reqRefund struct {
	PaymentID string `json:"payment_id" binding:"required"`
	Amount    int    `json:"amount"`
}

// refundPayment godoc
// @Summary Refund Paid Money
// @Description Refund Paid Money
// @Router /api/v1/ecomm/payments/refund [POST]
// @Tags Payments
// @Accept json
// @Produce json
// @Param reqRefund body reqRefund true "PaymentID|Amount"
// @Success 200 {object} R{data=respCommon}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) refundPayment() gin.HandlerFunc {
	return func(c *gin.Context) {
		req := reqRefund{}
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: errs.ErrInValidation.Error(),
			})
			return
		}
		// Detailni ichida ham keladi (bilet raqam yoki maxsulot IDsi) shuni hisobga olib ketishim kerak
		if errCustom := s.refundAction.RefundPayment(c, domains.ID(req.PaymentID), domains.Money(req.Amount), nil); errCustom.Err != nil {
			c.JSON(http.StatusInternalServerError, errView(errCustom.Code, errCustom.Err.Error()))
			return
		}
		c.JSON(http.StatusOK, view(
			respCommon{ResultMsg: "Успешно возвращён"}))
	}
}

// getPaymentDetails godoc
// @Summary Get Payment details
// @Description Get Payment Details
// @Router /api/v1/ecomm/payments/details/{id} [GET]
// @Tags Payments
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} R{data=payment}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) getPaymentDetails() gin.HandlerFunc {
	return func(c *gin.Context) {
		paymentID := c.Param("id")
		if paymentID == "" {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: "Ид не передан",
			})
			return
		}

		pDts, pData, err := s.paymentAction.GetPayDetails(c, paymentID)
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeDB, err.Error()))
			return
		}

		var pay domains.Payment
		pay.Details = append(pay.Details, pDts...)
		pay.PayerData = pData

		c.JSON(http.StatusOK, view(pay))
	}
}

// getAllPayments godoc
// @Summary Get All Payments
// @Description Get All Payments
// @Router /api/v1/ecomm/payments/get-all [GET]
// @Tags Payments
// @Accept json
// @Produce json
// @Success 200 {object} R{data=[]payment}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) getAllPayments() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Handle request
		res, err := s.paymentAction.GetAll(c)
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeRemoteOther, err.Error()))
			return
		}
		c.JSON(http.StatusOK, view(
			respPayments{Payments: toPaymentView(res), Length: len(res)}))
	}
}

type respPayments struct {
	Payments []payment `json:"payments"`
	Length   int       `json:"length"`
}

type reqFilterBody struct {
	PaymentID     string `json:"payment_id"`
	ExternalID    string `json:"external_id"`
	CreatedAtFrom string `json:"created_at_from"`
	CreatedAtTo   string `json:"created_at_to"`
	UpdatedAtFrom string `json:"updated_at_from"`
	UpdatedAtTo   string `json:"updated_at_to"`
	SumFrom       int    `json:"sum_from"`
	SumTo         int    `json:"sum_to"`
	OrgName       string `json:"org_name"`
	PaymentState  int    `json:"payment_state"`
	CardNumber    string `json:"card_number"`
}

// godoc filterPayments
// @Summary filter specific payment
// @Description Filter Specific payment
// @Router /api/v1/payments/payment/filter [POST]
// @Tags Payments
// @Accept json
// @Produce json
// @Param reqFilterBody body reqFilterBody true "The element of reqFilterBody"
// @Success 200 {object} R{data=[]payment}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) filterPayments() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			req = reqFilterBody{}
			p   domains.PaySearchParams
		)
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: errs.ErrInValidation.Error(),
			})
			return
		}
		if req.CreatedAtFrom != "" {
			t, err := tools.ParseDate(req.CreatedAtFrom)
			if err != nil {
				c.JSON(http.StatusUnprocessableEntity, R{
					Status:    status.Failure,
					ErrorCode: status.ErrorCodeValidation,
					ErrorNote: err.Error(),
				})
				return
			}
			p.CreatedAtFrom = t.Format(tools.DateStringTemplate)
			if req.CreatedAtTo != "" {
				t, err = tools.ParseDate(req.CreatedAtTo)
				if err != nil {
					c.JSON(http.StatusUnprocessableEntity, R{
						Status:    status.Failure,
						ErrorCode: status.ErrorCodeValidation,
						ErrorNote: err.Error(),
					})
					return
				}
				p.CreatedAtTo = t.Format(tools.DateStringTemplate)
			}
		}
		if req.UpdatedAtFrom != "" {
			t, err := tools.ParseDate(req.UpdatedAtFrom)
			if err != nil {
				c.JSON(http.StatusUnprocessableEntity, R{
					Status:    status.Failure,
					ErrorCode: status.ErrorCodeValidation,
					ErrorNote: err.Error(),
				})
				return
			}
			p.UpdatedAtFrom = t.Format(tools.DateStringTemplate)
			if req.UpdatedAtTo != "" {
				t, err = tools.ParseDate(req.UpdatedAtTo)
				if err != nil {
					c.JSON(http.StatusUnprocessableEntity, R{
						Status:    status.Failure,
						ErrorCode: status.ErrorCodeValidation,
						ErrorNote: err.Error(),
					})
					return
				}
				p.UpdatedAtTo = t.Format(tools.DateStringTemplate)
			}
		}
		if req.SumFrom < 0 || req.SumTo < 0 {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: errors.New("less than zero is invalid").Error(),
			})
			return
		}
		if req.PaymentState < 0 {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: errors.New("less than zero is invalid").Error(),
			})
			return
		}
		p.PaymentID = req.PaymentID
		p.ExternalID = req.ExternalID
		p.SumFrom = req.SumFrom
		p.SumTo = req.SumTo
		p.OrgName = req.OrgName
		p.CardNumber = req.CardNumber
		p.PaymentState = req.PaymentState
		if p.IsEmpty() {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: errors.New("empty payment data").Error(),
			})
			return
		}
		pay, err := s.paymentAction.FilterPayments(c, p)
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeRemoteOther, err.Error()))
			return
		}
		c.JSON(http.StatusOK, toPaymentView(pay))
	}
}

type reqDetailBody struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

// godoc filterDetails
// @Summary filter specific payment detail
// @Description Filter Specific payment detail
// @Router /api/v1/payments/detail/filter [POST]
// @Tags Payments
// @Accept json
// @Produce json
// @Param reqDetailBody body reqDetailBody true "Key|Value"
// @Success 200 {object} R{data=[]payment}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) filterDetails() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			req = reqDetailBody{}
		)
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: errs.ErrInValidation.Error(),
			})
			return
		}
		if req.Key == "" && req.Value == "" {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: errs.ErrInValidation.Error(),
			})
			return
		}
		pay, err := s.paymentAction.FilterDetails(c, domains.Detail{
			Field: req.Key,
			Value: req.Value,
		})
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeRemoteOther, err.Error()))
			return
		}
		c.JSON(http.StatusOK, toPaymentView(pay))
	}
}
