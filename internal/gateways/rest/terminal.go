package rest

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
)

type reqTerminal struct {
	Terminals  []Terminal `json:"terminals"`
	MerchantID string     `json:"merchant_id"`
	// CardType     string   `json:"card_type"`
}

type Terminal struct {
	TerminalNum  string   `json:"terminal_num"`
	MerchantNum  string   `json:"merchant_num"`
	TerminalName string   `json:"terminal_name"`
	Port         int      `json:"port"`
	Com          float64  `json:"com,omitempty"`
	TerminalAcc  []string `json:"terminal_acc"`
	TerminalType int8     `json:"terminal_type"`
}

type respTerminal struct {
	MerchantID  string   `json:"merchant_id"`
	TerminalIDs []string `json:"terminal_ids"`
}

const (
	UZCARD = iota
	HUMO
)

// / Add new Terminal
// @Summary Add New Terminal
// @Description Add New Terminal
// @Router /api/v1/ecomm/terminals/createTerminal [POST]
// @Tags Terminals
// @Accept json
// @Produce json
// @Param reqTerminal body reqTerminal true "MerchantID|[]TerminalIDs"
// @Success 200 {object} R{data=respTerminal}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) addTerminal() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			req         reqTerminal
			terminalIDs []string
			cardType    domains.PSCode
		)
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})
			return
		}
		for _, ter := range req.Terminals {
			switch ter.TerminalType {
			case UZCARD:
				cardType = domains.PSCodeUZCARD
			case HUMO:
				cardType = domains.PSCodeHUMO
			default:
				c.JSON(http.StatusUnprocessableEntity, R{
					Status:    status.Failure,
					ErrorCode: status.ErrorCodeValidation,
					ErrorNote: errors.New("card type is wrong").Error(),
				})
				return
			}
			if ter.Port < 0 || (ter.Com < 0 || ter.Com > 100) {
				c.JSON(http.StatusUnprocessableEntity, R{
					Status:    status.Failure,
					ErrorCode: status.ErrorCodeValidation,
					ErrorNote: errors.New("invalid port or commission").Error(),
				})
				return
			}
			// Add Terminal
			t, err := s.terminalAction.CreateTerminal(c, domains.Terminal{
				Name:        ter.TerminalName,
				TerminalNum: ter.TerminalNum,
				MerchantNum: ter.MerchantNum,
				Port:        ter.Port,
				PSCode:      cardType,
				TerminalAcc: ter.TerminalAcc,
				Com:         ter.Com,
			})
			if err != nil {
				c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeDB, err.Error()))
				return
			}
			// Connect Merchant To Terminal
			_, err = s.merchantAction.CreateMerchTer(c, domains.MerchantTerminal{
				Merchant: domains.Merchant{
					ID: domains.ID(req.MerchantID),
				},
				Terminal: domains.Terminal{
					ID: t.ID,
				},
			})
			if err != nil {
				c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeDB, err.Error()))
				return
			}
			terminalIDs = append(terminalIDs, string(t.ID))
		}
		c.JSON(http.StatusOK, view(respTerminal{
			MerchantID:  req.MerchantID,
			TerminalIDs: terminalIDs,
		}))
	}
}

// / Get Terminals
// @Summary Get Terminals
// @Description Get Terminals
// @Router /api/v1/ecomm/terminals [GET]
// @Tags Terminals
// @Accept json
// @Produce json
// @Success 200 {object} R{data=[]terminal}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) getTerminals() gin.HandlerFunc {
	return func(c *gin.Context) {
		ter, err := s.terminalAction.GetAllTerminals(c)
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeDB, err.Error()))
			return
		}
		c.JSON(http.StatusOK, view(
			toTerminalView(ter)))
	}
}

// getTerminal godoc
// @Summary Get Terminal
// @Description Get Terminal
// @Router /api/v1/ecomm/terminals/by-id/{id} [GET]
// @Tags Terminals
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} R{data=terminal}
// @Failure 422 {object} R
// @Failure 500 {object} R
//
//nolint:dupl
func (s *Server) getTerminal() gin.HandlerFunc {
	return func(c *gin.Context) {
		terminalID := c.Param("id")
		if terminalID == "" {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: "Ид не передан",
			})
			return
		}
		ter, err := s.terminalAction.GetTerminal(c, domains.Terminal{ID: domains.ID(terminalID)})
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeDB, err.Error()))
			return
		}
		c.JSON(http.StatusOK, view(
			toTerminalView([]domains.Terminal{ter})))
	}
}

type terminal struct {
	ID           string   `json:"id"`
	MerchantID   string   `json:"merchant_id"`
	Name         string   `json:"name"`
	TerminalNum  string   `json:"terminal_num"`
	MerchantNum  string   `json:"merchant_num"`
	Port         int      `json:"port"`
	TerminalAcc  []string `json:"terminal_acc"`
	TerminalType int8     `json:"terminal_type"`
	Com          float64  `json:"com"`
}

const (
	Uzcard = iota // Uzcard
	Humo          // Humo
)

func toTerminalView(m []domains.Terminal) []terminal {
	views := make([]terminal, len(m))
	var cardType int8
	for i := range m {
		switch m[i].PSCode { //nolint:exhaustive
		case domains.PSCodeUZCARD:
			cardType = Uzcard
		default:
			cardType = Humo
		}
		views[i] = terminal{
			ID:           string(m[i].ID),
			MerchantID:   m[i].MerchantID,
			Name:         m[i].Name,
			TerminalNum:  m[i].TerminalNum,
			MerchantNum:  m[i].MerchantNum,
			Port:         m[i].Port,
			TerminalType: cardType,
			Com:          m[i].Com,
		}
		terAccounts := make([]string, len(m[i].TerminalAcc))
		copy(terAccounts, m[i].TerminalAcc)
		views[i].TerminalAcc = terAccounts
	}
	return views
}

type reqUpdateTerminal struct {
	MerchantID string `json:"merchant_id"`
	Terminals  []struct {
		ID           string   `json:"id" binding:"required"`
		TerminalNum  string   `json:"terminal_num"`
		MerchantNum  string   `json:"merchant_num"`
		TerminalName string   `json:"terminal_name"`
		Port         int      `json:"port"`
		Com          float64  `json:"com,omitempty"`
		TerminalAcc  []string `json:"terminal_acc"`
		TerminalType int8     `json:"terminal_type"`
	} `json:"terminals"`
}

// / Update new Terminal
// @Summary Add Update Terminal
// @Description Add Update Terminal
// @Router /api/v1/ecomm/terminals/terminal [PUT]
// @Tags Terminals
// @Accept json
// @Produce json
// @Param reqUpdateTerminal body reqUpdateTerminal true "ID|MerchantID|CardType|TerminalNum|MerchantNum|TerminalName|Port|Com"
// @Success 200 {object} R{data=respTerminal}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) updateTerminal() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			reqTer      reqUpdateTerminal
			cardType    domains.PSCode
			terminalIds []string
		)
		if err := c.ShouldBindJSON(&reqTer); err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})
			return
		}
		for _, req := range reqTer.Terminals {
			switch req.TerminalType {
			case UZCARD:
				cardType = domains.PSCodeUZCARD
			case HUMO:
				cardType = domains.PSCodeHUMO
			default:
				c.JSON(http.StatusUnprocessableEntity, R{
					Status:    status.Failure,
					ErrorCode: status.ErrorCodeValidation,
					ErrorNote: errors.New("card type is wrong").Error(),
				})
				return
			}
			if req.Port < 0 || (req.Com < 0 || req.Com > 100) {
				c.JSON(http.StatusUnprocessableEntity, R{
					Status:    status.Failure,
					ErrorCode: status.ErrorCodeValidation,
					ErrorNote: errors.New("invalid port or commission").Error(),
				})
				return
			}
			// Update Terminal
			t, err := s.terminalAction.UpdateTerminal(c, domains.Terminal{
				ID:          domains.ID(req.ID),
				Name:        req.TerminalName,
				TerminalNum: req.TerminalNum,
				MerchantNum: req.MerchantNum,
				Port:        req.Port,
				PSCode:      cardType,
				TerminalAcc: req.TerminalAcc,
				Com:         req.Com,
			})
			if err != nil {
				c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeDB, err.Error()))
				return
			}
			terminalIds = append(terminalIds, string(t.ID))
			// Connect Merchant To Terminal
			// _, err = s.merchantAction.UpdateMerchTer(c, domains.MerchantTerminal{
			//	Merchant: domains.Merchant{
			//		ID: domains.ID(req.MerchantID),
			//	},
			//	Terminal: domains.Terminal{
			//		ID: t.ID,
			//	},
			//	//State:
			// } )
			// if err != nil {
			//	c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeDB, err.Error()))
			//	return
			// }
		}
		c.JSON(http.StatusOK, view(respTerminal{
			MerchantID:  reqTer.MerchantID,
			TerminalIDs: terminalIds,
		}))
	}
}
