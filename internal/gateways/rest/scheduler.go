package rest

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// schedulers
const (
	Error = "Ошибка"
)

// / Get schedulers of settings
// @Summary get schedulers of settings
// @Description Get schedulers of settings
// @Router /api/v1/ecomm/schedulers [GET]
// @Tags Schedulers
// @Accept json
// @Produce json
// @Success 200 {object} R{data=[]scheduler}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) getSchedulers() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Handle request
		res, err := s.scheduler.GetAllSchedulers(c)
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeRemoteOther, err.Error()))
			return
		}
		c.JSON(http.StatusOK, view(
			toSchedulersView(res)))
	}
}

type scheduler struct {
	ID         string `json:"id"`
	Code       string `json:"code"`
	Name       string `json:"name"`
	Expression string `json:"expression"`
	ExecFunc   string `json:"exec_func"`
	Status     string `json:"status"`
	JobID      int    `json:"job_id"`
	CreatedAt  string `json:"created_at"`
	UpdatedAt  string `json:"updated_at"`
}

func toSchedulersView(s []domains.Scheduler) []scheduler {
	views := make([]scheduler, len(s))
	for i := 0; i < len(s); i++ {
		v := s[i]
		views[i] = scheduler{
			ID:         string(v.ID),
			Code:       v.Code,
			Name:       v.Name,
			Expression: v.Expression,
			//ExecFunc:   v.ExecFunc,
			Status:    getJobStateView(v.Status),
			JobID:     v.JobID,
			CreatedAt: tools.ParseTimeToViewString(v.CreatedAt),
			UpdatedAt: tools.ParseTimeToViewString(v.UpdatedAt),
		}
	}
	return views
}

type reqUpdateScheduler struct {
	ID         string `json:"id"`
	Expression string `json:"expression"`
}

// / Update scheduler expression
// @Summary update scheduler expression
// @Description Update scheduler expression
// @Router /api/v1/ecomm/scheduler [PUT]
// @Tags Schedulers
// @Accept json
// @Produce json
// @Param reqUpdateScheduler body reqUpdateScheduler true "ID|Expression"
// @Success 200 {object} R{data=respCommon}
// @Failure 422 {object} R
// @Failure 500 {object} R
//
//nolint:dupl
func (s *Server) updateScheduler() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Input process and Validation
		req := reqUpdateScheduler{}
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})
			return
		}
		// Handle request
		err := s.scheduler.UpdateScheduler(c, domains.Scheduler{ID: domains.ID(req.ID), Expression: req.Expression})
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeDB, err.Error()))
			return
		}

		c.JSON(http.StatusOK, view(
			respCommon{ResultMsg: "Успешно изменён"}))
	}
}

// / Start scheduler action
// @Summary start scheduler action
// @Description Start scheduler action
// @Router /api/v1/ecomm/scheduler/start/{id} [PUT]
// @Tags Schedulers
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} R{data=respCommon}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) startScheduler() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Input process and Validation
		id := c.Param("id")
		if id == "" {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: "Ид планировщика не передан",
			})
			return
		}
		sch, err := s.scheduler.GetScheduler(c, id)
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeDB, err.Error()))
			return
		}
		if sch.Status == domains.JobStatusRunning {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeValidation, "Планировщик уже запущено"))
			return
		}
		jobID, err := s.schJob.CreateJob(sch.Code, sch.Expression)
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeRemoteOther, err.Error()))
			return
		}
		err = s.scheduler.UpdateSchedulerState(c, domains.Scheduler{ID: domains.ID(id), Status: domains.JobStatusRunning, JobID: jobID})
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeRemoteOther, err.Error()))
			return
		}
		c.JSON(http.StatusOK, view(
			respCommon{ResultMsg: "Принято в обработку"}))
	}
}

// / Stop scheduler action
// @Summary stop scheduler action
// @Description Stop scheduler action
// @Router /api/v1/ecomm/scheduler/stop/{id} [PUT]
// @Tags Schedulers
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} R{data=respCommon}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) stopScheduler() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Input process and Validation
		id := c.Param("id")
		if id == "" {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: "Ид планировщика не передан",
			})
			return
		}
		job, err := s.scheduler.GetScheduler(c, id)
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeRemoteOther, err.Error()))
			return
		}
		s.schJob.RemoveJob(job.JobID)
		// Handle request
		err = s.scheduler.UpdateSchedulerState(c, domains.Scheduler{ID: domains.ID(id), Status: domains.JobStatusStopped, JobID: 0})
		if err != nil {
			c.JSON(http.StatusInternalServerError, errView(status.ErrorCodeRemoteOther, err.Error()))
			return
		}
		c.JSON(http.StatusOK, view(
			respCommon{ResultMsg: "Принято в обработку"}))
	}
}

//nolint:unused
type reqScheduler struct {
	ID           string `json:"id" binding:"required"`
	TaskName     string `json:"task_name" binding:"required"`
	Interval     int8   `json:"interval"`
	IntervalForm int8   `json:"interval_form"`
	Action       int8   `json:"action"`
	RunTime      string `json:"run_time"`
}

// getJobStateView godoc
// @Summary run scheduler action
// @Description Run scheduler action
// @Router /api/v1/ecomm/schedulers/expression [PUT]
// @Tags Schedulers
// @Accept json
// @Produce json
// @Param reqScheduler body reqScheduler true "ID|TaskName|Interval|IntervalForm|Action|RunTime"
// @Success 200 {object} R{data=respCommon}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) runScheduler() gin.HandlerFunc { //nolint:all
	return func(c *gin.Context) {
		req := reqScheduler{}
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})
			return
		}

		c.JSON(http.StatusOK, view(
			respCommon{ResultMsg: "Успешно изменён"}))
	}
}

func getJobStateView(state int) string {
	var sv string
	switch state {
	case domains.JobStatusRunning:
		sv = Active
	case domains.JobStatusStopped:
		sv = Passive
	case domains.JobStatusFailed:
		sv = Error
	default:
		sv = NoStatus
	}
	return sv
}
