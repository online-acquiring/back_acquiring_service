package rest

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

//nolint:unused
type reqCheckParam struct {
	CardNumber   string `json:"card_number"  binding:"required"`
	CardExpire   string `json:"card_expire"   binding:"required"`
	Amount       int64  `json:"amount"   binding:"required"`
	CurrencyCode string `json:"currency_code"   binding:"required"`
}

type respCheckParam struct { //nolint
	PayID     string `json:"pay_id"`
	FeeAmount int64  `json:"fee_amount"`
}

// @Summary проверки карты
// @Description Проверки карты
// @Router /api/v1/ecomm/check-params [POST]
// @Tags External
// @Accept json
// @Produce json
// @Param reqCheckParam body reqCheckParam true "Number|Amount"
// @Success 200 {object} R{data=respCheckParam}
// @Failure 422 {object} R
// @Failure 500 {object} R
//
//nolint:unused
func (s *Server) externalCheckParams() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Input process and Validation
		req := reqCheckParam{}
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})
			return
		}

		c.JSON(http.StatusOK, view(respCheckParam{}))
	}
}

type reqConfirmPay struct { //nolint
	PayID       string `json:"pay_id"  binding:"required"`
	ExternalID  string `json:"external_id"   binding:"required"`
	ConfirmCode string `json:"confirm_code"   binding:"required"`
	Details     string `json:"details"`
}

type respConfirmPay struct { //nolint
	Status string `json:"status"`
}

// @Summary подтверждение платежа
// @Description Подтверждение платежа
// @Router /api/v1/ecomm/payment-confirm [POST]
// @Tags External
// @Accept json
// @Produce json
// @Param reqConfirmPay body reqConfirmPay true "Number|Amount"
// @Success 200 {object} R{data=respConfirmPay}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) confirmPayment() gin.HandlerFunc { //nolint
	return func(c *gin.Context) {
		// Input process and Validation
		req := reqConfirmPay{}
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})
			return
		}

		c.JSON(http.StatusOK, view(respConfirmPay{}))
	}
}

//nolint:unused
type reqReturnPay struct {
	PayID        string `json:"pay_id"  binding:"required"`
	ExternalID   string `json:"external_id"`
	ReturnAmount int64  `json:"return_amount"   binding:"required"`
}

//nolint:unused
type respReturnPay struct {
	Status string `json:"status"`
}

// @Summary отменить платежа
// @Description Отменить платежа
// @Router /api/v1/ecomm/payment-return [POST]
// @Tags External
// @Accept json
// @Produce json
// @Param reqReturnPay body reqReturnPay true "Number|Amount"
// @Success 200 {object} R{data=respReturnPay}
// @Failure 422 {object} R
// @Failure 500 {object} R
//
//nolint:unused
func (s *Server) returnPayment() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Input process and Validation
		req := reqReturnPay{}
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})
			return
		}

		c.JSON(http.StatusOK, view(respReturnPay{}))
	}
}

//nolint:unused
type reqHoldPay struct {
	PayID       string `json:"pay_id"  binding:"required"`
	ExternalID  string `json:"external_id"   binding:"required"`
	ConfirmCode string `json:"confirm_code"   binding:"required"`
	HoldTime    int8   `json:"hold_time"   binding:"required"`
	Details     string `json:"details"`
}

type respHoldPay struct { //nolint
	Status string `json:"status"`
}

// @Summary холдировать платежа
// @Description Холдировать платежа
// @Router /api/v1/ecomm/payment-hold [POST]
// @Tags External
// @Accept json
// @Produce json
// @Param reqHoldPay body reqHoldPay true "Number|Amount"
// @Success 200 {object} R{data=respHoldPay}
// @Failure 422 {object} R
// @Failure 500 {object} R
//
//nolint:unused
func (s *Server) holdPayment() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Input process and Validation
		req := reqHoldPay{}
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})
			return
		}

		c.JSON(http.StatusOK, view(respHoldPay{}))
	}
}

//nolint:unused
type reqConfirmHold struct {
	PayID string `json:"pay_id"  binding:"required"`
}

//nolint:unused
type respConfirmHold struct {
	Status string `json:"status"`
}

// @Summary подтвердить холд
// @Description Подтвердить холд
// @Router /api/v1/ecomm/confirm-hold [POST]
// @Tags External
// @Accept json
// @Produce json
// @Param reqConfirmHold body reqConfirmHold true "Number|Amount"
// @Success 200 {object} R{data=respConfirmHold}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) confirmHoldPayment() gin.HandlerFunc { //nolint
	return func(c *gin.Context) {
		// Input process and Validation
		req := reqConfirmHold{}
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})
			return
		}

		c.JSON(http.StatusOK, view(respConfirmHold{}))
	}
}

type reqCancelHold struct { //nolint
	PayID string `json:"pay_id"  binding:"required"`
}

type respCancelHold struct { //nolint
	Status string `json:"status"`
}

// @Summary возврать холд
// @Description Возврать холд
// @Router /api/v1/ecomm/cancel-hold [POST]
// @Tags External
// @Accept json
// @Produce json
// @Param reqCancelHold body reqCancelHold true "PayID"
// @Success 200 {object} R{data=respCancelHold}
// @Failure 422 {object} R
// @Failure 500 {object} R
func (s *Server) cancelHoldPayment() gin.HandlerFunc { //nolint
	return func(c *gin.Context) {
		// Input process and Validation
		req := reqCancelHold{}
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity, R{
				Status:    status.Failure,
				ErrorCode: status.ErrorCodeValidation,
				ErrorNote: err.Error(),
			})
			return
		}

		c.JSON(http.StatusOK, view(respCancelHold{}))
	}
}
