package rest

import (
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func (s *Server) endpoints() {
	noAuth := s.router.Group("/api/v1/ecomm")
	noAuth.POST("/login", s.login())
	noAuth.POST("/user/add-user", s.checkRole(domains.SuperAdminRole), s.addUser())
	noAuth.PUT("/user/state", s.checkRole(domains.SuperAdminRole), s.updateUserState())
	noAuth.PUT("/user/by-login", s.checkRole(domains.MerchantUserRole), s.updateUserPassByLogin())
	noAuth.PUT("/user/by-password", s.checkRole(domains.MerchantUserRole), s.updateUserByPassword())

	acquiring := s.router.Group("/api/v1/ecomm", s.middleware())
	acquiring.GET("/users", s.checkRole(domains.SuperAdminRole), s.getUsers())
	acquiring.PUT("/user", s.checkRole(domains.SuperAdminRole), s.updateUserState())
	acquiring.PUT("/user/pass", s.checkRole(domains.SuperAdminRole), s.updateUserPass())

	// merchant router
	acquiring.POST("/merchants/createMerchant", s.checkRole(domains.AdminRole), s.addMerchant())
	acquiring.GET("/merchants", s.checkRole(domains.AdminRole), s.getMerchants())
	acquiring.GET("/merchants/by-id/:id", s.checkRole(domains.AdminRole), s.getMerchant())
	acquiring.PUT("/merchants/merchant", s.checkRole(domains.AdminRole), s.updateMerchant())

	// terminal router
	acquiring.POST("/terminals/createTerminal", s.checkRole(domains.AdminRole), s.addTerminal())
	acquiring.GET("/terminals", s.checkRole(domains.AdminRole), s.getTerminals())
	acquiring.GET("/terminals/by-id/:id", s.checkRole(domains.AdminRole), s.getTerminal())
	acquiring.PUT("/terminals/terminal", s.checkRole(domains.AdminRole), s.updateTerminal())

	// account router
	acquiring.POST("/accounts/createAccount", s.checkRole(domains.AdminRole), s.addAccount())
	acquiring.GET("/accounts", s.checkRole(domains.AdminRole), s.getAccounts())
	acquiring.GET("/accounts/by-id/:id", s.checkRole(domains.AdminRole), s.getAccount())
	acquiring.PUT("/accounts/account", s.checkRole(domains.AdminRole), s.updateAccount())

	// payments router
	acquiring.PUT("/payments/state", s.checkRole(domains.MerchantUserRole), s.changePaymentState())
	acquiring.POST("/payments", s.checkRole(domains.MerchantUserRole), s.getPayments())
	acquiring.GET("/payments/by-id/:id", s.checkRole(domains.MerchantUserRole), s.getPayment())
	acquiring.GET("/payments/details/:id", s.checkRole(domains.MerchantUserRole), s.getPaymentDetails())
	acquiring.POST("/payments/addCard", s.checkRole(domains.MerchantUserRole), s.addCard())
	acquiring.PUT("/payments/confirmCard", s.checkRole(domains.MerchantUserRole), s.confirmCard())
	acquiring.POST("/payments/refund", s.checkRole(domains.MerchantUserRole), s.refundPayment())
	// temporary payments get
	acquiring.GET("/payments/get-all", s.checkRole(domains.MerchantUserRole), s.getAllPayments())
	acquiring.POST("/payments/payment/filter", s.checkRole(domains.MerchantUserRole), s.filterPayments())
	acquiring.POST("/payments/detail/filter", s.checkRole(domains.MerchantUserRole), s.filterDetails())

	//nolint
	// acquiring.POST("/terminals/createCreate", s.checkRole(domains.AdminRole), s.addTerminals())
	// acquiring.GET("/terminals", s.checkRole(domains.AdminRole), s.getTerminals())

	// param router
	acquiring.PUT("/param/code", s.checkRole(domains.SuperAdminRole), s.updateParamByCode())
	acquiring.PUT("/param/id", s.checkRole(domains.SuperAdminRole), s.updateParamByID())
	acquiring.GET("/params", s.checkRole(domains.SuperAdminRole), s.getParams())

	// scheduler
	acquiring.GET("/schedulers", s.checkRole(domains.SuperAdminRole), s.getSchedulers())
	acquiring.PUT("/scheduler", s.checkRole(domains.SuperAdminRole), s.updateScheduler())
	acquiring.PUT("/scheduler/start/:id", s.checkRole(domains.SuperAdminRole), s.startScheduler())
	acquiring.PUT("/scheduler/stop/:id", s.checkRole(domains.SuperAdminRole), s.stopScheduler())
	//nolint
	// acquiring.PUT("/schedulers/expression", s.checkRole(domains.SuperAdminRole), s.runScheduler())

	// object router
	acquiring.GET("/objects", s.checkRole(domains.MerchantUserRole), s.getProtocols())
	acquiring.GET("/objects/by-id/:payment_id", s.checkRole(domains.MerchantUserRole), s.getProtocolByID())
	s.router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
}
