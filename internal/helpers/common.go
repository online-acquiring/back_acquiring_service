package helpers

import "github.com/google/uuid"

const (
	ExpireSize = 4
)

func GenerateUUIDWithPrefix(pre string) string {
	id := uuid.New()
	return pre + id.String()
}

func FixExpiry(exp string) string {
	if len(exp) == ExpireSize {
		return exp[2:] + exp[:2]
	}
	return exp
}

const (
	cardNumberSize = 16
)

func ConvertMask(num string) string {
	if len(num) == cardNumberSize {
		return num[0:4] + " **** **** " + num[cardNumberSize-4:]
	}
	return num
}
