package tools

import (
	"database/sql"
	"math/rand"
	"time"
)

const (
	TimeStringTemplate  = "2006-01-02T15:04:05Z07"
	DateStringTemplate  = "2006-01-02"
	DateStringTemplate2 = "02.01.2006"
	DateTimeString      = "02.01.2006 15:04:05"
	DateTimeStringView  = "2006-01-02 15:04:05"
	DateStringView      = "2006-01-02"
	min                 = 1000
	max                 = 10000
)

func ParseDateToViewString(time2 time.Time) string {
	if time2.IsZero() {
		return ""
	}
	return time2.Format(DateStringView)
}

func ParseDate(timestr string) (*time.Time, error) {
	ts, err := time.Parse(DateStringTemplate, timestr)
	if err != nil {
		return nil, err
	}
	return &ts, nil
}

func ParseDBTime(tm sql.NullTime) time.Time {
	var rt time.Time
	if tm.Valid {
		rt = tm.Time
	}
	return rt
}

func ParseTimeToViewString(time2 time.Time) string {
	if time2.IsZero() {
		return ""
	}
	return time2.Format(DateTimeStringView)
}

func GetRandomInt() int {
	rand.NewSource(time.Now().UnixNano())
	return rand.Intn(max-min) + min //nolint
}
