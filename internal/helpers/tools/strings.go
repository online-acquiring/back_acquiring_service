package tools

import (
	"math/rand"
	"time"

	"github.com/google/uuid"
)

var seededRand *rand.Rand = rand.New(rand.NewSource(time.Now().UnixNano())) //nolint:gosec

func StringWithCharset(length int, charset string) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

func RandomDigitsString(length int) string {
	return StringWithCharset(length, "0123456789")
}

func MakeUUID() string {
	val, err := uuid.NewRandom()
	if err != nil {
		return "----"
	}
	return val.String()
}
