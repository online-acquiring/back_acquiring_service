package tools

import (
	"golang.org/x/crypto/bcrypt"
)

const defaultCost = 14

func HashSecret(secret string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(secret), defaultCost)
	return string(bytes), err
}

func CheckSecretHash(secret, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(secret))
	return err == nil
}
