package validation

import (
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/dustin/go-humanize"
)

const (
	MonthLimit = 12
)

// OnlyDigits check for number
func OnlyDigits(phone string) string {
	reg, err := regexp.Compile("[^0-9]+") //nolint:gocritic
	if err != nil {
		return phone
	}
	processedString := reg.ReplaceAllString(phone, "")
	return processedString
}

func ValidateDate(date string) bool {
	if date != "" {
		year, _ := strconv.Atoi(date[:2])
		month, _ := strconv.Atoi(date[2:])
		if month > MonthLimit {
			return false
		}

		time1 := time.Date(2000+year, time.Month(month), 1, 0, 0, 0, 0, time.UTC)
		time2 := time.Now().AddDate(0, 1, 0)

		return time1.After(time2)
	}

	return false
}

const Nine = 9

func PhoneValidation(p string) string {
	phone := strings.ReplaceAll(p, " ", "")
	if len(phone) >= Nine {
		return "998" + phone[len(phone)-9:]
	}
	return ""
}

func GetSumView(sum float64) string {
	return humanize.Commaf(sum)
}
