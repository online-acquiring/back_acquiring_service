package errs

import "errors"

const (
	ErrorCodeInternalServer = iota + 1000
	ErrorCodeInsufficientBalance
	ErrorCodeCardNotFound
	ErrorCodeWrongAction
	ErrorCodeSendOTP
	ErrorCodeMerchantNotFound
	ErrorCodeMerchantTerminalNotTied
	ErrorCodeCommissionNotFound
	ErrorCodeWrongOTP
	ErrorCodeHoldMethodNotWorking
	ErrorCodeConfirmMethodNotWorking
	ErrorCodeMethodNotFound
	ErrorCodeParamsNotFound
	ErrorCodeMethodUnknown
	ErrorCodeWrongInputParams
	ErrorCodePaymentNotFound
	ErrorCodeActionClosed
	ErrorCodeMerchantAccountNotTied
	ErrorCodeCancelTimeIsOver
	ErrorCodeRemoteFail
	ErrorCodeMerchantInsufficientBalance
	ErrorCodeCardExpired
	ErrorCodeLoginOrPassword
	ErrorCodeCardBinNotExist
)

var (
	InternalError           = NewError(ErrorCodeInternalServer, "Internal server error")
	InsufficientBalance     = NewError(ErrorCodeInsufficientBalance, "Insufficient balance on card")
	CardNotFound            = NewError(ErrorCodeCardNotFound, "The card is not found")
	WrongAction             = NewError(ErrorCodeWrongAction, "Wrong action")
	SendOTP                 = NewError(ErrorCodeSendOTP, "Send otp is not working")
	MerchantNotFound        = NewError(ErrorCodeMerchantNotFound, "The merchant is not found")
	MerchantTerminalNotTied = NewError(ErrorCodeMerchantTerminalNotTied, "The terminal is not tied to the merchant.")
	CommissionNotFound      = NewError(ErrorCodeCommissionNotFound, "The commission is not found")
	WrongOTP                = NewError(ErrorCodeWrongOTP, "The otp is wrong")
	HoldMethodNotWorking    = NewError(ErrorCodeHoldMethodNotWorking,
		"It is not possible to perform this 'hold' operation now, please try again later.")
	ConfirmMethodNotWorking = NewError(ErrorCodeConfirmMethodNotWorking,
		"It is not possible to perform this 'confirm' operation now, please try again later.")
	MethodNotFound              = NewError(ErrorCodeMethodNotFound, "Отсутствует поле 'method' в запросе")
	ParamsNotFound              = NewError(ErrorCodeParamsNotFound, "Отсутствует поле 'params' в запросе")
	MethodUnknown               = NewError(ErrorCodeMethodUnknown, "The method is unknown")
	WrongInputParams            = NewError(ErrorCodeWrongInputParams, "Something is wrong with request params")
	PaymentNotFound             = NewError(ErrorCodePaymentNotFound, "The payment id is wrong or non exist")
	ActionClosed                = NewError(ErrorCodeActionClosed, "The action is closed by admins")
	MerchantAccountNotTied      = NewError(ErrorCodeMerchantAccountNotTied, "The account of merchant is not fount")
	CancelTimeIsOver            = NewError(ErrorCodeCancelTimeIsOver, "The cancel time is over")
	RemoteServiceFail           = NewError(ErrorCodeRemoteFail, "Remote service error")
	MerchantInsufficientBalance = NewError(ErrorCodeMerchantInsufficientBalance, "Merchant has insufficient balance")
	CardExpired                 = NewError(ErrorCodeCardExpired, "Card expired")
	LoginOrPasswordWrong        = NewError(ErrorCodeLoginOrPassword, "Login or password is wrong")
	CardBinNotExist             = NewError(ErrorCodeCardBinNotExist, "Card bin is not found")
)

var (
	ErrEntityRelationNotLoaded = errors.New("relation is not loaded")
	ErrWrongInput              = errors.New("wrong input")
	ErrNotFound                = errors.New("entity not found")
	ErrSourceConnectionErr     = errors.New("connection error")
	ErrSourceInternal          = errors.New("internal connection error")
	// ErrValidation - External
	ErrValidation             = errors.New("validation error")
	ErrWithCardExpireDate     = errors.New("card year is expired")
	ErrUCNoInputIsProvided    = errors.New("no input")
	ErrorWithDailyCheckRS     = errors.New("perc not ok")
	ErrorWithAuthTransaction  = errors.New("supported card not found")
	ErrorPermissionCreditID   = errors.New("еще не рассчитан")
	ErrUnexpectedResponseData = errors.New("service returned unexpected data")
	ErrCardStatusIsInvalid    = errors.New("статус карты не активен")
	ErrRsInfoResponseData     = errors.New("базада бундай очик кредит мавжуд эмас")
	ErrRsInfoTypeIsWrong      = errors.New("тип клиента не совпадает")
	ErrSmsIsNotConnected      = errors.New("на данной карте не подключено смс")
	ErrGetCardInfo            = errors.New("decline, invalid card number")
	ErrInternalServer         = errors.New("internal server error")
	ErrHappenedInDB           = errors.New("db error occurred")
	ErrTimeOutTransact        = errors.New("iabs transact timeout")

	ErrInValidation         = errors.New("ошибки валидации параметров")
	ErrInternal             = errors.New("внутрненная ошибка сервера")
	ErrExternal             = errors.New("ошибка во внешных сервисах")
	ErrInAddition           = errors.New("временно отключен для добавления")
	ErrClientNotMatch       = errors.New("тип клиента не совпадает с которым указан")
	ErrSmsNotActive         = errors.New("смс номер не подключен на карту")
	ErrLoanIDNotFound       = errors.New("ид кредита не найдена")
	ErrSmsCodeIsWrong       = errors.New("смс код неправлиьно")
	ErrCardExpired          = errors.New("card expired")
	ErrAmountZero           = errors.New("amount equals 0")
	ErrAmountIsInsufficient = errors.New("amount is insufficient")
)

var (
	ErrIAbsRequestTimeout   = errors.New("IAbs request timeout error")
	ErrIAbsRequestUnknown   = errors.New("IAbs request unknown")
	ErrIAbsRequestDuplicate = errors.New("IAbs duplicate request")
	ErrIabsRequestIDError   = errors.New("IABS requestId error")
	ErrPSRequestTimeout     = errors.New("PS request timeout")
	ErrPSRequestUnknown     = errors.New("PS request unknown")
)

var (
	ErrWithPhoneParam     = errors.New("number is invalid to send a service_sms")
	ErrWithSmsCode        = errors.New("смс код не совпадает")
	ErrCodeWrongPhone     = errors.New("phone is not matching")
	ErrCodeAttemptsOver   = errors.New("card is blocked")
	ErrCodeValidationData = errors.New("request params not valid")
	ErrWrongOTP           = errors.New("wrong otp")
	ErrBadOTPRequestID    = errors.New("bad OTP request ID, try new OTP")
	ErrOTPExpired         = NewError(ErrorCodeWrongOTP, "otp expired")
	ErrAuthorization      = errors.New("can't authorize")
	ErrTokenNotFound      = errors.New("token not found")
	ErrChangePassword     = errors.New("вам был предоставлен одноразовый пароль, пожалуйста, создайте свой собственный пароль")
	ErrPasswordExpired    = errors.New("срок действия вашего пароля истек, пожалуйста, измените пароль в настройках")
	ErrTheSamePassword    = errors.New("новый пароль не должен совпадать со старым")
	ErrCardIsBlocked      = errors.New("карта блокирован")
)
