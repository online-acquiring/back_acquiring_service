package redis_cache //nolint

import (
	"context"
	"encoding/json"
	"time"
)

const (
	defaultDuration = 2
)

type RedisServ struct {
	log logger.Logger
	rd  redis.UniversalClient
}

func New(log logger.Logger, rds redis.UniversalClient) *RedisServ {
	return &RedisServ{
		log: log,
		rd:  rds,
	}
}

func (r *RedisServ) SetWithDuration(ctx context.Context, key string, data interface{}, duration time.Duration) error {
	jData, err := json.Marshal(data)
	if err != nil {
		r.log.Error("caching:redis_cache:SetWithDuration:marshal-" + err.Error())
		return err
	}
	err = r.rd.Set(ctx, cacheKey(key), jData, duration).Err()
	if err != nil {
		r.log.Error("caching:redis_cache:SetWithDuration:Set-" + err.Error())
	}
	return err
}

func (r *RedisServ) Delete(ctx context.Context, key string) error {
	err := r.rd.Del(ctx, cacheKey(key)).Err()
	if err != nil {
		r.log.Error("caching:redis_cache:Delete:Del-" + err.Error())
	}
	return err
}

func (r *RedisServ) UpdateData(ctx context.Context, key string, data interface{}) error {
	panic("implement me")
}

func (r *RedisServ) SetData(ctx context.Context, key string, data interface{}) error {
	jData, err := json.Marshal(data)
	if err != nil {
		r.log.Error("caching:redis_cache:SetData:marshal-" + err.Error())
		return err
	}
	err = r.rd.Set(ctx, cacheKey(key), jData, defaultDuration*time.Minute).Err()
	if err != nil {
		r.log.Error("caching:redis_cache:SetData:Set-" + err.Error())
	}
	return err
}

func (r *RedisServ) GetData(ctx context.Context, key string, resp interface{}) (err error) {
	val, err := r.rd.Get(ctx, cacheKey(key)).Bytes()
	if err != nil {
		r.log.Error("caching:redis_cache:GetData:Get-" + err.Error())
		return err
	}
	err = json.Unmarshal(val, resp)
	if err != nil {
		r.log.Error("caching:redis_cache:GetData:unmarshal-" + err.Error())
	}
	return
}

// cacheKey prefixes all keys with service_name.
// Required action for Redis cluster deployment
// as it does not support database
func cacheKey(originalKey string) string {
	return config.ServiceLabel + "-" + originalKey
}
