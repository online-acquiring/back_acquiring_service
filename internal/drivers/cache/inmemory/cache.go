package inmemory

import (
	"context"
	"encoding/json"
	"errors"
	
	"github.com/allegro/bigcache"
	"go.uber.org/zap"
)

type Cache struct {
	inMemory *bigcache.BigCache
	log      logger.Logger
}

func New(cfg bigcache.Config, l logger.Logger) *Cache {
	bigCache, err := bigcache.NewBigCache(cfg)
	if err != nil {
		l.Error("bigcache.NewBigCache failed", zap.Error(err))
		panic(err)
	}

	return &Cache{inMemory: bigCache, log: l}
}

func (c *Cache) Cleanup() error {
	return c.inMemory.Close()
}

func (c *Cache) Put(ctx context.Context, key string, value any) error {
	l := logger.FromCtx(ctx, "inmemory.Put")

	if value == nil || key == "" {
		return errs.Errf(errs.ErrValidation, "entry can not be cached - empty value or key")
	}

	b, err := json.Marshal(value)
	if err != nil {
		l.Error("json.Marshal failed", zap.Error(err))
		return err
	}

	if err := c.inMemory.Set(key, b); err != nil {
		l.Error("BigCache.Set failed", zap.Error(err))
		return err
	}

	return nil
}

func (c *Cache) Get(ctx context.Context, key string, dest any) error {
	l := logger.FromCtx(ctx, "inmemory.Get")

	if key == "" {
		return errs.Errf(errs.ErrValidation, "empty_key")
	}

	res, err := c.inMemory.Get(key)

	switch {
	case errors.Is(err, bigcache.ErrEntryNotFound):
		l.Debug("BigCache.Get item not found", zap.Error(err))
		return errs.Errf(errs.ErrNotFound, err.Error())
	case err != nil:
		return err
	default:
		l.Debug("Retrieved from cache")
	}

	if err := json.Unmarshal(res, dest); err != nil {
		l.Warn("failed to Unmarshal value into dest", zap.Error(err))
		return err
	}

	return nil
}
