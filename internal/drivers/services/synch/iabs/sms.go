package iabs

import (
	"context"
	"crypto/tls"
	"errors"
	"net/http"
	"time"

	"github.com/google/uuid"
	"go.uber.org/zap"
)

type Client struct {
	httpRemote remote.HTTPClientI
	cfg        config.Config
}

func New(cfg config.Config) *Client {
	var cf tls.Config
	cf.InsecureSkipVerify = true //nolint:gosec

	client := remote.New(cfg.IabsAPI.EsbHost, &http.Transport{
		TLSClientConfig: &cf})

	client.SetMiddleware(newIabsMiddlewareForOtp(cfg.IabsAPI))

	return &Client{httpRemote: client, cfg: cfg}
}

func (uc *Client) SendSms(ctx context.Context, smsData domains.SMS) (string, error) {
	var (
		l     = logger.FromCtx(ctx, "sms.SendSms")
		resp  response
		param smsParams
	)
	param.LocalCode = localCode
	param.IPhone = smsData.Phone
	param.IMessage = smsData.Message
	param.ISmsType = smsType
	param.ITaskCode = taskCode
	param.ITempID = tempID

	var hMap = map[string]string{}
	hMap["requestId"] = uuid.NewString()
	hMap["lang"] = langUZC

	// add 15 sec timeout on request
	ctxT, cancel := context.WithTimeout(ctx, time.Second*defaultTimeOut)
	defer cancel()

	err := uc.httpRemote.Post(ctxT, &resp, sendOtp, &param, hMap)
	if err != nil {
		l.Error("s.httpRemote.Post failed", zap.Error(err),
			zap.String("raw-params", param.Raw()))

		return "", errs.ErrWrongInput
	}

	if err := resp.ToError(); err != nil {
		l.Error("resp.ToError() failed",
			zap.String("raw-params", param.Raw()),
			zap.String("resp-params", resp.Raw()))

		if errors.Is(err, errs.ErrTokenNotFound) {
			return uc.SendSms(ctx, smsData)
		}

		return "", errs.ErrWithSmsCode
	}

	return smsData.SmsCode, nil
}
