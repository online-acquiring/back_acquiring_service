package iabs

import (
	"bytes"
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"sync"
)

type middleware struct {
	mu    sync.Mutex
	cfg   config.IabsAPI
	token string
}
type tokenReq struct {
	MFO      string `json:"filialCode"`
	Username string `json:"username"`
	Password string `json:"password"`
}

func newIabsMiddlewareForOtp(cfg config.IabsAPI) remote.Middleware {
	return &middleware{
		mu:  sync.Mutex{},
		cfg: cfg,
	}
}

func (m *middleware) BeforeRequest(ctx context.Context, req *http.Request) error {
	t, err := m.getAuthorization(ctx)
	if err != nil {
		return err
	}

	req.Header.Set("Authorization", t)

	return nil
}

func (m *middleware) getAuthorization(ctx context.Context) (string, error) {
	m.mu.Lock()
	defer m.mu.Unlock()

	if m.token == "" {
		tokens, err := m.retrieveToken(ctx)
		if err != nil {
			return "", err
		}

		m.token = tokens
	}

	return fmt.Sprintf("%s %s", "Bearer", m.token), nil
}

func (m *middleware) retrieveToken(ctx context.Context) (token string, err error) {
	type tokenResp struct {
		Token string `json:"token"`
	}

	var (
		tkResp  tokenResp
		reqBody tokenReq
	)
	reqBody.MFO = mfo
	reqBody.Username = m.cfg.Username
	reqBody.Password = m.cfg.Password
	body, err := json.Marshal(reqBody)
	if err != nil {
		return token, err
	}

	var (
		reader = bytes.NewReader(body)
		url    = m.cfg.EsbHost + getToken
	)
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, url, reader)
	if err != nil {
		return token, err
	}

	req.Header.Set("Content-Type", "application/json")
	var cf tls.Config
	cf.InsecureSkipVerify = true //nolint:gosec
	client := &http.Client{Transport: &http.Transport{
		TLSClientConfig: &cf}}
	resp, err := client.Do(req)
	if err != nil {
		return token, err
	}

	defer resp.Body.Close()

	b, err := io.ReadAll(resp.Body)
	if err != nil {
		return token, err
	}

	if err := json.Unmarshal(b, &tkResp); err != nil {
		return token, err
	}

	token = tkResp.Token

	return token, err
}

func (m *middleware) AfterResponse(res *http.Response) (err error) {
	if res.StatusCode == http.StatusUnauthorized {
		m.token = ""
	}
	return
}
