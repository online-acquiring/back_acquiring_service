package iabs

import (
	"context"
	"crypto/tls"
	"net/http"

	"github.com/google/uuid"
	"github.com/spf13/cast"
	"go.uber.org/zap"
)

type ClientIabs struct {
	httpRemote remote.HTTPClientI
	cfg        config.Config
}

// init iabs service
func NewIabsEsb(cfg config.Config) *ClientIabs {
	var cf tls.Config
	cf.InsecureSkipVerify = true //nolint:gosec
	client := remote.New(cfg.IabsAPI.EsbHost, &http.Transport{
		TLSClientConfig: &cf,
	})

	client.SetMiddleware(synch.NewIabsMiddleware(cfg))

	return &ClientIabs{httpRemote: client, cfg: cfg}
}

func (uc *ClientIabs) GetAccountDetail(ctx context.Context, code, mfo string) (account domains.Account, err error) {
	var (
		l      = logger.FromCtx(ctx, "check_account.GetAccountDetail")
		result AccountDetailResp
		resp   response
		url    = "/1.0.0/get-account-details?account=" + code + "&codeFilial=" + mfo
		param  GetAccountDetailParams
		hMap   = map[string]string{}
	)

	hMap["requestId"] = uuid.NewString()
	hMap["Accept-Language"] = "en"

	if err = uc.httpRemote.Get(ctx, &resp, url, nil, hMap); err != nil {
		l.Error("s.httpRemote.Get failed", zap.Error(err))

		return //nolint
	}

	if err = resp.ToError(); err != nil {
		l.Error("resp.ToError() failed",
			zap.String("raw-params", param.Raw()),
			zap.String("resp-params", resp.Raw()))

		return //nolint
	}

	if err = resp.Scan(&result); err != nil {
		l.Error("resp.ToError() failed",
			zap.String("raw-params", param.Raw()),
			zap.String("resp-params", resp.Raw()))

		return //nolint
	}

	account.Saldo = cast.ToFloat64(result.Saldo)
	account.Name = result.NameAcc
	account.Inn = result.Inn

	return account, nil
}
