package iabs

import (
	"encoding/json"
)

type smsParams struct {
	LocalCode string `json:"localcode"`
	IPhone    string `json:"phone"`
	IMessage  string `json:"smstext"`
	ISmsType  string `json:"smstype"`
	ITaskCode string `json:"taskcode"`
	ITempID   string `json:"tempId"`
}

type response struct {
	Token    string           `json:"token"`
	Code     int              `json:"code"`
	Msg      string           `json:"msg"`
	RespBody *json.RawMessage `json:"responseBody"`
	Error    string           `json:"error"`
}

func (r *response) ToError() error {
	if r.Error == "Token is not valid" {
		return errs.ErrTokenNotFound
	}

	if r.Code != 0 {
		return errs.ErrInternal
	}

	return nil
}

func (r *response) Scan(model interface{}) error {
	if err := json.Unmarshal(*r.RespBody, &model); err != nil {
		return err
	}
	return nil
}

func (r *response) Raw() string {
	raw, _ := json.Marshal(r)

	return string(raw)
}

func (req *smsParams) Raw() string {
	raw, _ := json.Marshal(req)

	return string(raw)
}

// Iabs
type GetAccountDetailParams struct {
	Token      string `json:"token"`
	RequestID  string `json:"requestId"`
	Account    string `json:"account"`
	CodeFilial string `json:"codeFilial"`
}

func (req *GetAccountDetailParams) Raw() string {
	raw, _ := json.Marshal(req)

	return string(raw)
}

type AccountDetailResp struct {
	ID             int    `json:"id"`
	TypeAccount    string `json:"typeAccount"`
	Account        string `json:"account"`
	NameAcc        string `json:"nameAcc"`
	Inn            string `json:"inn"`
	CodeCurrency   string `json:"codeCurrency"`
	Saldo          string `json:"saldo"`
	CodeFilial     string `json:"codeFilial"`
	CodeCoa        string `json:"codeCoa"`
	NameFilial     string `json:"nameFilial"`
	Condition      string `json:"condition"`
	OpenDate       string `json:"openDate"`
	PrOpen         string `json:"prOpen"`
	ClientUID      int    `json:"clientUid"`
	ClientID       int    `json:"clientId"`
	MobilePhone    string `json:"mobilePhone"`
	CodeSubAccount string `json:"codeSubAccount"`
}
