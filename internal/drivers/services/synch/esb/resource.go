package esb

import (
	"encoding/json"
)

type esbRequest struct{ remote.ESBRequest }

func (e *esbRequest) Raw() string {
	raw, _ := json.Marshal(e)

	return string(raw)
}

type esbDTO struct {
	HTTPStatus   string          `json:"httpStatus"`
	ErrorCode    string          `json:"errorCode"`
	ErrorMessage string          `json:"errorMessage"`
	Response     json.RawMessage `json:"response"`
}

func (e *esbDTO) ToError() error {
	if e.ErrorCode != "0" {
		return errs.ErrInternal
	}

	return nil
}

func (e *esbDTO) Raw() string {
	raw, _ := json.Marshal(e)

	return string(raw)
}
