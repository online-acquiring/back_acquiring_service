package esb

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"errors"
	"net/http"

	"go.uber.org/zap"
)

type Client struct {
	httpClient remote.HTTPClientI
	cfg        config.Config
}

func New(cfg config.Config) *Client {
	var cf tls.Config
	cf.InsecureSkipVerify = true //nolint
	client := remote.New(cfg.EsbHost, &http.Transport{
		TLSClientConfig: &cf,
	})

	return &Client{
		httpClient: client,
		cfg:        cfg,
	}
}

func (c *Client) GetFiscalCode(ctx context.Context, fiscal domains.Fiscal) (domains.FiscalInfo, error) {
	var (
		l    = logger.FromCtx(ctx, "esb.GetFiscalCode")
		resp remote.ESBResponse
	)

	req := esbRequest{remote.NewESBRequest(msgMethod, fiscal, esbMsgSource)}

	if err := c.httpClient.Post(ctx, &resp, esbDefaultRoute, req, nil); err != nil {
		l.Error("c.httpClient.Post failed", zap.Error(err),
			zap.String("req-params", req.Raw()))

		return domains.FiscalInfo{}, err
	}

	var wrapper esbDTO
	if err := resp.Scan(&wrapper); err != nil {
		l.Error("resp.Scan failed", zap.Error(err))

		return domains.FiscalInfo{}, err
	}

	if wrapper.ToError() != nil {
		l.Error("wrapper.ToError() failed", zap.Error(errors.New(wrapper.ErrorMessage)))

		return domains.FiscalInfo{}, errors.New(wrapper.ErrorMessage)
	}

	var result domains.FiscalInfo
	if err := json.Unmarshal(wrapper.Response, &result); err != nil {
		l.Error("json.Unmarshal failed", zap.Error(err))

		return domains.FiscalInfo{}, err
	}

	return result, nil
}
