package uzcard

import "encoding/json"

// Request
type Trans struct {
	Purpose    string  `json:"purpose"`
	ReceiverID string  `json:"receiverId"`
	CardID     string  `json:"cardId"`
	Amount     float64 `json:"amount"`
	Commission float64 `json:"commission"`
	Currency   string  `json:"currency"`
	Ext        string  `json:"ext"`
	MerchantID string  `json:"merchantId"`
	TerminalID string  `json:"terminalId"`
}

type TransPurpose struct {
	Trans Trans `json:"tran"`
}

// Uzcard
type svGateRequest struct {
	Jsonrpc string      `json:"jsonrpc,omitempty"`
	ID      int         `json:"id,omitempty"`
	Method  string      `json:"method"`
	Params  interface{} `json:"params"`
}

type Dismiss struct {
	HoldID     string  `json:"holdId"`
	MerchantID string  `json:"merchantId"`
	TerminalID string  `json:"terminalId"`
	Amount     float64 `json:"amount"`
}
type otpParams struct {
	Card otpCard `json:"card,omitempty"`
	OTP  OTP     `json:"otp,omitempty"`
}

type OTP struct {
	ID   int    `json:"id"`
	Code string `json:"code"`
}

type holdCharge struct {
	HoldID     int64   `json:"holdId"`
	Ext        string  `json:"ext"`
	MerchantID string  `json:"merchantId"`
	Port       string  `json:"port"`
	TerminalID string  `json:"terminalId"`
	Stan       string  `json:"stan"`
	Amount     float64 `json:"amount"`
	Date12     string  `json:"date12"`
}
type HoldCreateResp struct {
	ID          int64  `json:"id"`
	Status      int    `json:"status"`
	Description string `json:"description"`
}
type svCardsGetParam struct {
	IDs []string `json:"ids"`
}

func (sv svGateRequest) Raw() string {
	raw, _ := json.Marshal(sv)
	return string(raw)
}

type SvTranGetParams struct {
	SvID string `json:"svId"`
}

type SvTranGetByExtParams struct {
	ExtID string `json:"extId"`
}

type SvTranReverseV1Params struct {
	TranID string `json:"tranId"`
}
type svTranReverseV2Params struct {
	Tran Tran `json:"tran"`
}
type Tran struct {
	TranID        string  `json:"tranId"`
	ReverseAmount float64 `json:"reverseAmount"`
	Ext           string  `json:"ext"`
}
type SvTranReverseByExtParams struct {
	ExtID string `json:"extId"`
}
type HoldDismissParams struct {
	Hold Dismiss `json:"hold"`
}
type holdChargeParams struct {
	Hold holdCharge `json:"hold"`
}

type HoldGetParams struct {
	ID int64 `json:"id"`
}

type svGetCardID struct {
	Pan string `json:"hpan"`
}

type svGetCards struct {
	Phone string `json:"phone"`
}

type SvGateNinetyDaysReversal struct {
	Credit SvGateCredit `json:"credit"`
}

type holdCreateParam struct {
	Hold hold `json:"hold"`
}

type SvGateCredit struct {
	Ext        string `json:"ext"`
	Amount     int    `json:"amount"`
	MerchantID string `json:"merchantId"`
	TerminalID string `json:"terminalId"`
	Recipient  string `json:"recipient"`
	Sender     Sender `json:"sender"`
}

type Sender struct {
	System     string `json:"system"`
	LegalName  string `json:"legalName"`
	LastName   string `json:"lastName"`
	FirstName  string `json:"firstName"`
	MiddleName string `json:"middleName"`
	ID         string `json:"id"`
	RefNum     string `json:"refNum"`
	Doc        Doc    `json:"doc"`
}

type Doc struct {
	Nationality  string `json:"nationality"`
	Type         string `json:"type"`
	SeriesNumber string `json:"seriesNumber"`
	BirthDate    string `json:"birthDate"`
	ValidTo      string `json:"validTo"`
	Mrz          string `json:"mrz"`
}

type sms struct {
	Ussd       string `json:"ussd"`
	Hash       string `json:"hash"`
	TemplateID int    `json:"templateId"`
}

type otpCard struct {
	Pan            string `json:"pan"`
	Expiry         string `json:"expiry"`
	Sms            sms    `json:"iabs"`
	RequestorPhone string `json:"requestorPhone"`
}

type hold struct {
	CardID     string  `json:"cardId"`
	MerchantID string  `json:"merchantId"`
	TerminalID string  `json:"terminalId"`
	Amount     float64 `json:"amount"`
	Time       int     `json:"time"`
}
type CardNewParam struct {
	Card Card `json:"card"`
}

type Card struct {
	Number string `json:"pan"`
	Expiry string `json:"expiry"`
}

// Response
// Uzcard
type svGateResponse struct {
	Jsonrpc string          `json:"jsonrpc"`
	ID      int             `json:"id"`
	Result  json.RawMessage `json:"result"`
	Error   struct {
		Code    int    `json:"code"`
		Message string `json:"message"`
	} `json:"error"`
}

func (s *svGateResponse) Scan(model interface{}) error {
	err := json.Unmarshal(s.Result, &model)
	if err != nil {
		return err
	}
	return nil
}

type CardNewResp struct {
	ID             string      `json:"id"`
	UserName       string      `json:"username"`
	Pan            string      `json:"pan"`
	Expiry         string      `json:"expiry"`
	Status         int         `json:"status"`
	Phone          string      `json:"phone"`
	FullName       string      `json:"fullName"`
	Balance        int         `json:"balance"`
	Sms            bool        `json:"sms"`
	PinCnt         int         `json:"pincnt"`
	Aacct          string      `json:"aacct"`
	CardType       string      `json:"cardtype"`
	HoldAmount     interface{} `json:"holdAmount"`
	CashbackAmount interface{} `json:"cashbackAmount"`
	Par            interface{} `json:"par"`
}

type SvCardIDResp struct {
	CardNum    string `json:"CREF_NO"`
	FIO        string `json:"EMBOS_NAME"`
	ExpDt      string `json:"EXP_DT"`
	CardType   string `json:"CARDTYPE"`
	CardStatus string `json:"CARDSTATUS"`
	CardID     string `json:"CARDID"`
}

type ScCardsResp struct {
	ID         string `json:"id"`
	Pan        string `json:"pan"`
	Mfo        string `json:"mfo"`
	FullName   string `json:"fullName"`
	ContractID string `json:"contractId"`
	LastActive string `json:"lastActive"`
}

type SvTransPayResp struct {
	ID         string `json:"id"`
	Username   string `json:"username"`
	RefNum     string `json:"refNum"`
	Ext        string `json:"ext"`
	Pan        string `json:"pan"`
	Pan2       string `json:"pan2"`
	Expiry     string `json:"expiry"`
	TranType   string `json:"tranType"`
	Date7      string `json:"date7"`
	Date12     string `json:"date12"`
	Amount     int    `json:"amount"`
	Currency   int    `json:"currency"`
	Stan       string `json:"stan"`
	MerchantID string `json:"merchantId"`
	TerminalID string `json:"terminalId"`
	Resp       int    `json:"resp"`
	RespText   string `json:"respText"`
	RespSV     string `json:"respSV"`
	Status     string `json:"status"`
}

func (s *svGateResponse) Raw() string {
	raw, _ := json.Marshal(s)
	return string(raw)
}

func (s *svGateResponse) ToError() error {
	return nil
}

type NewOtpRespCard struct {
	ID        int    `json:"id"`
	PhoneMask string `json:"phoneMask"`
	Token     string `json:"token"`
	Verified  bool   `json:"verified"`
}
