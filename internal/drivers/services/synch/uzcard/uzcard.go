package uzcard

import (
	"context"
	"crypto/tls"
	"errors"
	"net/http"
	"strconv"
	"time"

	"github.com/spf13/cast"

	"go.uber.org/zap"
)

type Client struct {
	httpClient remote.HTTPClientI
	cfg        config.Config
}

func (c *Client) GetCardNumber(ctx context.Context, cardID string) (*domains.Card, error) {
	// this method does not work for UZCARD
	panic("this method does not work for UZCARD")
}

func New(cfg config.Config) *Client {
	cf := tls.Config{
		InsecureSkipVerify: true, //nolint
	}

	client := remote.New(cfg.OpenAPI.Host, &http.Transport{
		TLSClientConfig: &cf,
	})
	client.SetMiddleware(synch.GetOpenAPIMiddleware(cfg))

	return &Client{
		httpClient: client,
		cfg:        cfg,
	}
}

func (c *Client) GetCardMaskedPAN(ctx context.Context, cardNumber string) (*domains.Card, error) {
	// this method does not work for UZCARD
	panic("this method does not work for UZCARD")
}

func (c *Client) CardNewOTP(ctx context.Context, pan, exDate, hash, phone string, tempID int) (string, error) {
	var (
		l      = logger.FromCtx(ctx, "uzcard.CardNewOTP")
		resp   svGateResponse
		url    = c.cfg.UzCardOpenAPIPath + svGateRoute
		sms    sms
		card   otpCard
		params otpParams
		req    svGateRequest
	)

	sms.Ussd = ussd
	sms.Hash = hash
	sms.TemplateID = tempID

	card.Pan = pan
	card.Expiry = exDate
	card.Sms = sms
	card.RequestorPhone = phone

	params.Card = card

	req.Jsonrpc = jsonRPC
	req.ID = defaultID
	req.Method = cardsNewOTP
	req.Params = params

	err := c.httpClient.Post(ctx, &resp, url, &req, nil)
	if err != nil {
		l.Error("c.httpClient.Post failed", zap.Error(err),
			zap.String("raw-params", req.Raw()))

		return "", err
	}

	if resp.Error.Code != 0 {
		l.Error("resp.Error.Code != 0",
			zap.String("errMsg", resp.Error.Message),
			zap.Int("errCode", resp.Error.Code))

		switch resp.Error.Code {
		case -320:
			return "", errs.ErrCodeWrongPhone
		case -206, -215:
			return "", errs.ErrCodeValidationData
		default:
			return "", errs.NewError(resp.Error.Code, resp.Error.Message)
		}
	}

	var result NewOtpRespCard
	if err = resp.Scan(&result); err != nil {
		l.Error("resp.Scan failed", zap.Error(err),
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", string(resp.Result)),
		)

		return "", err
	}

	return strconv.Itoa(result.ID), nil
}

func (c *Client) CardNewVerify(ctx context.Context, pSignID, cCode string) (*domains.Card, error) {
	var (
		l    = logger.FromCtx(ctx, "uzcard.CardNewVerify")
		resp svGateResponse
		url  = c.cfg.UzCardOpenAPIPath + svGateRoute
	)

	pID, err := strconv.Atoi(pSignID)
	if err != nil {
		l.Error("strconv.Atoi(pSignID) failed", zap.Error(err))

		return nil, err
	}

	var (
		params otpParams
		otp    OTP
		svgReq svGateRequest
	)

	otp.ID = pID
	otp.Code = cCode

	params.OTP = otp

	svgReq.Jsonrpc = jsonRPC
	svgReq.ID = defaultID
	svgReq.Method = cardsNewVerify
	svgReq.Params = params

	err = c.httpClient.Post(ctx, &resp, url, &svgReq, nil)
	if err != nil {
		l.Error("c.httpClient.Post failed", zap.Error(err),
			zap.String("raw-params", svgReq.Raw()))

		return nil, err
	}

	if resp.Error.Code != 0 {
		l.Error("resp.Error.Code != 0",
			zap.String("errMsg", resp.Error.Message),
			zap.Int("errCode", resp.Error.Code))

		switch resp.Error.Code {
		case -261:
			return nil, errs.ErrCodeAttemptsOver
		case -269:
			return nil, errs.ErrWrongOTP
		case -314:
			return nil, errs.ErrBadOTPRequestID
		case -270:
			return nil, errs.ErrOTPExpired
		default:
			return nil, errs.NewError(resp.Error.Code, resp.Error.Message)
		}
	}

	var result CardNewResp
	if err = resp.Scan(&result); err != nil {
		l.Error("resp.Scan(&result) failed", zap.Error(err),
			zap.String("resp-params", string(resp.Result)))

		return nil, err
	}

	return &domains.Card{
		ID:      result.ID,
		Status:  convertCardStatus(result.Status),
		Phone:   result.Phone,
		Balance: float64(result.Balance),
	}, nil
}

func (c *Client) GetCardByPhone(ctx context.Context, phone string) ([]domains.Card, error) {
	var (
		l     = logger.FromCtx(ctx, "uzcard.GetCardByPhone")
		resp  svGateResponse
		url   = c.cfg.UzCardOpenAPIPath + svGateRoute
		param svGetCards
		req   svGateRequest
	)

	param.Phone = phone

	req.Jsonrpc = jsonRPC
	req.ID = defaultID
	req.Method = cardsListToken
	req.Params = param

	if err := c.httpClient.Post(ctx, &resp, url, req, nil); err != nil {
		l.Error("c.httpClient.Post failed", zap.Error(err),
			zap.String("raw-params", req.Raw()))

		return nil, err
	}

	if resp.Error.Code != 0 {
		l.Error("resp.Error.Code failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", resp.Raw()))

		return nil, errors.New(resp.Error.Message)
	}

	var result []ScCardsResp
	if err := resp.Scan(&result); err != nil {
		l.Error("resp.Scan failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", string(resp.Result)))

		return nil, err
	}

	cards := make([]domains.Card, len(result))
	for i, v := range result {
		var card domains.Card
		card.ID = v.ID
		cards[i] = card
	}

	return cards, nil
}

func (c *Client) GetCardID(ctx context.Context, number string) (*domains.Card, error) {
	var (
		l     = logger.FromCtx(ctx, "uzcard.GetCardID")
		resp  svGateResponse
		url   = c.cfg.UzCardOpenAPIPath + svGateRoute
		param svGetCardID
		req   svGateRequest
	)

	param.Pan = number

	req.Jsonrpc = jsonRPC
	req.ID = defaultID
	req.Method = p2pInfo
	req.Params = param

	if err := c.httpClient.Post(ctx, &resp, url, req, nil); err != nil {
		l.Error("c.httpClient.Post failed", zap.Error(err),
			zap.String("raw-params", req.Raw()))

		return nil, err
	}

	if resp.Error.Code != 0 {
		l.Error("resp.Error.Code failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", resp.Raw()))

		return nil, errors.New(resp.Error.Message)
	}

	var result SvCardIDResp
	if err := resp.Scan(&result); err != nil {
		l.Error("resp.Scan failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", string(resp.Result)))

		return nil, err
	}

	// cardStatus chaqirilgan joydan tekshiramiz
	return &domains.Card{
		ID:     result.CardID,
		Status: result.CardStatus,
		Number: number,
	}, nil
}

func (c *Client) GetCardBalance(ctx context.Context, id string) (*domains.Card, error) {
	var (
		l     = logger.FromCtx(ctx, "uzcard.GetCardBalance")
		resp  svGateResponse
		url   = c.cfg.UzCardOpenAPIPath + svGateRoute
		ids   []string
		param svCardsGetParam
		req   svGateRequest
	)

	ids = append(ids, id)

	param.IDs = ids

	req.Jsonrpc = jsonRPC
	req.ID = defaultID
	req.Method = cardsGet
	req.Params = param

	if err := c.httpClient.Post(ctx, &resp, url, req, nil); err != nil {
		l.Error("c.httpClient.Post failed", zap.Error(err),
			zap.String("raw-params", req.Raw()))

		return nil, err
	}

	if resp.Error.Code != 0 {
		l.Error("resp.Error.Code failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", resp.Raw()))

		return nil, errors.New(resp.Error.Message)
	}

	var result []CardNewResp
	if err := resp.Scan(&result); err != nil {
		l.Error("resp.Scan failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", string(resp.Result)))

		return nil, err
	}

	if !result[0].Sms {
		l.Error("!result[0].Sms failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", string(resp.Result)))

		return nil, errs.ErrSmsIsNotConnected
	}

	return &domains.Card{
		ID:      result[0].ID,
		Number:  helpers.ConvertMask(result[0].Pan),
		Status:  convertCardStatus(result[0].Status),
		Phone:   result[0].Phone,
		Balance: float64(result[0].Balance),
		Expiry:  result[0].Expiry,
	}, nil
}

func (c *Client) GetTransactionByExt(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error) {
	var (
		l     = logger.FromCtx(ctx, "uzcard.GetTransactionByExt")
		resp  svGateResponse
		url   = c.cfg.UzCardOpenAPIPath + svGateRoute
		param SvTranGetByExtParams
		req   svGateRequest
	)

	param.ExtID = tran.ExternalID

	req.Jsonrpc = jsonRPC
	req.ID = defaultID
	req.Method = transExt
	req.Params = param

	if err := c.httpClient.Post(ctx, &resp, url, req, nil); err != nil {
		l.Error("c.httpClient.Post failed", zap.Error(err),
			zap.String("raw-params", req.Raw()))

		return tran, err
	}

	if resp.Error.Code != 0 {
		l.Error("resp.Error.Code failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", resp.Raw()))

		return tran, errors.New(resp.Error.Message)
	}

	var result []SvTransPayResp
	if err := resp.Scan(&result); err != nil {
		l.Error("resp.Scan failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", string(resp.Result)))

		return tran, err
	}

	tran.RefNumber = result[0].RefNum
	tran.Status = convertTranStatus(result[0].Status)

	return tran, nil
}

func (c *Client) CancelTransaction(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error) {
	var (
		l     = logger.FromCtx(ctx, "uzcard.CancelTransaction")
		resp  svGateResponse
		hold  Dismiss
		param HoldDismissParams
		req   svGateRequest
		url   = c.cfg.UzCardOpenAPIPath + svGateRoute
	)

	hold.HoldID = tran.ProcessingID
	hold.MerchantID = tran.PayTerminal.MerchantNum
	hold.TerminalID = tran.PayTerminal.TerminalNum
	hold.Amount = cast.ToFloat64(tran.Amount)

	param.Hold = hold

	req.Jsonrpc = jsonRPC
	req.ID = defaultID
	req.Method = holdDismiss
	req.Params = param
	if err := c.httpClient.Post(ctx, &resp, url, req, nil); err != nil {
		l.Error("c.httpClient.Post failed", zap.Error(err),
			zap.String("raw-params", req.Raw()))

		return tran, err
	}

	/*
		Статус холдирования:
		0 – деньги холдированы
		1 – деньги снят, или истек срок холдирования
	*/
	if resp.Error.Code != 0 {
		l.Error("resp.Error.Code failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", resp.Raw()))

		return tran, errors.New(resp.Error.Message)
	}

	var result HoldCreateResp
	if err := resp.Scan(&result); err != nil {
		l.Error("resp.Scan failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", string(resp.Result)))

		return tran, err
	}

	if result.Status != 0 {
		l.Error("result.Status != 0 failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", string(resp.Result)))

		return tran, errors.New(result.Description)
	}

	tran.ProcessingID = cast.ToString(result.ID)
	tran.Status = convertHoldStatus(result.Status)

	return tran, nil
}

// Variant 1
func (c *Client) ReversalTransactionV1(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error) {
	var (
		l     = logger.FromCtx(ctx, "uzcard.ReversalTransactionV1")
		resp  svGateResponse
		param SvTranReverseV1Params
		req   svGateRequest
		url   = c.cfg.UzCardOpenAPIPath + svGateRoute
	)

	param.TranID = tran.RefNumber

	req.Jsonrpc = jsonRPC
	req.ID = defaultID
	req.Method = transReverse
	req.Params = param
	if err := c.httpClient.Post(ctx, &resp, url, req, nil); err != nil {
		l.Error("c.httpClient.Post failed", zap.Error(err),
			zap.String("raw-params", req.Raw()))

		return tran, err
	}

	if resp.Error.Code != 0 {
		l.Error("resp.Error.Code failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", resp.Raw()))

		return tran, errors.New(resp.Error.Message)
	}

	var result []SvTransPayResp
	if err := resp.Scan(&result); err != nil {
		l.Error("resp.Scan failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", string(resp.Result)))

		return tran, err
	}

	if err := checkSvTranRev(result[0]); err != nil {
		l.Error("checkSvTranRev(result[0]) failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", string(resp.Result)))

		return tran, err
	}

	tran.Status = convertTranStatus(result[0].Status)

	return tran, nil
}

// Variant 2
func (c *Client) ReversalPartial(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error) {
	var (
		l     = logger.FromCtx(ctx, "uzcard.ReversalPartial")
		resp  svGateResponse
		pTran Tran
		param svTranReverseV2Params
		req   svGateRequest
		url   = c.cfg.UzCardOpenAPIPath + svGateRoute
	)
	pTran.TranID = tran.RefNumber
	pTran.ReverseAmount = cast.ToFloat64(tran.Amount)
	pTran.Ext = tran.ExternalID

	param.Tran = pTran

	req.Jsonrpc = jsonRPC
	req.ID = defaultID
	req.Method = transReversePartial
	req.Params = param
	if err := c.httpClient.Post(ctx, &resp, url, req, nil); err != nil {
		l.Error("c.httpClient.Post failed", zap.Error(err),
			zap.String("raw-params", req.Raw()))

		return tran, err
	}

	if resp.Error.Code != 0 {
		l.Error("resp.Error.Code failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", resp.Raw()))

		return tran, errors.New(resp.Error.Message)
	}

	var result SvTransPayResp
	if err := resp.Scan(&result); err != nil {
		l.Error("resp.Scan failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", string(resp.Result)))

		return tran, err
	}

	if err := checkSvTran(result); err != nil {
		l.Error("checkSvTran(result) failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", string(resp.Result)))

		return tran, err
	}

	tran.Status = convertTranStatus(result.Status)

	return tran, nil
}

func (c *Client) ReversalToCard(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error) {
	var (
		l    = logger.FromCtx(ctx, "uzcard.ReversalToCard")
		resp svGateResponse
		url  = c.cfg.UzCardOpenAPIPath + svGateRoute
	)
	// shuni aniqlashtirib olish kerak
	doc := Doc{
		Nationality:  "",
		Type:         "",
		SeriesNumber: "",
		BirthDate:    "",
		ValidTo:      "",
		Mrz:          "",
	}
	sender := Sender{
		System:     "",
		LegalName:  "",
		LastName:   "",
		FirstName:  "",
		MiddleName: "",
		ID:         "",
		RefNum:     "",
		Doc:        doc,
	}
	svCredit := SvGateCredit{
		Ext:        tran.ExternalID,
		Amount:     int(tran.Amount),
		MerchantID: tran.PayTerminal.MerchantNum,
		TerminalID: tran.PayTerminal.TerminalNum,
		Recipient:  "",
		Sender:     sender,
	}
	param := SvGateNinetyDaysReversal{
		Credit: svCredit,
	}

	req := svGateRequest{
		Jsonrpc: jsonRPC,
		ID:      defaultID,
		Method:  p2pUniversalCredit,
		Params:  param,
	}

	if err := c.httpClient.Post(ctx, &resp, url, req, nil); err != nil {
		l.Error("c.httpClient.Post failed", zap.Error(err),
			zap.String("raw-params", req.Raw()))

		return tran, err
	}

	if resp.Error.Code != 0 {
		l.Error("resp.Error.Code failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", resp.Raw()))

		return tran, errors.New(resp.Error.Message)
	}

	var result SvTransPayResp
	if err := resp.Scan(&result); err != nil {
		l.Error("resp.Scan failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", string(resp.Result)))

		return tran, err
	}
	if err := checkSvTran(result); err != nil {
		l.Error("checkSvTran(result[0]) failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", string(resp.Result)))

		return tran, err
	}

	tran.Status = convertTranStatus(result.Status)

	return tran, nil
}

func (c *Client) AuthTransaction(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error) {
	var (
		l     = logger.FromCtx(ctx, "uzcard.AuthTransaction")
		resp  svGateResponse
		hd    hold
		param holdCreateParam
		req   svGateRequest
	)
	hd.CardID = tran.PayCard.ID
	hd.MerchantID = tran.PayTerminal.MerchantNum
	hd.TerminalID = tran.PayTerminal.TerminalNum
	hd.Amount = cast.ToFloat64(tran.Amount)
	hd.Time = 4320

	param.Hold = hd

	req.Jsonrpc = jsonRPC
	req.ID = defaultID
	req.Method = holdCreate
	req.Params = param
	if err := c.httpClient.Post(ctx, &resp, c.cfg.UzCardOpenAPIPath+svGateRoute, req, nil); err != nil {
		l.Error("c.httpClient.Post failed", zap.Error(err),
			zap.String("raw-params", req.Raw()))

		return tran, err
	}
	/*
		Статус холдирования:
		0 – деньги холдированы
		1 – деньги снят, или истек срок холдирования
	*/

	if resp.Error.Code != 0 {
		l.Error("resp.Error.Code failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", resp.Raw()))

		return tran, errors.New(resp.Error.Message)
	}

	var result HoldCreateResp
	if err := resp.Scan(&result); err != nil {
		l.Error("resp.Scan failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", string(resp.Result)))

		return tran, err
	}
	if result.Status != 0 {
		l.Error("result.Status != 0 failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", string(resp.Result)))

		return tran, errs.ErrorWithAuthTransaction
	}

	tran.ProcessingID = strconv.FormatInt(result.ID, Base10)
	tran.Status = convertHoldStatus(result.Status)

	return tran, nil
}

func (c *Client) ConfirmTransaction(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error) {
	var (
		l    = logger.FromCtx(ctx, "uzcard.ConfirmTransaction")
		resp svGateResponse
	)
	holdID, err := strconv.ParseInt(tran.ProcessingID, Base10, BitSize64)
	if err != nil {
		l.Error("strconv.ParseInt(tran.ProcessingID, Base10, BitSize64) failed",
			zap.String("req-params", tran.ProcessingID))

		return tran, err
	}

	var (
		t     = time.Now()
		tr    holdCharge
		param holdChargeParams
		req   svGateRequest
		url   = c.cfg.UzCardOpenAPIPath + svGateRoute
	)
	tr.HoldID = holdID
	tr.Ext = tran.ExternalID
	tr.MerchantID = tran.PayTerminal.MerchantNum
	tr.Port = strconv.Itoa(tran.PayTerminal.Port)
	tr.TerminalID = tran.PayTerminal.TerminalNum
	tr.Stan = tools.RandomDigitsString(UzCardStanSize) // shuni to'ldirish kerak
	tr.Amount = float64(tran.Amount)
	tr.Date12 = t.Format("060102150405")

	param.Hold = tr

	req.Jsonrpc = jsonRPC
	req.ID = defaultID
	req.Method = holdDismissCharge
	req.Params = param

	if err := c.httpClient.Post(ctx, &resp, url, req, nil); err != nil {
		l.Error("c.httpClient.Post failed", zap.Error(err),
			zap.String("raw-params", req.Raw()))

		return tran, err
	}

	if resp.Error.Code != 0 {
		l.Error("resp.Error.Code failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", resp.Raw()))

		return tran, errors.New(resp.Error.Message)
	}

	var result SvTransPayResp
	if err = resp.Scan(&result); err != nil {
		l.Error("resp.Scan failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", string(resp.Result)))

		return tran, err
	}

	if err = checkSvTran(result); err != nil {
		l.Error("checkSvTran(result) failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", string(resp.Result)))

		return tran, err
	}

	tran.RefNumber = result.RefNum
	tran.Status = convertTranStatus(result.Status)

	return tran, nil
}

func (c *Client) GetCardByPan(ctx context.Context, number, expire string) (*domains.Card, error) {
	var (
		l     = logger.FromCtx(ctx, "uzcard.GetCardByPan")
		resp  svGateResponse
		card  Card
		param CardNewParam
		req   svGateRequest
		url   = c.cfg.UzCardOpenAPIPath + svGateRoute
	)
	card.Number = number
	card.Expiry = expire

	param.Card = card

	req.Jsonrpc = jsonRPC
	req.ID = defaultID
	req.Method = cardNew
	req.Params = param
	if err := c.httpClient.Post(ctx, &resp, url, req, nil); err != nil {
		l.Error("c.httpClient.Post failed", zap.Error(err),
			zap.String("raw-params", req.Raw()))

		return nil, err
	}

	if resp.Error.Code != 0 {
		l.Error("resp.Error.Code failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", resp.Raw()))

		return nil, errors.New(resp.Error.Message)
	}

	var result CardNewResp
	if err := resp.Scan(&result); err != nil {
		l.Error("resp.Scan failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", string(resp.Result)))

		return nil, err
	}

	return &domains.Card{
		ID:      result.ID,
		Status:  convertCardStatus(result.Status),
		Phone:   result.Phone,
		Balance: float64(result.Balance),
	}, nil
}

func convertHoldStatus(status int) string {
	var hs string
	if status == 0 {
		hs = AuthStatusActive
	} else if status == 1 {
		hs = AuthStatusFinished // consts.AuthStatusCanceled
	}
	return hs
}

func convertTranStatus(status string) string {
	var ts string
	if status == "" {
		ts = AuthStatusActive
	} else if status == "OK" {
		ts = AuthStatusFinished // consts.AuthStatusCanceled
	} else if status == "ROK" {
		ts = AuthStatusCanceled
	} else if status == "ERR" {
		ts = AuthStatusError
	}
	return ts
}

func checkSvTran(tran SvTransPayResp) error {
	if tran.Status != statusOK {
		if tran.RespSV == "33" {
			return errs.ErrCardExpired
		}
		return errors.New(tran.RespText)
	}
	if tran.RespSV != "00" {
		return errors.New(tran.RespText)
	}
	return nil
}

func checkSvTranRev(tran SvTransPayResp) error {
	if tran.Status != statusROK {
		return errors.New(tran.RespText)
	}

	if tran.RespSV != "00" {
		return errors.New(tran.RespText)
	}

	return nil
}

func convertCardStatus(status int) string {
	var s string
	switch status {
	case 0:
		s = domains.CardStatusActive
	default:
		s = domains.CardStatusNoActive
	}
	return s
}
