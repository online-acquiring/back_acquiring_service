package humo

import (
	"encoding/json"
	"fmt"
)

// Request
type DmsAuthParams struct {
	Language    string `json:"language"`
	BillerRef   string `json:"billerRef"`
	PayinstrRef string `json:"payinstrRef"`
	SessionID   string `json:"sessionID"`
	PaymentRef  string `json:"paymentRef"`
	Details     struct {
		Item []struct {
			Name  string `json:"name"`
			Value string `json:"value"`
		} `json:"item"`
	} `json:"details"`
	PaymentOriginator string `json:"paymentOriginator"`
}
type RequestHumo struct {
	Method string      `json:"method"`
	Params interface{} `json:"params"`
}

func (h *RequestHumo) Raw() string {
	raw, _ := json.Marshal(h)
	return string(raw)
}

type ConfirmParams struct {
	PaymentID         string `json:"paymentID"`
	PaymentRef        string `json:"paymentRef"`
	Confirmed         string `json:"confirmed"`
	Finished          string `json:"finished"`
	PaymentOriginator string `json:"paymentOriginator"`
}

type PaymentIDHumo struct {
	PaymentID string `json:"paymentID"`
}

type PaymentRefHumo struct {
	PaymentRef string `json:"paymentRef"`
}

type RecoParams struct {
	BillerRef   string `json:"billerRef"`
	PayinstrRef string `json:"payinstrRef"`
	Details     struct {
		Item []struct {
			Name  string `json:"name"`
			Value string `json:"value"`
		} `json:"item"`
	} `json:"details"`
	PaymentOriginator string `json:"paymentOriginator"`
}

type RespHumoOld struct {
	MaskedPan string `json:"masked_pan,omitempty"`
	Pan       string `json:"pan,omitempty"`
	Message   string `json:"message,omitempty"`
	Error     string `json:"error,omitempty"`
}

func (r RespHumoOld) ToError() error {
	return nil
}

func (r RespHumoOld) Scan(i interface{}) error {
	return nil
}

type Item struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}
type Detail struct {
	Item []struct {
		Name  string `json:"name"`
		Value string `json:"value"`
	} `json:"item"`
}
type ParamHumo struct {
	ID     string     `json:"id"`
	Params PhoneParam `json:"params"`
}

type PhoneParam struct {
	Phone  string `json:"phone"`
	BankID string `json:"bankId"`
}

type GetBalanceHumo struct {
	ID     string           `json:"id"`
	Params BalanceParamHumo `json:"params"`
}
type BalanceParamHumo struct {
	PrimaryAccountNumber string `json:"primaryAccountNumber"`
	MgFlag               string `json:"mb_flag"`
}

// Response
type RespHumo struct {
	Envelope struct {
		Body struct {
			Fault          *Fault          `json:"Fault"`
			Payment        json.RawMessage `json:"PaymentResponse"`
			ReqResponse    json.RawMessage `json:"RequestResponse"`
			GetPayment     json.RawMessage `json:"GetPaymentResponse"`
			GetCardBalance json.RawMessage `json:"getCardAccountsBalanceResponse"`
		} `json:"Body"`
	} `json:"Envelope"`
}

func (h *RespHumo) Raw() string {
	raw, _ := json.Marshal(h)
	return string(raw)
}

type Fault struct {
	Faultcode   string      `json:"faultcode"`
	Faultstring interface{} `json:"faultstring"`
	Detail      struct {
		PaymentServerException struct {
			Provider    string      `json:"provider"`
			Error       interface{} `json:"error"`
			Description interface{} `json:"description"`
			Screen      string      `json:"screen"`
		} `json:"PaymentServerException"`
	} `json:"detail"`
}

func (h *RespHumo) ToError() error {
	return nil
}

func (h *RespHumo) Scan(model interface{}) error {
	err := json.Unmarshal(h.Envelope.Body.Payment, &model)
	if err != nil {
		return err
	}
	return nil
}

func (h *RespHumo) ScanReq(model interface{}) error {
	err := json.Unmarshal(h.Envelope.Body.ReqResponse, &model)
	if err != nil {
		return err
	}
	return nil
}

func (h *RespHumo) ScanGet(model interface{}) error {
	err := json.Unmarshal(h.Envelope.Body.GetPayment, &model)
	if err != nil {
		return err
	}
	return nil
}
func (h *RespHumo) ScanGetCard(model interface{}) error {
	err := json.Unmarshal(h.Envelope.Body.GetCardBalance, &model)
	if err != nil {
		return err
	}
	return nil
}

//lint:ignore ST1016 we want to make sure that no two results of errors.New are ever the same
func (h *RespHumo) CheckError() *ErrHumo {
	if h.Envelope.Body.Fault != nil {
		return &ErrHumo{
			ErrorNote: h.Envelope.Body.Fault.Faultcode,
			ErrorCode: fmt.Sprintf("%v", h.Envelope.Body.Fault.Faultstring),
		}
	}

	return nil
}

type ErrHumo struct {
	ErrorCode string
	ErrorNote string
}

type DmsAuthResp struct {
	PaymentID  int    `json:"paymentID"`
	PaymentRef string `json:"paymentRef"`
	Details    struct {
		Item []struct {
			Name  string      `json:"name"`
			Value interface{} `json:"value"`
		} `json:"item"`
	} `json:"details"`
	Action int `json:"action"`
}

func (h *DmsAuthResp) Raw() string {
	raw, _ := json.Marshal(h)

	return string(raw)
}

type ConfirmResp struct {
	PaymentID  int    `json:"paymentID"`
	PaymentRef string `json:"paymentRef"`
	Details    struct {
		Item []struct {
			Name  string      `json:"name"`
			Value interface{} `json:"value"`
		} `json:"item"`
	} `json:"details"`
	Action int `json:"action"`
}

func (h *ConfirmResp) Raw() string {
	raw, _ := json.Marshal(h)
	return string(raw)
}

type PaymentResp struct {
	Status     string `json:"status"`
	PaymentID  string `json:"paymentID"`
	PaymentRef string `json:"paymentRef"`
	Details    struct {
		Item []struct {
			Name  string `json:"name"`
			Value string `json:"value"`
		} `json:"item"`
	} `json:"details"`
}

func (h *PaymentResp) Raw() string {
	raw, _ := json.Marshal(h)
	return string(raw)
}

type RecoResp struct {
	PaymentID string      `json:"paymentID"`
	Action    interface{} `json:"action"`
	Details   struct {
		Item []struct {
			Name  string `json:"name"`
			Value string `json:"value"`
		} `json:"item"`
	} `json:"details"`
}

func (h *RecoResp) Raw() string {
	raw, _ := json.Marshal(h)
	return string(raw)
}

type ReturnPayParamsV1 struct {
	PaymentID  string `json:"paymentID"`
	SessionID  string `json:"sessionID"`
	PaymentRef string `json:"paymentRef"`
	Details    struct {
		Item []struct {
			Name  string `json:"name"`
			Value string `json:"value"`
		} `json:"item"`
	} `json:"details"`
	PaymentOriginator string `json:"paymentOriginator"`
}
type ReturnPayParamsV2 struct {
	BillerRef   string `json:"billerRef"`
	PayinstrRef string `json:"payinstrRef"`
	SessionID   string `json:"sessionID"`
	PaymentRef  string `json:"paymentRef"`
	Details     struct {
		Item []struct {
			Name  string `json:"name"`
			Value string `json:"value"`
		} `json:"item"`
	} `json:"details"`
	PaymentOriginator string `json:"paymentOriginator"`
}

type CancelPayParams struct {
	PaymentID         string `json:"paymentID"`
	PaymentRef        string `json:"paymentRef"`
	PaymentOriginator string `json:"paymentOriginator"`
}

type GetCardsRespOpenAPI struct {
	ID      string            `json:"id"`
	Error   *ErrorHumoOpenAPI `json:"error"`
	Results json.RawMessage   `json:"result"`
}

func (h *GetCardsRespOpenAPI) ToError() error {
	return nil
}

func (h *GetCardsRespOpenAPI) Scan(model interface{}) error {
	if err := json.Unmarshal(h.Results, &model); err != nil {
		return err
	}
	return nil
}
func (h *GetCardsRespOpenAPI) Raw() string {
	raw, _ := json.Marshal(h)

	return string(raw)
}

type ErrorHumoOpenAPI struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type CardsByPhoneResp struct {
	Customer []struct {
		CustomerID     string `json:"customerId"`
		BankID         string `json:"bankId"`
		CardholderName string `json:"cardholderName"`
		Card           []struct {
			State   string `json:"state"`
			Pan     string `json:"pan"`
			Expiry  string `json:"expiry"`
			Service []struct {
				ServiceID      string `json:"serviceID"`
				ServiceChannel string `json:"serviceChannel"`
			} `json:"Service"`
		} `json:"Card"`
	} `json:"Customer"`
}

type GetCardBalanceNewVerResp struct {
	ListSize int `json:"listSize"`
	Card     struct {
		PrimaryAccountNumber string      `json:"primaryAccountNumber"`
		CardholderID         interface{} `json:"cardholderId"`
		PinTryCount          int         `json:"pinTryCount"`
		Expiry               string      `json:"expiry"`
		Statuses             struct {
			Item []struct {
				Type       interface{} `json:"type"`
				ActionCode string      `json:"actionCode"`
			} `json:"item"`
		} `json:"statuses"`
	} `json:"card"`
	Account []struct {
	} `json:"account"`
	Balance struct {
		AvailableAmount int `json:"availableAmount"`
	} `json:"balance"`
	Mb struct {
		Phone string `json:"phone"`
	} `json:"mb"`
}

type respHumoOld struct {
	MaskedPan string `json:"masked_pan,omitempty"`
	Pan       string `json:"pan,omitempty"`
	Message   string `json:"message,omitempty"`
	Error     string `json:"error,omitempty"`
}

func (res *respHumoOld) ToError() error {
	return nil
}

func (res *respHumoOld) Scan(i interface{}) error {
	return nil
}

func (res *respHumoOld) Raw() string {
	raw, _ := json.Marshal(res)

	return string(raw)
}
