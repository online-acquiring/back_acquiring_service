package humo

import (
	"context"
	"crypto/tls"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/spf13/cast"

	"go.uber.org/zap"
)

type Client struct {
	httpClient remote.HTTPClientI
	cfg        config.Config
}

func (h *Client) GetCardID(ctx context.Context, number string) (*domains.Card, error) {
	// this method does not work for HUMO
	panic("this method does not work for HUMO")
}

func New(cfg config.Config) *Client {
	var cf tls.Config
	cf.InsecureSkipVerify = true //nolint
	client := remote.New(cfg.OpenAPI.Host, &http.Transport{
		TLSClientConfig: &cf,
	})

	client.SetMiddleware(synch.GetOpenAPIMiddleware(cfg))

	return &Client{
		httpClient: client,
		cfg:        cfg,
	}
}

func (h *Client) GetCardNumber(ctx context.Context, cardID string) (*domains.Card, error) {
	var (
		l       = logger.FromCtx(ctx, "humo.GetCardNumber")
		dboResp respHumoOld
		card    domains.Card
		url     = h.cfg.HumoOldOpenAPIPath + humoGetRealPan
	)

	param := map[string]string{}
	param["masked_pan"] = cardID

	// add 15 sec timeout on request
	ctxT, cancel := context.WithTimeout(ctx, time.Second*defaultTimeOut)
	defer cancel()

	err := h.httpClient.Get(ctxT, &dboResp, url, param, nil)
	if err != nil {
		l.Error("h.httpClient.Get failed",
			zap.Error(err),
			zap.Any("raw-params", param),
			zap.String("resp-params", dboResp.Raw()))

		return nil, err
	}

	if dboResp.Message != "" && dboResp.Pan == "" {
		l.Error("dboResp.Message != \"\" && dboResp.Pan == \"\"",
			zap.String("resp-params", dboResp.Raw()))

		return nil, errors.New(dboResp.Message)
	}

	card.Number = dboResp.Pan

	return &card, nil
}

func (h *Client) GetCardMaskedPAN(ctx context.Context, cardNumber string) (*domains.Card, error) {
	var (
		l       = logger.FromCtx(ctx, "humo.GetCardMaskedPAN")
		dboResp respHumoOld
		card    domains.Card
		url     = h.cfg.HumoOldOpenAPIPath + humoGetMaskedPan + "?pan=" + cardNumber
	)
	// add 15 sec timeout on request
	ctxT, cancel := context.WithTimeout(ctx, time.Second*defaultTimeOut)
	defer cancel()

	err := h.httpClient.Get(ctxT, &dboResp, url, nil, nil)
	if err != nil {
		l.Error("h.httpClient.Get",
			zap.Error(err))

		return nil, err
	}

	if dboResp.Message != "" && dboResp.MaskedPan == "" {
		l.Error("dboResp.Message != \"\" && dboResp.MaskedPan == \"\"",
			zap.String("resp-params", dboResp.Raw()))

		return nil, errors.New(dboResp.Message)
	}

	card.ID = dboResp.MaskedPan

	return &card, nil
}

func (h *Client) GetCardByPhone(ctx context.Context, phone string) ([]domains.Card, error) {
	var (
		l        = logger.FromCtx(ctx, "humo.GetCardByPhone")
		resp     GetCardsRespOpenAPI
		param    PhoneParam
		reqParam ParamHumo
		url      = h.cfg.HumoMwOpenAPI + humoCustomerList
	)

	param.Phone = "+" + phone
	param.BankID = bankID

	reqParam.ID = tools.MakeUUID()
	reqParam.Params = param

	// add 15 sec timeout on request
	ctxT, cancel := context.WithTimeout(ctx, time.Second*rSTime)
	defer cancel()

	err := h.httpClient.Post(ctxT, &resp, url, reqParam, nil)
	if err != nil {
		l.Error("h.httpClient.Post failed", zap.Error(err),
			zap.String("raw-params", cast.ToString(param)))

		return nil, err
	}

	if resp.Error != nil {
		l.Error("resp.Error != nil failed",
			zap.String("raw-params", cast.ToString(param)),
			zap.String("resp-params", resp.Raw()))

		return nil, errors.New(resp.Error.Message)
	}

	var result CardsByPhoneResp
	if err = resp.Scan(&result); err != nil {
		l.Error("resp.Scan failed", zap.Error(err),
			zap.String("raw-params", cast.ToString(param)),
			zap.String("resp-params", resp.Raw()))

		return nil, err
	}

	var cards []domains.Card
	if len(result.Customer) != 0 {
		cardResp := result.Customer
		for i := 0; i < len(cardResp); i++ {
			if cardResp[i].Card != nil {
				for j := 0; j < len(cardResp[i].Card); j++ {
					var card = domains.Card{
						ID:     cardResp[i].CustomerID,
						Status: cardResp[i].Card[j].State,
						Phone:  phone,
						Number: cardResp[i].Card[j].Pan,
						Expiry: cardResp[i].Card[j].Expiry,
						Owner:  cardResp[i].CardholderName,
					}

					cards = append(cards, card)
				}
			}
		}
	}

	return cards, nil
}

func (h *Client) GetCardByPan(ctx context.Context, number, expire string) (*domains.Card, error) {
	var (
		l        = logger.FromCtx(ctx, "humo.GetCardByPan")
		resp     GetCardsRespOpenAPI
		param    BalanceParamHumo
		reqParam GetBalanceHumo
		url      = h.cfg.HumoMwOpenAPI + humoIIACCard // humomw/v1
	)
	param.PrimaryAccountNumber = number
	param.MgFlag = mgFlag
	reqParam.ID = tools.MakeUUID()
	reqParam.Params = param

	// add 15 sec timeout on request
	ctxT, cancel := context.WithTimeout(ctx, time.Second*rSTime)
	defer cancel()

	err := h.httpClient.Post(ctxT, &resp, url, reqParam, nil)
	if err != nil {
		l.Error("h.httpClient.Post failed", zap.Error(err),
			zap.String("raw-params", cast.ToString(param)),
			zap.String("resp-params", resp.Raw()))

		return nil, err
	}

	if resp.Error != nil {
		l.Error("resp.Error != nil failed",
			zap.String("raw-params", cast.ToString(param)),
			zap.String("resp-params", resp.Raw()))

		return nil, errors.New(resp.Error.Message)
	}

	var result GetCardBalanceNewVerResp
	if err = resp.Scan(&result); err != nil {
		l.Error("resp.Scan failed", zap.Error(err),
			zap.String("raw-params", cast.ToString(param)),
			zap.String("resp-params", resp.Raw()))

		return nil, err
	}

	card := &domains.Card{
		ID:      number,
		Phone:   result.Mb.Phone,
		Balance: 0,
	}
	for _, v := range result.Card.Statuses.Item {
		if v.Type.(string) == cardType {
			card.Status = convertHumoCardStatus(v.ActionCode)
			break
		}
	}

	return card, nil
}

func (h *Client) GetCardBalance(ctx context.Context, number string) (*domains.Card, error) {
	var (
		l        = logger.FromCtx(ctx, "humo.GetCardBalance")
		resp     GetCardsRespOpenAPI
		param    BalanceParamHumo
		reqParam GetBalanceHumo
		url      = h.cfg.HumoMwOpenAPI + humoIIACCard
	)
	param.PrimaryAccountNumber = number
	param.MgFlag = mgFlag
	reqParam.ID = tools.MakeUUID()
	reqParam.Params = param

	// add 15 sec timeout on request
	ctxT, cancel := context.WithTimeout(ctx, time.Second*rSTime)
	defer cancel()

	err := h.httpClient.Post(ctxT, &resp, url, reqParam, nil)
	if err != nil {
		l.Error("h.httpClient.Post failed", zap.Error(err),
			zap.String("raw-params", cast.ToString(param)),
			zap.String("resp-params", resp.Raw()))

		return nil, err
	}

	if resp.Error != nil {
		l.Error("resp.Error != nil failed",
			zap.String("raw-params", cast.ToString(param)),
			zap.String("resp-params", resp.Raw()))

		return nil, errors.New(resp.Error.Message)
	}

	var result GetCardBalanceNewVerResp
	if err = resp.Scan(&result); err != nil {
		l.Error("resp.Scan failed", zap.Error(err),
			zap.Any("raw-params", param),
			zap.String("resp-params", resp.Raw()))

		return nil, err
	}

	card := &domains.Card{
		ID:      result.Card.CardholderID.(string),
		Phone:   result.Mb.Phone,
		Number:  result.Card.PrimaryAccountNumber,
		Balance: float64(result.Balance.AvailableAmount),
		Expiry:  result.Card.Expiry[len(result.Card.Expiry)-2:len(result.Card.Expiry)] + result.Card.Expiry[:2],
	}

	if result.Card.PinTryCount == three {
		l.Error("result.Card.PinTryCount == three failed",
			zap.String("raw-params", strconv.Itoa(result.Card.PinTryCount)))

		return nil, errs.ErrCardIsBlocked
	}

	for _, v := range result.Card.Statuses.Item {
		if v.Type.(string) == cardType {
			card.Status = convertHumoCardStatus(v.ActionCode)
			break
		}
	}

	return card, nil
}

func (h *Client) AuthTransaction(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error) {
	var (
		l       = logger.FromCtx(ctx, "humo.AuthTransaction")
		resp    RespHumo
		details Detail
		param   DmsAuthParams
		req     RequestHumo
	)
	details.Item = append(details.Item,
		Item{Name: "pan", Value: tran.PayCard.Number},
		Item{Name: "expiry", Value: tran.PayCard.Expiry},
		Item{Name: "ccy_code", Value: domains.CurrencyCodeUZS},
		Item{Name: "amount", Value: fmt.Sprintf("%d", cast.ToInt(tran.Amount))},
		Item{Name: "merchant_id", Value: tran.PayTerminal.MerchantNum},
		Item{Name: "terminal_id", Value: tran.PayTerminal.TerminalNum},
		Item{Name: "point_code", Value: h.cfg.HumoPointCode},
		Item{Name: "centre_id", Value: h.cfg.HumoCentreID}, //nolint:misspell
	)
	param.Language = langEn
	param.BillerRef = soapDMS
	param.PayinstrRef = soapDMS
	param.SessionID = tools.RandomDigitsString(humoReqIDSize) // session id ni o'ylash kerak nima berishni
	param.PaymentRef = tran.ExternalID
	param.Details = details
	param.PaymentOriginator = h.cfg.HumoOrginator

	req.Method = humoPaymentMethod
	req.Params = param

	if err := h.httpClient.Post(ctx, &resp, h.cfg.HumoEndpoint, req, nil); err != nil {
		l.Error("h.httpClient.Post failed", zap.Error(err),
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", resp.Raw()))

		return tran, err
	}

	if err := resp.CheckError(); err != nil {
		l.Error("resp.CheckError() failed", zap.Error(errors.New(err.ErrorNote)),
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", resp.Raw()))

		return tran, errors.New("errorCode: " + err.ErrorCode + " - " + err.ErrorNote)
	}

	var result DmsAuthResp
	if err := resp.Scan(&result); err != nil {
		l.Error("resp.Scan failed", zap.Error(err),
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", result.Raw()))

		return tran, err
	}

	// caselarni qarab ketish kerak
	if result.Action != authSuccessAction {
		l.Error("result.Action != authSuccessAction failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", result.Raw()))

		return tran, errors.New("errorAction code: " + strconv.Itoa(result.Action))
	}

	for _, item := range result.Details.Item {
		if item.Name == fieldNameRRN {
			tran.RefNumber = fmt.Sprintf("%v", item.Value)
			break
		}
	}

	tran.ProcessingID = strconv.Itoa(result.PaymentID)

	return tran, nil
}
func (h *Client) ConfirmTransaction(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error) {
	var (
		l     = logger.FromCtx(ctx, "humo.ConfirmTransaction")
		resp  RespHumo
		param ConfirmParams
		req   RequestHumo
	)
	param.PaymentID = tran.ProcessingID
	param.PaymentRef = tran.ExternalID
	param.Confirmed = confirmed
	param.Finished = finished
	param.PaymentOriginator = h.cfg.HumoOrginator

	req.Method = humoPaymentMethod
	req.Params = param
	if err := h.httpClient.Post(ctx, &resp, h.cfg.HumoEndpoint, req, nil); err != nil {
		l.Error("h.httpClient.Post failed", zap.Error(err),
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", resp.Raw()))

		return tran, err
	}

	if err := resp.CheckError(); err != nil {
		l.Error("resp.CheckError() failed", zap.Error(errors.New(err.ErrorNote)),
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", resp.Raw()))

		return tran, errors.New("errorCode: " + err.ErrorCode + " - " + err.ErrorNote)
	}

	var result ConfirmResp
	if err := resp.Scan(&result); err != nil {
		l.Error("resp.ScanReq failed", zap.Error(err),
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", result.Raw()))

		return tran, err
	}

	// caselarni qarab ketish kerak
	if result.Action != confirmSuccessAction {
		l.Error("result.Action != confirmSuccessAction failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", result.Raw()))

		return tran, errors.New("errorAction code:" + strconv.Itoa(result.Action))
	}

	tran.ProcessingID = strconv.Itoa(result.PaymentID)
	for _, item := range result.Details.Item {
		if item.Name == fieldNameRRN {
			tran.RefNumber = fmt.Sprintf("%v", item.Value)
			break
		}
	}

	return tran, nil
}
func (h *Client) GetTransactionByExt(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error) {
	var (
		l     = logger.FromCtx(ctx, "humo.GetTransactionByExt")
		resp  RespHumo
		param PaymentRefHumo
		req   RequestHumo
	)
	param.PaymentRef = tran.ExternalID

	req.Method = humoGetPaymentMethod
	req.Params = param

	if err := h.httpClient.Post(ctx, &resp, h.cfg.HumoEndpoint, req, nil); err != nil {
		l.Error("h.httpClient.Post failed", zap.Error(err),
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", resp.Raw()))

		return tran, err
	}

	if err := resp.CheckError(); err != nil {
		l.Error("resp.CheckError() failed", zap.Error(errors.New(err.ErrorNote)),
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", resp.Raw()))

		return tran, errors.New("errorCode: " + err.ErrorCode + " - " + err.ErrorNote)
	}

	var result PaymentResp

	if err := resp.ScanGet(&result); err != nil {
		l.Error("resp.ScanReq failed", zap.Error(err),
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", result.Raw()))

		return tran, err
	}

	tran.Status = result.Status
	tran.ExternalID = result.PaymentRef
	tran.ProcessingID = result.PaymentID

	return tran, nil
}

func (h *Client) CancelTransaction(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error) {
	var (
		l     = logger.FromCtx(ctx, "humo.CancelTransaction")
		resp  RespHumo
		param CancelPayParams
		req   RequestHumo
	)

	param.PaymentID = tran.ProcessingID
	param.PaymentRef = tran.RefNumber
	param.PaymentOriginator = h.cfg.HumoOrginator
	req.Method = humoCancelRequestMethod
	req.Params = param

	if err := h.httpClient.Post(ctx, &resp, h.cfg.HumoEndpoint, req, nil); err != nil {
		l.Error("h.httpClient.Post failed", zap.Error(err),
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", resp.Raw()))

		return tran, err
	}

	if err := resp.CheckError(); err != nil {
		l.Error("resp.CheckError() failed", zap.Error(errors.New(err.ErrorNote)),
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", resp.Raw()))

		return tran, errors.New("errorCode: " + err.ErrorCode + " - " + err.ErrorNote)
	}

	return tran, nil
}

func (h *Client) ReversalTransactionV1(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error) {
	var (
		l       = logger.FromCtx(ctx, "humo.ReversalTransactionV1")
		details Detail
		resp    RespHumo
		param   ReturnPayParamsV1
		req     RequestHumo
	)
	details.Item = append(details.Item,
		Item{Name: "merchant_id", Value: tran.PayTerminal.MerchantNum},
		Item{Name: "terminal_id", Value: tran.PayTerminal.TerminalNum},
		Item{Name: "centre_id", Value: h.cfg.HumoCentreID}, //nolint:misspell
	)
	param.PaymentID = tran.ProcessingID
	param.SessionID = ""
	param.PaymentRef = tran.ExternalID
	param.Details = details
	param.PaymentOriginator = h.cfg.HumoOrginator

	req.Method = humoReturnPaymentMethod
	req.Params = param
	if err := h.httpClient.Post(ctx, &resp, h.cfg.HumoEndpoint, req, nil); err != nil {
		l.Error("h.httpClient.Post failed", zap.Error(err),
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", resp.Raw()))

		return tran, err
	}

	if err := resp.CheckError(); err != nil {
		l.Error("resp.CheckError() failed", zap.Error(errors.New(err.ErrorNote)),
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", resp.Raw()))

		return tran, errors.New("errorCode: " + err.ErrorCode + " - " + err.ErrorNote)
	}

	return tran, nil
}

func (h *Client) ToCard(ctx context.Context, tran domains.PSTransaction) (domains.PSTransaction, error) {
	var (
		l       = logger.FromCtx(ctx, "humo.ToCard")
		details Detail
		resp    RespHumo
		param   ReturnPayParamsV2
		req     RequestHumo
	)

	details.Item = append(details.Item,
		Item{Name: "pan2", Value: tran.PayCard.Number},
		Item{Name: "expiry", Value: tran.PayCard.Expiry},
		Item{Name: "ccy_code", Value: domains.CurrencyCodeUZS}, // aniqlik kiritilishi kerak
		Item{Name: "amount", Value: strconv.FormatInt(tran.Amount, 10)},
		Item{Name: "merchant_id", Value: tran.PayTerminal.MerchantNum},
		Item{Name: "terminal_id", Value: tran.PayTerminal.TerminalNum},
		Item{Name: "point_code", Value: h.cfg.HumoPointCode}, // aniqlik kiritilishi kerak
		Item{Name: "centre_id", Value: h.cfg.HumoCentreID},   //nolint:misspell
		Item{Name: "sender_nationality", Value: tran.PayDetail.Nationality},
		Item{Name: "sender_doc_type", Value: tran.PayDetail.DocType},
		Item{Name: "sender_serial_no", Value: tran.PayDetail.SerialNo},
		Item{Name: "sender_id_card", Value: tran.PayDetail.IDCard},
		Item{Name: "sender_doc_validthrough", Value: tran.PayDetail.DocValidThrough}, // details tablitsadan olish kerak
		Item{Name: "sender_person_code", Value: tran.PayDetail.PersonCode},           // details tablitsadan olish kerak
		Item{Name: "sender_surname", Value: tran.PayDetail.Surname},                  // details tablitsadan olish kerak
		Item{Name: "sender_first_name", Value: tran.PayDetail.FirstName},             // details tablitsadan olish kerak
		Item{Name: "sender_middle_name", Value: tran.PayDetail.MiddleName},           // details tablitsadan olish kerak
		Item{Name: "sender_birth_date", Value: tran.PayDetail.BirthDate},             // details tablitsadan olish kerak
	)
	param.BillerRef = soapToCard2
	param.PayinstrRef = soapToCard2
	param.SessionID = tran.RefNumber
	param.PaymentRef = tran.RefNumber
	param.Details = details
	param.PaymentOriginator = h.cfg.HumoOrginator

	req.Method = humoPaymentMethod
	req.Params = param
	if err := h.httpClient.Post(ctx, &resp, h.cfg.HumoEndpoint, req, nil); err != nil {
		l.Error("h.httpClient.Post failed", zap.Error(err),
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", resp.Raw()))

		return tran, err
	}

	if err := resp.CheckError(); err != nil {
		l.Error("resp.CheckError() failed", zap.Error(errors.New(err.ErrorNote)),
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", resp.Raw()))

		return tran, errors.New("errorCode: " + err.ErrorCode + " - " + err.ErrorNote)
	}

	return tran, nil
}

func (h *Client) Reconciliation(ctx context.Context, terminal domains.Terminal) (domains.Reconciliation, error) {
	var (
		l       = logger.FromCtx(ctx, "humo.ToCard")
		details Detail
		resp    RespHumo
		reco    domains.Reconciliation
		param   RecoParams
		req     RequestHumo
	)

	details.Item = append(details.Item, Item{Name: "terminal_id", Value: terminal.TerminalNum})

	param.BillerRef = soapReco
	param.PayinstrRef = soapReco
	param.Details = details
	param.PaymentOriginator = h.cfg.HumoOrginator

	req.Method = humoPaymentMethod
	req.Params = param
	if err := h.httpClient.Post(ctx, &resp, h.cfg.HumoEndpoint, req, nil); err != nil {
		l.Error("h.httpClient.Post failed", zap.Error(err),
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", resp.Raw()))

		return reco, err
	}

	if err := resp.CheckError(); err != nil {
		l.Error("resp.CheckError() failed", zap.Error(errors.New(err.ErrorNote)),
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", resp.Raw()))

		return reco, errors.New("errorCode: " + err.ErrorCode + " - " + err.ErrorNote)
	}

	var result RecoResp
	if err := resp.Scan(&result); err != nil {
		l.Error("resp.Scan failed", zap.Error(err),
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", result.Raw()))

		return reco, err
	}

	reco.Action = strconv.Itoa(result.Action.(int))
	reco.PaymentID = result.PaymentID

	if result.Action != authSuccessAction {
		l.Error("result.Action != authSuccessAction failed",
			zap.String("raw-params", req.Raw()),
			zap.String("resp-params", result.Raw()))

		return reco, errors.New("auth action is wrong")
	}

	return reco, nil
}

func convertHumoCardStatus(status string) string {
	var s string
	switch status {
	case "000":
		s = domains.CardStatusActive
	default:
		s = domains.CardStatusNoActive
	}
	return s
}
