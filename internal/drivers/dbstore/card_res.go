package dbstore

import (
	"database/sql"
	"time"
)

type dbCardBin struct {
	PSCode    string `db:"ps_code"`
	MaskedPAN string `db:"masked_pan"`
	Expiry    string `db:"expiry"`
}

func (db *dbCardBin) toModel() (pCode domains.PSCode, mPan, exp string) {
	pCode = domains.PSCode(db.PSCode)
	mPan = db.MaskedPAN
	exp = db.Expiry

	return
}

type dbCard struct {
	ID        string         `db:"id"`
	MaskedPAN sql.NullString `db:"masked_pan"`
	BIN       string         `db:"bin"`
	Expiry    sql.NullString `db:"expiry"`
	CreatedAt time.Time      `db:"created_at"`
	UpdatedAt sql.NullTime   `db:"updated_at"`
}

func (db *dbCard) toModel() domains.PayCard {
	return domains.PayCard{
		ID:        db.ID,
		BIN:       db.BIN,
		MaskedPAN: db.MaskedPAN.String,
		Expiry:    db.Expiry.String,
	}
}

func fromModelCard(c domains.PayCard) dbCard {
	return dbCard{
		ID: c.ID,
		MaskedPAN: sql.NullString{
			Valid:  c.MaskedPAN != "",
			String: c.MaskedPAN,
		},
		BIN: c.BIN,
		Expiry: sql.NullString{
			String: c.Expiry,
			Valid:  c.Expiry != "",
		},
	}
}
