package dbstore

import (
	"database/sql"

	"github.com/spf13/cast"
)

type dbTerminal struct {
	ID          sql.NullString  `db:"id"`
	Name        sql.NullString  `db:"name"`
	PsCode      sql.NullString  `db:"ps_code"`
	TerminalNum sql.NullString  `db:"terminal_num"`
	MerchantNum sql.NullString  `db:"merchant_num"`
	Port        int             `db:"port"`
	TerminalAcc sql.NullString  `db:"terminal_acc"`
	Commission  sql.NullFloat64 `db:"com"`
}

func (db *dbTerminal) toModelTer() domains.Terminal {
	m := domains.Terminal{
		ID:          domains.ID(db.ID.String),
		Name:        db.Name.String,
		MerchantNum: db.MerchantNum.String,
		Port:        cast.ToInt(db.Port),
		PSCode:      domains.PSCode(db.PsCode.String),
		TerminalNum: db.TerminalNum.String,
		Com:         db.Commission.Float64,
	}
	m.TerminalAcc = append(m.TerminalAcc, db.TerminalAcc.String)
	return m
}

func fromModelToTerDB(t domains.Terminal) dbTerminal {
	return dbTerminal{
		ID: sql.NullString{
			String: string(t.ID),
			Valid:  string(t.ID) != "",
		},
		Name: sql.NullString{
			String: t.Name,
			Valid:  t.Name != "",
		},
		PsCode: sql.NullString{
			String: t.PSCode.ToString(),
			Valid:  t.PSCode.ToString() != "",
		},
		TerminalNum: sql.NullString{
			String: t.TerminalNum,
			Valid:  t.TerminalNum != "",
		},
		MerchantNum: sql.NullString{
			String: t.MerchantNum,
			Valid:  t.MerchantNum != "",
		},
		Port: t.Port,
		TerminalAcc: sql.NullString{
			String: t.TerminalAcc[0],
			Valid:  t.TerminalAcc[0] != "",
		},
		Commission: sql.NullFloat64{
			Float64: t.Com,
			Valid:   t.Com >= 0,
		},
	}
}
func collectTerminals(db []dbTerminal) []domains.Terminal {
	m := make([]domains.Terminal, len(db))
	for i := range db {
		m[i] = db[i].toModelTer()
	}
	return m
}

type dbTerminalAcc struct {
	ID         string       `json:"id"`
	TerminalID string       `json:"terminal_id"`
	AccountID  string       `json:"account_id"`
	Type       int          `json:"type"`
	State      int          `json:"state"`
	CreatedAt  sql.NullTime `json:"created_at"`
	UpdatedAt  sql.NullTime `json:"updated_at"`
}

// func (db *dbTerminalAcc) toModelTerAcc() domains.TerminalAccount {
//	m := domains.TerminalAccount{
//		ID:          domains.ID(db.ID),
//		AccountType: domains.AccType(db.Type),
//		Terminal: domains.Terminal{
//			ID: domains.ID(db.TerminalID),
//		},
//		Account: domains.Account{
//			ID: domains.ID(db.AccountID),
//		},
//		State: domains.State(db.State),
//	}
//	m.Dates.CreatedAt = tools.ParseDBTime(db.CreatedAt)
//	m.Dates.UpdatedAt = tools.ParseDBTime(db.UpdatedAt)
//	return m
// }

func fromModelToTerAccDB(t domains.TerminalAccount) dbTerminalAcc {
	t.Dates.UpdatedAt = IsNullReturnNow(t.Dates.UpdatedAt)
	return dbTerminalAcc{
		ID:         string(t.ID),
		TerminalID: string(t.Terminal.ID),
		AccountID:  string(t.Account.ID),
		Type:       int(t.AccountType),
		State:      int(t.State),
		CreatedAt: sql.NullTime{
			Time:  t.Dates.CreatedAt,
			Valid: true,
		},
		UpdatedAt: sql.NullTime{
			Time:  t.Dates.UpdatedAt,
			Valid: false,
		},
	}
}
