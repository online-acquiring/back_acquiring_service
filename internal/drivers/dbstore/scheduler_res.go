package dbstore

import (
	"database/sql"
)

// scheduler_res.go
type dbScheduler struct {
	ID         string       `db:"id"`
	Code       string       `db:"code"`
	Name       string       `db:"name"`
	Expression string       `db:"expression"`
	Status     int          `db:"status"`
	JobID      int          `db:"job_id"`
	CreatedAt  sql.NullTime `db:"created_at"`
	UpdatedAt  sql.NullTime `db:"updated_at"`
}

func (db *dbScheduler) toModelScheduler() domains.Scheduler {
	m := domains.Scheduler{
		ID:         domains.ID(db.ID),
		Code:       db.Code,
		Name:       db.Name,
		Expression: db.Expression,
		Status:     db.Status,
		JobID:      db.JobID,
	}
	m.CreateUpdateTime.CreatedAt = tools.ParseDBTime(db.CreatedAt)
	m.CreateUpdateTime.UpdatedAt = tools.ParseDBTime(db.UpdatedAt)
	return m
}

func fromModelToComDBScheduler(mr domains.Scheduler) dbScheduler {
	mr.CreateUpdateTime.UpdatedAt = IsNullReturnNow(mr.CreateUpdateTime.UpdatedAt)
	return dbScheduler{
		ID:         string(mr.ID),
		Code:       mr.Code,
		Name:       mr.Name,
		Expression: mr.Expression,
		Status:     mr.Status,
		JobID:      mr.JobID,
		CreatedAt: sql.NullTime{
			Time:  mr.CreateUpdateTime.CreatedAt,
			Valid: true,
		},
		UpdatedAt: sql.NullTime{
			Time:  mr.CreateUpdateTime.UpdatedAt,
			Valid: true,
		},
	}
}

func schedulerCollection(db []dbScheduler) []domains.Scheduler {
	data := make([]domains.Scheduler, len(db))
	for i := range db {
		data[i] = db[i].toModelScheduler()
	}
	return data
}
