package dbstore

import (
	"database/sql"
)

type dbCommission struct {
	ID         string       `db:"id"`
	MerchantID string       `db:"merchant_id"`
	State      int          `db:"state"`
	FeeType    int          `db:"fee_type"`
	Value      float64      `db:"value"`
	ComType    int          `db:"comm_type"`
	CreatedAt  sql.NullTime `db:"created_at"`
	UpdatedAt  sql.NullTime `db:"updated_at"`
}

func (db *dbCommission) toModel() domains.Commission {
	m := domains.Commission{
		ID:         domains.ID(db.ID),
		MerchantID: domains.ID(db.MerchantID),
		FeeType:    domains.Fee(db.FeeType),
		Value:      domains.ComValue(db.Value),
		ComType:    domains.ComType(db.ComType),
	}
	m.Dates.CreatedAt = tools.ParseDBTime(db.CreatedAt)
	m.Dates.UpdatedAt = tools.ParseDBTime(db.UpdatedAt)
	return m
}

func fromModelToComDB(mr domains.Commission) dbCommission {
	mr.Dates.UpdatedAt = IsNullReturnNow(mr.Dates.UpdatedAt)
	return dbCommission{
		ID:         string(mr.ID),
		MerchantID: string(mr.MerchantID),
		FeeType:    int(mr.FeeType),
		Value:      float64(mr.Value),
		ComType:    int(mr.ComType),
		CreatedAt: sql.NullTime{
			Time:  mr.Dates.CreatedAt,
			Valid: true,
		},
		UpdatedAt: sql.NullTime{
			Time:  mr.Dates.UpdatedAt,
			Valid: true,
		},
	}
}

func collectCommissions(db []dbCommission) []domains.Commission {
	m := make([]domains.Commission, len(db))
	for i := range db {
		m[i] = db[i].toModel()
	}
	return m
}
