package dbstore

import (
	"context"
	"database/sql"
	"errors"

	"go.uber.org/zap"
)

type TerminalRepo struct {
	store *DBStore
}

func (dbo *TerminalRepo) Create(ctx context.Context, ter domains.Terminal) (domains.Terminal, error) {
	var (
		l         = logger.FromCtx(ctx, "terminalRepo.Create")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		q         = `insert into terminals(name,ps_code,terminal_num,merchant_num,port,terminal_acc,com)
                     values($1,$2,$3,$4,$5,$6,$7) returning id`
		r = fromModelToTerDB(ter)
	)
	if err := sqlClient.GetContext(ctx, &ter.ID, q, r.Name,
		r.PsCode, r.TerminalNum, r.MerchantNum, r.Port, r.TerminalAcc, r.Commission); err != nil {
		l.Error("sqlClient.GetContext failed", zap.Error(err))

		return domains.Terminal{}, errs.Errf(errs.ErrSourceInternal, err.Error())
	}

	return ter, nil
}

func (dbo *TerminalRepo) GetAll(ctx context.Context) ([]domains.Terminal, error) {
	var (
		l         = logger.FromCtx(ctx, "terminalRepo.GetAll")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		record    []dbTerminal
		q         = `SELECT t.* FROM terminals t`
	)
	if err := sqlClient.SelectContext(ctx, &record, q); err != nil {
		l.Error("sqlClient.SelectContext failed", zap.Error(err))

		return nil, errs.Errf(errs.ErrSourceConnectionErr, err.Error())
	}

	return collectTerminals(record), nil
}

//nolint:dupl
func (dbo *TerminalRepo) Get(ctx context.Context, ter domains.Terminal) (domains.Terminal, error) {
	var (
		l         = logger.FromCtx(ctx, "terminalRepo.Get")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		record    = dbTerminal{}
		q         = `SELECT * FROM terminals where id = $1`
	)

	if err := sqlClient.Get(&record, q, ter.ID); err != nil {
		l.Error("sqlClient.Get failed", zap.Error(err))

		if errors.Is(err, sql.ErrNoRows) {
			return domains.Terminal{}, errs.Errf(errs.ErrNotFound, "Terminal not found. ID %v", ter.ID)
		}

		return domains.Terminal{}, errs.Errf(errs.ErrWrongInput, err.Error())
	}

	return record.toModelTer(), nil
}

func (dbo *TerminalRepo) UpdateTerminal(ctx context.Context, ter domains.Terminal) (domains.Terminal, error) {
	var (
		l         = logger.FromCtx(ctx, "terminalRepo.UpdateTerminal")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		q         = `UPDATE terminals SET name=$1,ps_code=$2,terminal_num=$3,merchant_num=$4,port=$5,terminal_acc=$6,com=$7
                     WHERE id=$8`
		r = fromModelToTerDB(ter)
	)
	result, err := sqlClient.Exec(q, r.Name, r.PsCode, r.TerminalNum, r.MerchantNum, r.Port, r.TerminalAcc, r.Commission, r.ID)
	if err != nil {
		l.Error("sqlClient.Exec failed", zap.Error(err))

		return domains.Terminal{}, errs.Errf(errs.ErrSourceInternal, err.Error())
	}

	if count, _ := result.RowsAffected(); count != 1 {
		l.Error("result.RowsAffected() failed", zap.Int64("count", count))

		return domains.Terminal{}, errs.Errf(errs.ErrNotFound, "no merchant account record updated")
	}

	return r.toModelTer(), nil
}

func (dbo *TerminalRepo) CreateTerminalAccount(ctx context.Context, terAcc domains.TerminalAccount) (domains.TerminalAccount, error) {
	var (
		l         = logger.FromCtx(ctx, "terminalRepo.CreateTerminalAccount")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		q         = `insert into terminal_accounts(terminal_id,account_id,type,state) 
         values($1,$2,$3,$4) returning id`
		r = fromModelToTerAccDB(terAcc)
	)
	err := sqlClient.GetContext(ctx, &terAcc.ID, q, r.TerminalID, r.AccountID, r.Type, r.State)
	if err != nil {
		l.Error("sqlClient.GetContext failed", zap.Error(err))

		return domains.TerminalAccount{}, errs.Errf(errs.ErrSourceInternal, err.Error())
	}

	return terAcc, nil
}

//nolint:dupl
func (dbo *TerminalRepo) GetTerminalByType(ctx context.Context, termType string) (domains.Terminal, error) {
	var (
		l         = logger.FromCtx(ctx, "terminalRepo.GetTerminalByType")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		record    = dbTerminal{}
		q         = `SELECT * FROM terminals c where ps_code = $1`
	)
	if err := sqlClient.Get(&record, q, termType); err != nil {
		l.Error("sqlClient.Get failed", zap.Error(err))

		if errors.Is(err, sql.ErrNoRows) {
			return domains.Terminal{}, errs.Errf(errs.ErrNotFound, "Terminal not found. type %v", termType)
		}

		return domains.Terminal{}, errs.Errf(errs.ErrSourceConnectionErr, err.Error())
	}

	return record.toModelTer(), nil
}
