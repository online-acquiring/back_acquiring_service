package dbstore

import (
	"database/sql"
	"time"
)

type dbCardBins struct {
	BIN       string         `db:"bin"`
	PSCode    sql.NullString `db:"ps_code"`
	CreatedAt time.Time      `db:"created_at"`
	UpdatedAt sql.NullTime   `db:"updated_at"`
}

func (db *dbCardBins) toModel() domains.Bin {
	return domains.Bin{
		Code:   db.BIN,
		PSCode: domains.PSCode(db.PSCode.String),
	}
}

func fromModelBin(bin domains.Bin) dbCardBins {
	return dbCardBins{
		BIN: bin.Code,
		PSCode: sql.NullString{
			String: bin.PSCode.ToString(),
			Valid:  bin.PSCode.ToString() != "",
		},
	}
}

func binCollection(db []dbCardBins) []domains.Bin {
	data := make([]domains.Bin, len(db))

	for i := range db {
		data[i] = domains.Bin{
			Code:   db[i].BIN,
			PSCode: domains.PSCode(db[i].PSCode.String),
		}
	}

	return data
}
