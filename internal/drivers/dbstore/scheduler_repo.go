package dbstore

import (
	"context"
	"database/sql"
	"errors"

	"go.uber.org/zap"
)

// scheduler_repo.go
type SchedulerRepo struct {
	store *DBStore
}

func (dbo *SchedulerRepo) GetAll(ctx context.Context) ([]domains.Scheduler, error) {
	var (
		l         = logger.FromCtx(ctx, "schedulerRepo.GetAll")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		record    []dbScheduler
		q         = `SELECT * FROM schedulers`
	)
	if err := sqlClient.SelectContext(ctx, &record, q); err != nil {
		l.Error("sqlClient.SelectContext failed", zap.Error(err))

		return nil, errs.Errf(errs.ErrSourceInternal, err.Error())
	}

	return schedulerCollection(record), nil
}

func (dbo *SchedulerRepo) Create(ctx context.Context, com domains.Scheduler) (domains.Scheduler, error) {
	var (
		l         = logger.FromCtx(ctx, "schedulerRepo.Create")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		q         = `INSERT INTO schedulers (code, name, expression, status, job_id)
			VALUES ($1, $2, $3, $4, $5) RETURNING id`
		r = fromModelToComDBScheduler(com)
	)
	err := sqlClient.GetContext(ctx, &com.ID, q, r.Code, r.Name, r.Expression, r.Status, r.JobID)
	if err != nil {
		l.Error("sqlClient.GetContext failed", zap.Error(err))

		return domains.Scheduler{}, errs.Errf(errs.ErrSourceInternal, err.Error())
	}

	return com, nil
}

//nolint:dupl
func (dbo *SchedulerRepo) Get(ctx context.Context, id string) (domains.Scheduler, error) {
	var (
		l         = logger.FromCtx(ctx, "schedulerRepo.Get")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		record    dbScheduler
		q         = `SELECT * FROM schedulers where id = $1`
	)

	if err := sqlClient.Get(&record, q, id); err != nil {
		l.Error("sqlClient.Get failed", zap.Error(err))

		if errors.Is(err, sql.ErrNoRows) {
			return domains.Scheduler{}, errs.Errf(errs.ErrNotFound, "Schedulers not found. ID %v", id)
		}

		return domains.Scheduler{}, errs.Errf(errs.ErrSourceConnectionErr, err.Error())
	}

	return record.toModelScheduler(), nil
}

//nolint:dupl
func (dbo *SchedulerRepo) UpdateScheduler(ctx context.Context, id, expression string) error {
	var (
		l         = logger.FromCtx(ctx, "schedulerRepo.UpdateScheduler")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		q         = `UPDATE schedulers SET
		expression=$1, updated_at=now()
		WHERE id=$2`
	)

	result, err := sqlClient.Exec(
		q,
		expression, id,
	)
	if err != nil {
		l.Error("sqlClient.Exec failed", zap.Error(err))

		return errs.Errf(errs.ErrSourceInternal, err.Error())
	}

	if count, _ := result.RowsAffected(); count != 1 {
		l.Error("result.RowsAffected() failed", zap.Int64("count", count))

		return errs.Errf(errs.ErrNotFound, "no scheduler record updated")
	}

	return nil
}

func (dbo *SchedulerRepo) UpdateSchedulerState(ctx context.Context, param domains.Scheduler) error {
	var (
		l         = logger.FromCtx(ctx, "schedulerRepo.UpdateSchedulerState")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		q         = `UPDATE schedulers SET
		status=$1,job_id=$2, updated_at=now()
		WHERE id=$3`
		r = fromModelToComDBScheduler(param)
	)
	result, err := sqlClient.Exec(
		q,
		r.Status, r.JobID, r.ID,
	)
	if err != nil {
		l.Error("sqlClient.Exec failed", zap.Error(err))

		return errs.Errf(errs.ErrSourceInternal, err.Error())
	}

	if count, _ := result.RowsAffected(); count != 1 {
		l.Error("result.RowsAffected() failed", zap.Int64("count", count))

		return errs.Errf(errs.ErrNotFound, "no scheduler record updated")
	}

	return nil
}
