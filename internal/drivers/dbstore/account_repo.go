package dbstore

import (
	"context"

	"go.uber.org/zap"
)

type AccountRepo struct {
	store *DBStore
}

func (ac *AccountRepo) Create(ctx context.Context, acc domains.Account) (domains.Account, error) {
	var (
		l         = logger.FromCtx(ctx, "accountRepo.Create")
		sqlClient = ac.store.sqlClientByCtx(ctx)
		q         = `insert into accounts(mfo,name,tax,code,acc_type,inn) 
                     values($1,$2,$3,$4,$5,$6) returning id`
	)
	r := fromModelToAccDB(acc)

	if err := sqlClient.GetContext(ctx, &acc.ID, q, r.Mfo, r.Name, r.Tax, r.Code, r.AccType, r.Inn); err != nil {
		l.Error("sqlClient.GetContext failed", zap.Error(err))

		return domains.Account{}, errs.Errf(errs.ErrSourceInternal, err.Error())
	}
	return acc, nil
}

func (ac *AccountRepo) GetAll(ctx context.Context) ([]domains.Account, error) {
	var (
		l         = logger.FromCtx(ctx, "accountRepo.GetAll")
		sqlClient = ac.store.sqlClientByCtx(ctx)
		q         = `select * from accounts`
		record    []dbAccount
	)
	if err := sqlClient.SelectContext(ctx, &record, q); err != nil {
		l.Error("sqlClient.SelectContext failed", zap.Error(err))
		return nil, errs.Errf(errs.ErrSourceInternal, err.Error())
	}

	return collectAccounts(record), nil
}

func (ac *AccountRepo) Get(ctx context.Context, acc domains.Account) (domains.Account, error) {
	var (
		l         = logger.FromCtx(ctx, "accountRepo.Get")
		sqlClient = ac.store.sqlClientByCtx(ctx)
		q         = `select * from accounts where id=$1`
		record    dbAccount
	)
	if err := sqlClient.GetContext(ctx, &record, q, acc.ID); err != nil {
		l.Error("sqlClient.GetContext failed", zap.Error(err))

		return domains.Account{}, errs.Errf(errs.ErrSourceInternal, err.Error())
	}

	return record.toModel(), nil
}

func (ac *AccountRepo) UpdateAccount(ctx context.Context, acc domains.Account) (domains.Account, error) {
	var (
		l         = logger.FromCtx(ctx, "accountRepo.UpdateAccount")
		sqlClient = ac.store.sqlClientByCtx(ctx)
		q         = `update accounts set mfo=$1,name=$2,tax=$3,code=$4,acc_type=$5,inn=$6 
                     where id=$7`
	)
	r := fromModelToAccDB(acc)

	result, err := sqlClient.Exec(q, r.Mfo, r.Name, r.Tax, r.Code, r.AccType, r.Inn, acc.ID)
	if err != nil {
		l.Error("sqlClient.Exec failed", zap.Error(err))

		return domains.Account{}, errs.Errf(errs.ErrSourceInternal, err.Error())
	}
	if count, _ := result.RowsAffected(); count != 1 {
		l.Error("result.RowsAffected() failed", zap.Int64("count", count))

		return domains.Account{}, errs.Errf(errs.ErrNotFound, "no account record updated")
	}

	return r.toModel(), nil
}
