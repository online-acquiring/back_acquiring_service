package dbstore

import (
	"database/sql"
)

type dbAccount struct {
	ID        sql.NullString `db:"id"`
	Mfo       sql.NullString `db:"mfo"`
	Name      sql.NullString `db:"name"`
	Tax       sql.NullString `db:"tax"`
	Code      sql.NullString `db:"code"`
	AccType   int8           `db:"acc_type"`
	Inn       sql.NullString `db:"inn"`
	CreatedAt sql.NullTime   `db:"created_at"`
	UpdatedAt sql.NullTime   `db:"updated_at"`
}

func (db *dbAccount) toModel() domains.Account {
	m := domains.Account{
		ID:      domains.ID(db.ID.String),
		Code:    db.Code.String,
		Name:    db.Name.String,
		Tax:     db.Tax.String,
		AccType: domains.AccType(db.AccType),
		MFO:     db.Mfo.String,
		Inn:     db.Inn.String,
	}
	m.Dates.CreatedAt = tools.ParseDBTime(db.CreatedAt)
	m.Dates.UpdatedAt = tools.ParseDBTime(db.UpdatedAt)
	return m
}

func fromModelToAccDB(acc domains.Account) dbAccount {
	acc.Dates.UpdatedAt = IsNullReturnNow(acc.Dates.UpdatedAt)
	return dbAccount{
		ID: sql.NullString{
			String: string(acc.ID),
			Valid:  string(acc.ID) != "",
		},
		Mfo: sql.NullString{
			String: acc.MFO,
			Valid:  acc.MFO != "",
		},
		Name: sql.NullString{
			String: acc.Name,
			Valid:  acc.Name != "",
		},
		Tax: sql.NullString{
			String: acc.Tax,
			Valid:  acc.Tax != "",
		},
		Code: sql.NullString{
			String: acc.Code,
			Valid:  acc.Code != "",
		},
		AccType: int8(acc.AccType),
		Inn: sql.NullString{
			String: acc.Inn,
			Valid:  acc.Inn != "",
		},
		CreatedAt: sql.NullTime{
			Time:  acc.Dates.CreatedAt,
			Valid: true,
		},
		UpdatedAt: sql.NullTime{
			Time:  acc.Dates.UpdatedAt,
			Valid: true,
		},
	}
}

func collectAccounts(db []dbAccount) []domains.Account {
	m := make([]domains.Account, len(db))
	for i := range db {
		m[i] = db[i].toModel()
	}
	return m
}
