package dbstore

import (
	"context"
	"database/sql"
	"errors"

	"go.uber.org/zap"
)

type UserRepo struct {
	store *DBStore
}

func (dbo *UserRepo) AddUser(ctx context.Context, user domains.User) (domains.User, error) {
	var (
		l         = logger.FromCtx(ctx, "userRepo.AddUser")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		q1        = `insert into users(name,state,login,role_code,email,merchant_id,phone_number) 
          values($1,$2,$3,$4,$5,$6,$7) returning id`
		r1 = dbUserFromModel(user)
	)
	if err := sqlClient.GetContext(ctx, &user.ID, q1, r1.Name,
		r1.State, r1.Login, r1.Role, r1.Email, r1.MerchantID, r1.PhoneNumber); err != nil {
		l.Error("sqlClient.GetContext failed", zap.Error(err))

		return domains.User{}, errs.Errf(errs.ErrInternal, err.Error())
	}

	var (
		q2 = `insert into credentials(password,user_id) values($1,$2) returning id`
		r2 = dbCredentialsFromModel(user)
	)
	if err := sqlClient.GetContext(ctx, &user.Credential.ID, q2, r2.Password, r2.UserID); err != nil {
		l.Error("sqlClient.GetContext failed", zap.Error(err))

		return domains.User{}, errs.Errf(errs.ErrInternal, err.Error())
	}

	return user, nil
}

//nolint:dupl
func (dbo *UserRepo) UpdateUserByUserID(ctx context.Context, user domains.User) error {
	var (
		l         = logger.FromCtx(ctx, "userRepo.UpdateUserByUserID")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		q         = `UPDATE credentials SET
		  password=$1
		WHERE user_id=$2`
	)

	result, err := sqlClient.Exec(
		q,
		user.Password, user.ID,
	)
	if err != nil {
		l.Error("sqlClient.Exec failed", zap.Error(err))

		return errs.Errf(errs.ErrSourceInternal, err.Error())
	}

	if count, _ := result.RowsAffected(); count != 1 {
		l.Error("result.RowsAffected() failed", zap.Int64("count", count))

		return errs.Errf(errs.ErrNotFound, "no user record updated")
	}

	return nil
}

func (dbo *UserRepo) UpdateUserPassByLogin(ctx context.Context, user domains.User) error {
	var (
		l         = logger.FromCtx(ctx, "userRepo.UpdateUserPassByLogin")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		q1        = `SELECT u.id from users u where u.login = $1`
		q2        = `UPDATE credentials SET
		  password=$1, added_by=1
		WHERE user_id=$2`
		userID string
	)
	if err := sqlClient.GetContext(ctx, &userID, q1, user.Login); err != nil {
		l.Error("sqlClient.GetContext failed", zap.Error(err))

		return errs.Errf(errs.ErrInternal, err.Error())
	}

	result, err := sqlClient.Exec(
		q2,
		user.Password, userID,
	)
	if err != nil {
		l.Error("sqlClient.Exec failed", zap.Error(err))

		return errs.Errf(errs.ErrSourceInternal, err.Error())
	}

	if count, _ := result.RowsAffected(); count != 1 {
		l.Error("result.RowsAffected() failed", zap.Int64("count", count))

		return errs.Errf(errs.ErrNotFound, "no user record updated")
	}

	if err = dbo.UpdateUserStateByLogin(ctx, user.Login, int(domains.UserActionActivate)); err != nil {
		l.Error("dbo.UpdateUserStateByLogin failed", zap.Error(err))

		return errs.Errf(errs.ErrInternal, err.Error())
	}

	return nil
}

//nolint:dupl
func (dbo *UserRepo) UpdateUserState(ctx context.Context, user domains.User) error {
	var (
		l         = logger.FromCtx(ctx, "userRepo.UpdateUserState")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		q         = `update users set state=$1 where id=$2`
	)
	result, err := sqlClient.Exec(q, user.State, user.ID)
	if err != nil {
		l.Error("sqlClient.Exec failed", zap.Error(err))

		return errs.Errf(errs.ErrSourceInternal, err.Error())
	}

	if count, _ := result.RowsAffected(); count != 1 {
		l.Error("result.RowsAffected() failed", zap.Int64("count", count))

		return errs.Errf(errs.ErrNotFound, "no user record updated")
	}

	return nil
}

func (dbo *UserRepo) UpdateUserStateByLogin(ctx context.Context, login string, state int) error {
	var (
		l         = logger.FromCtx(ctx, "userRepo.UpdateUserStateByLogin")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		q         = `update users set state=$1 where login=$2`
	)
	result, err := sqlClient.Exec(q, state, login)
	if err != nil {
		l.Error("sqlClient.Exec failed", zap.Error(err))

		return errs.Errf(errs.ErrSourceInternal, err.Error())
	}

	if count, _ := result.RowsAffected(); count != 1 {
		l.Error("result.RowsAffected() failed", zap.Int64("count", count))

		return errs.Errf(errs.ErrNotFound, "no user record updated")
	}

	return nil
}

func (dbo *UserRepo) GetUsers(ctx context.Context) ([]domains.User, error) {
	var (
		l         = logger.FromCtx(ctx, "userRepo.GetUsers")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		records   = make([]dbUser, 0)
		q         = `select u.*, m.org_name as org_name
                    from users u
                    inner join credentials c on u.id=c.user_id
                    left join merchants m on m.id=u.merchant_id`
	)
	if err := sqlClient.Select(&records, q); err != nil {
		l.Error("sqlClient.Select failed", zap.Error(err))

		return nil, errs.Errf(errs.ErrSourceInternal, err.Error())
	}

	users := make([]domains.User, len(records))
	for i := 0; i < len(records); i++ {
		users[i] = records[i].toModel()
	}

	return users, nil
}

func (dbo *UserRepo) GetUserByLogin(ctx context.Context, user domains.User) (domains.User, error) {
	var (
		l         = logger.FromCtx(ctx, "userRepo.GetUserByLogin")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		record    = dbUser{}
		q         = `SELECT u.id,u.name,u.login,
       					u.merchant_id,c.added_by as added_by, 
       					c.password as password,u.state,
       					u.role_code,c.expired_at as expired_at
          			FROM users u, credentials c 
          			WHERE u.id=c.user_id and u.login = $1 and (u.state = 2 or u.state = 1)`
	)
	if err := sqlClient.Get(&record, q, user.Login); err != nil {
		l.Error("sqlClient.Get failed", zap.Error(err), zap.String("user-login", user.Login))

		if errors.Is(err, sql.ErrNoRows) {
			return domains.User{}, errs.Errf(errs.ErrNotFound, "Ползователь не найден. login %v", user.Login)
		}

		return domains.User{}, errs.Errf(errs.ErrSourceConnectionErr, err.Error())
	}

	return record.toModel(), nil
}

func (dbo *UserRepo) GetUserByLoginAndPass(ctx context.Context, user domains.User) (domains.User, error) {
	var (
		l         = logger.FromCtx(ctx, "userRepo.GetUserByLoginAndPass")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		record    dbUser
		q         = `SELECT u.* FROM users u where u.login = $1 and u.password=$2 and u.state = 11`
	)
	if err := sqlClient.Get(&record, q, user.Login, user.Password); err != nil {
		l.Error("sqlClient.Get failed", zap.Error(err))

		if errors.Is(err, sql.ErrNoRows) {
			return domains.User{}, errs.Errf(errs.ErrNotFound, "User not found. login %v", user.Login)
		}

		return domains.User{}, errs.Errf(errs.ErrSourceConnectionErr, err.Error())
	}

	return record.toModel(), nil
}

//nolint:dupl
func (dbo *UserRepo) UpdateUserPassByExistedPassword(ctx context.Context, userID, newPass string) error {
	var (
		l         = logger.FromCtx(ctx, "userRepo.UpdateUserPassByExistedPassword")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		q         = `update credentials set password=$1, expired_at=now() + interval '90 day' where user_id=$2`
	)

	result, err := sqlClient.Exec(q, newPass, userID)
	if err != nil {
		l.Error("sqlClient.Exec failed", zap.Error(err))

		return errs.Errf(errs.ErrSourceInternal, err.Error())
	}

	if count, _ := result.RowsAffected(); count != 1 {
		l.Error("result.RowsAffected() failed", zap.Int64("count", count))

		return errs.Errf(errs.ErrNotFound, "no user record updated")
	}
	return nil
}

func (dbo *UserRepo) GetUserByID(ctx context.Context, userID domains.ID) (domains.User, error) {
	var (
		l         = logger.FromCtx(ctx, "userRepo.GetUserByID")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		record    dbUser
		q         = `SELECT * FROM users where id = $1`
	)
	if err := sqlClient.Get(&record, q, userID); err != nil {
		l.Error("sqlClient.Get failed", zap.Error(err))

		if errors.Is(err, sql.ErrNoRows) {
			return domains.User{}, errs.Errf(errs.ErrNotFound, "User not found. user-id %v", userID)
		}

		return domains.User{}, errs.Errf(errs.ErrSourceConnectionErr, err.Error())
	}

	return record.toModel(), nil
}
