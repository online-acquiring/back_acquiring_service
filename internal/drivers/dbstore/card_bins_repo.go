package dbstore

import (
	"context"
	"database/sql"
	"errors"

	"go.uber.org/zap"
)

type BinRepo struct {
	store *DBStore
}

func (dbo *BinRepo) Create(ctx context.Context, bin domains.Bin) (domains.Bin, error) {
	var (
		l         = logger.FromCtx(ctx, "binRepo.Create")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		data      = dbCardBins{}
		q         = `INSERT INTO card_bins (bin, ps_code)
					 VALUES ($1, $2) RETURNING *`
	)

	model := fromModelBin(bin)
	err := sqlClient.GetContext(ctx, &data, q, &model.BIN, &model.PSCode)
	if err != nil {
		l.Error("sqlClient.GetContext failed", zap.Error(err))

		return domains.Bin{}, err
	}

	return data.toModel(), nil
}

func (dbo *BinRepo) GetAll(ctx context.Context) ([]domains.Bin, error) {
	var (
		l         = logger.FromCtx(ctx, "binRepo.GetAll")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		record    []dbCardBins
		q         = `SELECT * FROM card_bins`
	)

	if err := sqlClient.SelectContext(ctx, &record, q); err != nil {
		l.Error("sqlClient.SelectContext failed", zap.Error(err))

		return []domains.Bin{}, errs.Errf(errs.ErrSourceConnectionErr, err.Error())
	}

	return binCollection(record), nil
}

//nolint:dupl
func (dbo *BinRepo) GetCardTypeByNumber(ctx context.Context, number string) (domains.Bin, error) {
	var (
		l         = logger.FromCtx(ctx, "binRepo.GetCardTypeByNumber")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		record    = dbCardBins{}
		q         = `SELECT * FROM card_bins c where position(c.bin in $1) = 1`
	)
	if err := sqlClient.Get(&record, q, number); err != nil {
		l.Error("sqlClient.Get failed", zap.Error(err))

		if errors.Is(err, sql.ErrNoRows) {
			return domains.Bin{}, errs.Errf(errs.ErrNotFound, "Card type not found. number %v", number)
		}

		return domains.Bin{}, errs.Errf(errs.ErrSourceConnectionErr, err.Error())
	}

	return record.toModel(), nil
}
