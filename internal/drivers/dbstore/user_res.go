package dbstore

import (
	"database/sql"
	"time"
)

type dbUser struct {
	ID          string         `db:"id"`
	Name        string         `db:"name"`
	Login       string         `db:"login"`
	Password    string         `db:"password"` // it is returned if needed
	State       int8           `db:"state"`
	Role        int8           `db:"role_code"`
	AddedBy     int8           `db:"added_by"`
	Email       sql.NullString `db:"email"`
	MerchantID  sql.NullString `db:"merchant_id"`
	PhoneNumber sql.NullString `db:"phone_number"`
	ExpiredAt   sql.NullTime   `db:"expired_at"`
	OrgName     sql.NullString `db:"org_name"`
	CreatedAt   sql.NullTime   `db:"created_at"`
	UpdatedAt   sql.NullTime   `db:"updated_at"`
}

type dbCredentials struct {
	ID        string       `db:"id"`
	Password  string       `db:"password"`
	UserID    string       `db:"user_id"`
	AddedBy   int8         `db:"added_by"`
	CreatedAt sql.NullTime `db:"created_at"`
	UpdatedAt sql.NullTime `db:"updated_at"`
	ExpiredAt sql.NullTime `db:"expired_at"`
}

func (db dbUser) toModel() domains.User {
	m := domains.User{
		ID:    domains.ID(db.ID),
		Name:  db.Name,
		Login: db.Login,
		Credential: domains.Credential{
			Password:  db.Password,
			ExpiredAt: db.ExpiredAt.Time,
			AddedBy:   db.AddedBy,
		},
		State: db.State,
		Role:  domains.Role(db.Role),
		Email: db.Email.String,
		Merchant: domains.Merchant{
			ID:      domains.ID(db.MerchantID.String),
			OrgName: db.OrgName.String,
		},
		PhoneNumber: db.PhoneNumber.String,
	}
	m.CreateUpdateTime.CreatedAt = tools.ParseDBTime(db.CreatedAt)
	m.CreateUpdateTime.UpdatedAt = tools.ParseDBTime(db.UpdatedAt)
	return m
}

func dbUserFromModel(model domains.User) dbUser {
	model.UpdatedAt = IsNullReturnNow(model.UpdatedAt)
	rec := dbUser{
		ID:      string(model.ID),
		Name:    model.Name,
		Login:   model.Login,
		State:   model.State,
		Role:    int8(model.Role),
		AddedBy: model.AddedBy,
		Email: sql.NullString{
			String: model.Email,
			Valid:  model.Email != "",
		},
		MerchantID: sql.NullString{
			String: string(model.Merchant.ID),
			Valid:  string(model.Merchant.ID) != "",
		},
		PhoneNumber: sql.NullString{
			String: model.PhoneNumber,
			Valid:  model.PhoneNumber != "",
		},
		CreatedAt: sql.NullTime{Valid: true, Time: model.CreatedAt},
		UpdatedAt: sql.NullTime{Valid: true, Time: model.UpdatedAt},
	}
	return rec
}

// func (db *dbCredentials) toModel() domains.Credential {
//	return domains.Credential{
//		ID:        domains.ID(db.ID),
//		Password:  db.Password,
//		ExpiredAt: db.ExpiredAt.Time,
//		CreateUpdateTime: domains.CreateUpdateTime{
//			CreatedAt: tools.ParseDBTime(db.CreatedAt),
//			UpdatedAt: tools.ParseDBTime(db.UpdatedAt),
//		},
//		AddedBy: db.AddedBy,
//	}
// }

const threeMonths = 3

func dbCredentialsFromModel(u domains.User) dbCredentials {
	u.UpdatedAt = IsNullReturnNow(u.UpdatedAt)
	rec := dbCredentials{
		ID:        string(u.Credential.ID),
		Password:  u.Password,
		UserID:    string(u.ID),
		AddedBy:   u.AddedBy,
		CreatedAt: sql.NullTime{Valid: true, Time: u.CreatedAt},
		UpdatedAt: sql.NullTime{Valid: true, Time: u.UpdatedAt},
		ExpiredAt: sql.NullTime{Valid: true, Time: time.Now().AddDate(0, threeMonths, 0)},
	}
	return rec
}
