package dbstore

import (
	"context"
	"database/sql"
	"errors"

	"go.uber.org/zap"
)

type ComRepo struct {
	store *DBStore
}

func (dbo *ComRepo) GetAll(ctx context.Context) ([]domains.Commission, error) {
	var (
		l         = logger.FromCtx(ctx, "comRepo.GetAll")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		q         = `select c.* from commissions c`
		record    []dbCommission
	)
	if err := sqlClient.SelectContext(ctx, &record, q); err != nil {
		l.Error("sqlClient.SelectContext", zap.Error(err))

		return nil, errs.Errf(errs.ErrSourceInternal, err.Error())
	}

	return collectCommissions(record), nil
}

func (dbo *ComRepo) Create(ctx context.Context, com domains.Commission) (domains.Commission, error) {
	var (
		l         = logger.FromCtx(ctx, "comRepo.Create")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		q         = `INSERT INTO commissions (merchant_id,fee_type,value,comm_type)
					 VALUES ($1, $2, $3, $4) RETURNING id`
	)
	r := fromModelToComDB(com)
	err := sqlClient.GetContext(ctx, &com.ID, q, r.MerchantID, r.FeeType, r.Value, r.ComType)
	if err != nil {
		l.Error("sqlClient.GetContext", zap.Error(err))

		return domains.Commission{}, errs.Errf(errs.ErrSourceInternal, err.Error())
	}

	return com, nil
}

func (dbo *ComRepo) Get(ctx context.Context, com domains.Commission) (domains.Commission, error) {
	var (
		l         = logger.FromCtx(ctx, "comRepo.Get")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		record    dbCommission
		q         = `SELECT * FROM commissions where merchant_id = $1`
	)

	if err := sqlClient.Get(&record, q, com.MerchantID); err != nil {
		l.Error("sqlClient.Get", zap.Error(err))

		if errors.Is(err, sql.ErrNoRows) {
			return domains.Commission{}, errs.Errf(errs.ErrNotFound, "Commissions not found. ID %v", com.MerchantID)
		}

		return domains.Commission{}, errs.Errf(errs.ErrSourceConnectionErr, err.Error())
	}

	return record.toModel(), nil
}

func (dbo *ComRepo) Update(ctx context.Context, com domains.Commission) (domains.Commission, error) {
	var (
		l         = logger.FromCtx(ctx, "comRepo.Update")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		q         = `UPDATE commissions SET fee_type=$1, value=$2,comm_type=$3 WHERE merchant_id=$4`
		r         = fromModelToComDB(com)
	)
	result, err := sqlClient.Exec(q, r.FeeType, r.Value, r.ComType, r.MerchantID)
	if err != nil {
		l.Error("sqlClient.Exec failed", zap.Error(err))

		return domains.Commission{}, errs.Errf(errs.ErrNotFound, "Commissions not found. ID %v", com.MerchantID)
	}

	if count, _ := result.RowsAffected(); count != 1 {
		l.Error("result.RowsAffected() failed", zap.Int64("count", count))

		return domains.Commission{}, errs.Errf(errs.ErrNotFound, "no commission record updated")
	}

	return r.toModel(), nil
}
