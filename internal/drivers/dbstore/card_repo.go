package dbstore

import (
	"context"
	"database/sql"
	"errors"

	"go.uber.org/zap"
)

type CardRepo struct {
	store *DBStore
}

func (dbo *CardRepo) Create(ctx context.Context, card domains.PayCard) (domains.PayCard, error) {
	var (
		l         = logger.FromCtx(ctx, "binRepo.Create")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		data      dbCard
		s         = `select * from card where id = $1`
		q         = `INSERT INTO card (id, masked_pan, bin, expiry)
					 VALUES ($1, $2, $3, $4) RETURNING *`
		model = fromModelCard(card)
	)

	err := sqlClient.Get(&data, s, card.ID)
	if err != nil {
		l.Error("sqlClient.Exec failed", zap.Error(err))
		if !errors.Is(err, sql.ErrNoRows) {
			return domains.PayCard{}, err
		}
	}

	if data.ID != "" {
		l.Error("result.RowsAffected() failed", zap.Error(errors.New("there is a duplicate")))

		return data.toModel(), nil
	}

	err = sqlClient.GetContext(ctx, &data, q, &model.ID, &model.MaskedPAN, &model.BIN, &model.Expiry)
	if err != nil {
		l.Error("sqlClient.GetContext failed", zap.Error(err))

		return domains.PayCard{}, err
	}

	return data.toModel(), nil
}

func (dbo *CardRepo) GetCardPS(ctx context.Context, id string) (ps domains.PSCode, ms, exp string, err error) {
	var (
		l         = logger.FromCtx(ctx, "binRepo.Get")
		sqlClient = dbo.store.sqlClientByCtx(ctx)
		data      []dbCardBin
		s         = `select cb.ps_code, c.masked_pan, c.expiry from card c
					left join card_bins cb on c.bin = cb.bin
					where c.id = $1;`
	)

	err = sqlClient.SelectContext(ctx, &data, s, id)
	if err != nil {
		l.Error("sqlClient.SelectContext failed", zap.Error(err), zap.String("id", id))

		return "", "", "", err
	}

	ps, ms, exp = data[0].toModel()

	return
}
